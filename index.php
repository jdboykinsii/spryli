<?php 
// Second Commit
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); 
header("Cache-Control: no-store, no-cache, must-revalidate"); 
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
require('navigation.php');
require("system/dbconnect.php");
require("system/Content/contentClass.php");
require("system/Analytics/analyticsClass.php");
require("system/Billing/billingClass.php");
$analytics = new Analytics();
$content = new Content();
$randomString = $content->rollTheDice();
$billing = new Billing();

?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>spryli &middot; Simple QR Codes</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" type="image/png" href="img/favicon.ico">

<link href='http://fonts.googleapis.com/css?family=Lato:400,700,300' rel='stylesheet' type='text/css'>
<!--[if IE]>
	<link href="http://fonts.googleapis.com/css?family=Lato" rel="stylesheet" type="text/css">
	<link href="http://fonts.googleapis.com/css?family=Lato:400" rel="stylesheet" type="text/css">
	<link href="http://fonts.googleapis.com/css?family=Lato:700" rel="stylesheet" type="text/css">
	<link href="http://fonts.googleapis.com/css?family=Lato:300" rel="stylesheet" type="text/css">
<![endif]-->

<link href="css/bootstrap.css" rel="stylesheet">
<link href="css/font-awesome.min.css" rel="stylesheet">
<link href="css/theme.css" rel="stylesheet">
<link href="css/prettyPhoto.css" rel="stylesheet" type="text/css"/>
<link href="css/zocial.css" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" href="css/nerveslider.css">
<link href="css/wysiwyg.css" rel="stylesheet">
<link href="css/iconStyle.css" rel="stylesheet">

<!--[if lt IE 9]>
<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<!--[if IE 7]>
<link rel="stylesheet" href="css/font-awesome-ie7.min.css">
<![endif]-->
<style>
.textControlsDiv {
					display: none;
				}
.pictureControlsDiv {
					display: none;
				}	
.videoControlsDiv {
					display: none;
				}				
	.glyph:hover {
		-webkit-transition: all 0.5s ease-out;
		background:#222821;
		color:#fff;
		text-shadow: none;
		-moz-text-shadow: none;
		-webkit-text-shadow: none;
		top:-5px;
		position:relative;
	}
</style>
</head>

<body>
<!--header-->
	<div class="header ">
		<!--logo-->
			<div class="container">
					<div class="logo">
						 <a href="index"><img src="img/spryli.png" alt="" class="animated bounceInDown" /></a>  
					</div>
					<!--menu-->
					<nav id="main_menu">
					<div class="menu_wrap">
						<ul class="nav sf-menu">
							<?php echo $navigationMenu; ?>
						</ul>
					</div>
				</nav>
			</div>
		</div>
	<!--//header-->
	<!--page-->

		<!-- /SLIDER -->
		
	<div id="banner">
	<div class="container intro_wrapper">
	<div class="inner_content">
	
	<!--welcome-->
		<div class="welcome_index">
		<span class="hue_block white normal">QR Codes:</span><span> Simplified </span>
		
		<br />
		Spryli makes it simple to create &amp; share QR codes for <span>Text, Pictures, or Embedded Videos.</span> 
		<br />
		<br />
		
		


		</div>
	<!--//welcome-->
		</div>
			</div>
				</div>
				<!--//banner-->
			
	<div class="container wrapper">
	<div class="inner_content">
	<div class="pad45"></div>
	<div class="row features">
		<div class="span4">
			<div class="tile">
			<div class="intro-icon-disc cont-large"><i class="icon-rocket intro-icon-large"></i></div>
			<h2>Quick
			<br><a href="#"><span>got 10 seconds?</span></a></h2>
			<p>Spryli helps you create as many unique QR codes or short links as you need, as quickly as possible. </p>
			</div> 
				<div class="pad25"></div>
		</div>
		<div class="span4">
			<div class="tile">
			<div class="intro-icon-disc cont-large"><i class="icon-magic intro-icon-large"></i></div>
			<h2>Easy
			<br><a href="#"><span>easy to use &amp; powerful results</span></a></h2>
			<p>Spryli was built from the ground up to make creating QR codes easy. </p>
			</div> 
				<div class="pad25"></div>
		</div>
		<div class="span4">
			<div class="tile">
			<div class="intro-icon-disc cont-large"><i class="icon-bar-chart intro-icon-large"></i></div>
			<h2>Results
			<br><a href="#"><span>track your results</span></a></h2>
			<p>Spryli tracks views for you across <span class="hue"> Phones, Tablets, and Computers.</span> </p>
			</div> 
				<div class="pad25"></div>
		</div>
	</div>
	<!-- Demo -->
	<div class="row demo">
	<div class="span12 highlight-blue">
		<h1 class="white text-center">Demo QR Codes are limited to <span> 50 FREE VIEWS</span></h1>
		<h1 class="white text-center"> What would you like to share? </h1>
		<a href="#" class="icon-file-alt demoIcon span1 offset3" style="font-size: 7em; color: #FFF;"></a>
		<a href="#" class="icon-picture demoIcon span1 offset1  " style="font-size: 7em; color: #FFF;"></a>
		<a href="#" class="icon-facetime-video demoIcon span1 offset1  " style="font-size: 7em; color: #FFF;"></a>

		
		
	</div>
	
		<div class="span12 well pictureControlsDiv">
		<h1 class="text-center"> What image would you like to share? </h1>
		<ul class="nav nav-tabs" id="tabs">
			<li class="active">
				<a href="#upload" data-toggle="tab">Edit</a>
			</li>
			<li>
				<a href="#view" class="" data-toggle="tab">View It</a>
			</li>
		</ul>
		<div class="tab-content">
			<div class="tab-pane active slide in text-center" id="upload">
			<div id="uploadStatus"> </div>
			<br />
			<form method="post" action="manage/processpictureuploads.php" enctype="multipart/form-data" id="frmImgUpload">
    <input name="ImageFile" class="btn btn-info btn-rounded btn-large" type="file" accept="image/*" multiple="false" />
	<input type="hidden" name="keyId" id="key" value="<?php echo $randomString ?>">
    <br />
	<br />
	<p class=""> By uploading, you agree to our <a href="terms.php" target="_blank">Site Terms &amp; Conditions</a></p>
		
    <input name="btnSubmit" class="btn btn-success btn-rounded btn-large" type="submit" value="Upload" />
</form>
</div>



<div class="tab-pane slide bg-color-white" id="view">
			
			<div class="span4 offset1">
			<h3> Scan this QR code:</h3>
				<img id="demoQR" src="https://chart.googleapis.com/chart?cht=qr&amp;chs=250x250&amp;chl=http://spryli.com/<?php echo $content->base64_url_encode($randomString) ?>">
			</div>
			<div class="span4 offset1">
			<h3> Or go to this link:</h3>
				<h2><a href="http://spryli.com/<?php echo $content->base64_url_encode($randomString) ?>" target="_blank">spryli.com/<?php echo $content->base64_url_encode($randomString) ?></a></h2>
			</div>
			
			</div>
		</div>
		</div>
		
		<div class="span12 well videoControlsDiv">
		<h1 class="text-center"> What is the embedd code of the video? </h1>
		<ul class="nav nav-tabs" id="tabs">
			<li class="active">
				<a href="#editVideo" data-toggle="tab">Edit</a>
			</li>
			<li>
				<a href="#viewVideo" class="" data-toggle="tab">View It</a>
			</li>
		</ul>
		<div class="tab-content">
			<div class="tab-pane active slide in text-center" id="editVideo">
				<div class="row">
						<div class="span5">
							
							<textarea name="newVideoEmbeddCode" id="newVideoEmbeddCode" class="span5" rows="12"></textarea>
							
						</div>
						<div class="span5 offset1">
							<div id="resultOutput">
							<h2> <a href=""><i class="icon-arrow-left"></i></a>Paste the embedd code here.</h2>
							<h3>For YouTube: </h3>
							<p> When viewing the video page, click "Share", and then click "Embed". </p>
							<h3>For Vimeo: </h3>
							<p> When viewing the video, click the "Share" icon near the top right corner of the video. </p>
							</div>
							<p class=""> By uploading, you agree to our <a href="terms.php" target="_blank">Site Terms &amp; Conditions</a></p>
	
							<a class="btn btn-large btn-success pull-right" id="saveVideo">Done</a>
						</div>
					</div>
			</div>
			<div class="tab-pane slide bg-color-white" id="viewVideo">
				<div class="span4 offset1">
			<h3> Scan this QR code:</h3>
				<img id="demoQR" src="https://chart.googleapis.com/chart?cht=qr&amp;chs=250x250&amp;chl=http://spryli.com/<?php echo $content->base64_url_encode($randomString) ?>">
			</div>
			<div class="span4 offset1">
			<h3> Or go to this link:</h3>
				<h2><a href="http://spryli.com/<?php echo $content->base64_url_encode($randomString) ?>" target="_blank">spryli.com/<?php echo $content->base64_url_encode($randomString) ?></a></h2>
			</div>
			</div>
		</div>
		
		</div>
		
        <div class="span12 well textControlsDiv">
		<h1 class="text-center"> What text would you like to share? </h1>
        	     <div class="row-fluid" id="controlsContainer">
        <div class="">
          <ul class="nav nav-tabs" id="tabs">
			<li class="active">
				<a href="#edit" data-toggle="tab">Edit</a>
			</li>
			<li>
				<a href="#qr-code" class="viewIt" data-toggle="tab">View It</a>
			</li>
		</ul>

		<div class="tab-content">
			<div class="tab-pane active slide in" id="edit">
			
	<div id="alerts"></div>
	<div id="editorMain">
	
    <div class="btn-toolbar" data-role="editor-toolbar" data-target="#editor">
      <div class="btn-group">
        <a class="btn dropdown-toggle" data-toggle="dropdown" title="" data-original-title="Font"><i class="icon-font"></i><b class="caret"></b></a>
          <ul class="dropdown-menu">
          <li><a data-edit="fontName Serif" style="font-family:'Serif'">Serif</a></li><li><a data-edit="fontName Sans" style="font-family:'Sans'">Sans</a></li><li><a data-edit="fontName Arial" style="font-family:'Arial'">Arial</a></li><li><a data-edit="fontName Arial Black" style="font-family:'Arial Black'">Arial Black</a></li><li><a data-edit="fontName Courier" style="font-family:'Courier'">Courier</a></li><li><a data-edit="fontName Courier New" style="font-family:'Courier New'">Courier New</a></li><li><a data-edit="fontName Comic Sans MS" style="font-family:'Comic Sans MS'">Comic Sans MS</a></li><li><a data-edit="fontName Helvetica" style="font-family:'Helvetica'">Helvetica</a></li><li><a data-edit="fontName Impact" style="font-family:'Impact'">Impact</a></li><li><a data-edit="fontName Lucida Grande" style="font-family:'Lucida Grande'">Lucida Grande</a></li><li><a data-edit="fontName Lucida Sans" style="font-family:'Lucida Sans'">Lucida Sans</a></li><li><a data-edit="fontName Tahoma" style="font-family:'Tahoma'">Tahoma</a></li><li><a data-edit="fontName Times" style="font-family:'Times'">Times</a></li><li><a data-edit="fontName Times New Roman" style="font-family:'Times New Roman'">Times New Roman</a></li><li><a data-edit="fontName Verdana" style="font-family:'Verdana'">Verdana</a></li></ul>
        </div>
      <div class="btn-group">
        <a class="btn dropdown-toggle" data-toggle="dropdown" title="" data-original-title="Font Size"><i class="icon-text-height"></i>&nbsp;<b class="caret"></b></a>
          <ul class="dropdown-menu">
          <li><a data-edit="fontSize 5"><font size="5">Huge</font></a></li>
          <li><a data-edit="fontSize 3"><font size="3">Normal</font></a></li>
          <li><a data-edit="fontSize 1"><font size="1">Small</font></a></li>
          </ul>
      </div>
      <div class="btn-group">
        <a class="btn" data-edit="bold" title="" data-original-title="Bold (Ctrl/Cmd+B)"><i class="icon-bold"></i></a>
        <a class="btn" data-edit="italic" title="" data-original-title="Italic (Ctrl/Cmd+I)"><i class="icon-italic"></i></a>
        <a class="btn" data-edit="strikethrough" title="" data-original-title="Strikethrough"><i class="icon-strikethrough"></i></a>
        <a class="btn" data-edit="underline" title="" data-original-title="Underline (Ctrl/Cmd+U)"><i class="icon-underline"></i></a>
      </div>
      <div class="btn-group">
        <a class="btn btn-info" data-edit="insertunorderedlist" title="" data-original-title="Bullet list"><i class="icon-list-ul"></i></a>
        <a class="btn" data-edit="insertorderedlist" title="" data-original-title="Number list"><i class="icon-list-ol"></i></a>
        <a class="btn" data-edit="outdent" title="" data-original-title="Reduce indent (Shift+Tab)"><i class="icon-indent-left"></i></a>
        <a class="btn" data-edit="indent" title="" data-original-title="Indent (Tab)"><i class="icon-indent-right"></i></a>
      </div>
      <div class="btn-group">
        <a class="btn btn-info" data-edit="justifyleft" title="" data-original-title="Align Left (Ctrl/Cmd+L)"><i class="icon-align-left"></i></a>
        <a class="btn" data-edit="justifycenter" title="" data-original-title="Center (Ctrl/Cmd+E)"><i class="icon-align-center"></i></a>
        <a class="btn" data-edit="justifyright" title="" data-original-title="Align Right (Ctrl/Cmd+R)"><i class="icon-align-right"></i></a>
        <a class="btn" data-edit="justifyfull" title="" data-original-title="Justify (Ctrl/Cmd+J)"><i class="icon-align-justify"></i></a>
      </div>
      <div class="btn-group">
		  <a class="btn dropdown-toggle" data-toggle="dropdown" title="" data-original-title="Hyperlink"><i class="icon-link"></i></a>
		    <div class="dropdown-menu input-append">
			    <input class="span2" placeholder="URL" type="text" data-edit="createLink">
			    <button class="btn" type="button">Add</button>
        </div>
        <a class="btn" data-edit="unlink" title="" data-original-title="Remove Hyperlink"><i class="icon-cut"></i></a>

      </div>
      
      <div class="btn-group">
        <a class="btn" data-edit="undo" title="" data-original-title="Undo (Ctrl/Cmd+Z)"><i class="icon-undo"></i></a>
        <a class="btn" data-edit="redo" title="" data-original-title="Redo (Ctrl/Cmd+Y)"><i class="icon-repeat"></i></a>
      </div>
	  <input type="hidden" name="keyId" id="key" value="<?php echo $randomString ?>">
	   <button class="btn btn-success" id="saveButton">Save</button>
 
     </div>
	<p class=""> By uploading, you agree to our <a href="terms.php" target="_blank">Site Terms &amp; Conditions</a></p>
	
    <div id="editor" contenteditable="true">
  <p>What would you like to say?</p>
    </div>
	</div>
			</div>
			<div class="tab-pane slide bg-color-white" id="qr-code">
			
			<div class="span4 offset1">
			<h3> Scan this QR code:</h3>
				<img id="demoQR" src="https://chart.googleapis.com/chart?cht=qr&amp;chs=250x250&amp;chl=http://spryli.com/<?php echo $content->base64_url_encode($randomString) ?>">
			</div>
			<div class="span4 offset1">
			<h3> Or go to this link:</h3>
				<h2><a href="http://spryli.com/<?php echo $content->base64_url_encode($randomString) ?>" target="_blank">spryli.com/<?php echo $content->base64_url_encode($randomString) ?></a></h2>
			</div>
			
			</div>
		</div>

        </div>
        
      </div>
        </div>
      </div>
	<!-- END DEMO -->
	<!--info boxes-->
	<div class="row">
	<span><h1 class="text-center span12"> Pricing <button class="btn btn-large btn-info btn-rounded" id="viewMonthly">Monthly</button> <button class="btn btn-large btn-success btn-rounded" id="viewppu">Pay Per Use</button> </h1></span>
	<br />
	</div>
	
	<div class="row pricing">	
	<br />
				<!--block1-->
		<?php $billing->listPlans(); ?>
	</div>
						
			<!--//info boxes-->
	</div>
		<!--//page-->
		
		<div class="pad25 hidden-desktop"></div>
	</div>
	
	<!-- footer -->
	
	
	<!-- footer 2 -->
	<div id="footer2">
		<div class="container">
			<div class="row">
				<div class="span12">
				<a href="terms.php">Site Terms &amp; Conditions, Privacy Policy</a>
				<div class="copyright">
							spryli
							&copy;
							<script type="text/javascript">
							//<![CDATA[
								var d = new Date()
								document.write(d.getFullYear())
								//]]>
								</script>
							 - All Rights Reserved
						</div>
						</div>
					</div>
				</div>
					</div>
						
				<!-- up to top -->
				<a href="#"><i class="go-top hidden-phone hidden-tablet  icon-double-angle-up"></i></a>
				<!--//end-->
				
<script src="js/jquery.js"></script>			
<script src="js/bootstrap.min.js"></script>	
<script src="js/jquery.touchSwipe.min.js"></script>
<script src="js/jquery.mousewheel.min.js"></script>				
<script src="js/superfish.js"></script>
<script type="text/javascript" src="js/jquery.prettyPhoto.js"></script>
<script type="text/javascript" src="js/scripts.js"></script>
<!-- carousel -->
<script type="text/javascript" src="js/jquery.carouFredSel-6.2.1-packed.js"></script>
<script type="text/javascript">

$(document).ready(function() {

	
	
	
	$('.icon-file-alt').click(function(e){
		e.preventDefault();
		$('.textControlsDiv').fadeIn('slow');
		$('.pictureControlsDiv').fadeOut('fast');
		$('.videoControlsDiv').fadeOut('fast');
	});
	$('.icon-picture').click(function(e){
		e.preventDefault();
		$('.pictureControlsDiv').fadeIn('slow');
		$('.textControlsDiv').fadeOut('fast');
		$('.videoControlsDiv').fadeOut('fast');
      
	 });
	 $('.icon-facetime-video').click(function(e){
		e.preventDefault();
		$('.pictureControlsDiv').fadeOut('slow');
		$('.textControlsDiv').fadeOut('fast');
		$('.videoControlsDiv').fadeIn('fast');
      
	 });
	 $('#viewMonthly').click(function(){

		$('.ppu').fadeTo('slow', 0.3, function() {
		$('.monthly').fadeTo('slow', 1.0);
	  });
		
	 });
	 
	 $('#viewppu').click(function(){

		$('.monthly').fadeTo('slow', 0.3, function() {
			$('.ppu').fadeTo('slow', 1.0);
		});
		
	 });
	 
	 $(".navItem").click(function(e) {
			e.preventDefault();
			var scrollTo = $(this).attr('id');
			$('html, body').animate({
			 scrollTop: $(scrollTo).offset().top
				}, 500);
		});
		
	 
	});
	
	</script>
	<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-42810904-1', 'spryli.com');
  ga('send', 'pageview');

</script>
<!-- slider -->
<script src="js/jquery-ui.min.js"></script>
<script type="text/javascript" src="js/jquery.nerveSlider.min.js"></script>
<script type="text/javascript" src="js/bootstrap-wysiwyg.js"></script>
<script type="text/javascript" src="js/core.js"></script>
<script type="text/javascript" src="https://mindmup.s3.amazonaws.com/lib/jquery.hotkeys.js"></script>
<script type="text/javascript" src="js/storage.js"></script>
<script type="text/javascript" src="js/jquery.form.js"></script>

</body>
</html>