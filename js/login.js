// Javascript UI Functions
$(document).ready(function () {
$('#loginBtn').click( function() {
	var loginEmail = $('#loginEmail').val().toLowerCase();								  
	var loginPassword = $('#loginPassword').val();		
	var tag = "loginUser";
	var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
	if(loginEmail == "") {
		$('#form_status').html('<p class="text-error">Please enter your email.</p>');
		return false;
	}				  
	
	if( !emailReg.test( loginEmail ) ) {
		$('#form_status').html('<p class="text-error">Please enter a valid email.</p>');
		return false;
	}
	else if(loginPassword == "") {
		$('#form_status').html('<p class="text-error">Please enter a valid password.</p>');
		return false;
	}
		var jsonData = { 
			"tag" : tag, 
			"loginEmail": loginEmail, 
			"loginPassword": loginPassword};
		$('#form_status').html('<h2> Logging you in... <img src="../img/ajax-loader.gif" alt="progress bar"/></h2>');	
		$.ajax({
            type: "POST",
			dataType: 'JSON',
            url: 'system/AJAX/ajaxHandler.php',
            data: {
                jsonObj: jsonData
            },
            success: function (data) {
				var data = eval(data);
				if (data.status == "TRUE") {
					window.location='manage/index.php';
				}
				else if (data.status == "FALSE") {
					$('#form_status').html('<p class="text-error">Your email or password is incorrect.</p>');
				}
				else {
					   	$('.loginForm').html('<div class="well"><h2>We&#39;re sorry</h2> An error has occured in signing you in please try again later. </div>');		
				}
            },
            error: function (data) {
               	$('#form_status').html('<div class="well"><h2>We&#39;re sorry</h2> An error has occured in signing you in please try again later. </div>');	
            }
        });

	
});

$('#loginPassword').click( function() {
	$('#form_status').html('');
});

$('#loginEmail').click( function() {
	$('#form_status').html('');
});

$("#loginEmail").keyup(function(event){
    if(event.keyCode == 13){
        $("#loginBtn").click();
    }
});

$("#loginPassword").keyup(function(event){
    if(event.keyCode == 13){
        $("#loginBtn").click();
    }
});


});