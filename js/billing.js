$(document).ready(function () {
$('.cancelAccount').click( function() {
	var allegedUserId = $('.cancelAccount').attr('id');								  	
	var tag = "cancelAccount";

		var jsonData = { 
			"tag" : tag, 
			"userId": allegedUserId};
		if (confirm('Are you sure you want to close your account? (This will cancel your current plan)')) { 
			$.ajax({
				type: "POST",
				dataType: 'JSON',
				url: '../system/AJAX/ajaxHandler.php',
				data: {
					jsonObj: jsonData
				},
				success: function (data) {
					var data = eval(data);
					if (data.status == "SUCCESS") {
						alert(data.message);
						location.reload();
					}
					
					else {
							alert(data.message);		
					}
				},
				error: function (data) {
					alert("We're sorry. An error has occured in closing your account. Our technicians have been notified.");	
				}
			});
		}
		

	
});


});