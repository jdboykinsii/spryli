// JavaScript Document
$(document).ready(function() {
$('#email').change(function() {
		var email = $('#email').val().toLowerCase();
		var tag = "checkEmail";
		var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
		var jsonData = {tag: "checkEmail", email: email};
		if(email == "") {
			$('#signupStatus').html('<h2 class="text-error">Please enter a valid email address.</h2>');
			window.scrollTo(0,0);
			return false;
		}
		if( !emailReg.test( email ) ) {
			$('#signupStatus').html('<h2 class="text-error">Please enter a valid email address.</h2>');
			window.scrollTo(0,0);
			return false;
		}
		$.ajax({
            type: "POST",
			dataType: "JSON", 
            url: 'system/AJAX/ajaxHandler.php',
            data: { 
				jsonObj: jsonData
			},
            success: function (data) {
			var data = eval(data);
			if(data.status == "PASS"){
			$('#signupStatus').html('<h2 class="text-success">This email is available!</h2>');
			
			}
		    else if (data.status == "TAKEN" ) {
					$('#signupStatus').html('<h2 class="text-error">Sorry, this email is taken!</h2>');
					window.scrollTo(0,0);
				}
            },
            error: function (data) {
			$('#signupStatus').html('<h2 class="text-error"><b>Hmm...</b> Something went wrong, but our technicians are on it! </h2>');
				window.scrollTo(0,0);
			}
        });
});


$('#signupButton').click( function() {
	$('#signupStatus').html('');
	$('#billingStatus').html('');
	var planType = $('#planType').val();
	var fName = $('#fName').val().toLowerCase();
	var lName = $('#lName').val().toLowerCase();
	var email = $('#email').val().toLowerCase();
	var password = $('#password').val();
	var confirmPassword = $('#passwordConfirm').val();
	var cardNumber = $('#cardNumber').val();
	var nameOnCard = $('#nameOnCard').val();
	var cvc = $('#cvc').val();
	var expMonth = $('#expMonth').val();
	var expYear = $('#expYear').val();
	var address1 = $('#address1').val();
	var address2 = $('#address2').val();
	var city = $('#city').val();
	var state = $('#state').val();
	var zip = $('#zip').val();
	var phoneNum = $('#phone').val();
	var agree = $('input:checkbox:checked').val();
	var tag = "createNewAccount";
	var couponCode = $('#coupon').val();
			
	if(fName == "" ) {
		$('#signupStatus').html('<h2 class="text-error">Please enter your first name.</h2>');
		window.scrollTo(0,0);
		return false;
	}
	if(lName == "" ) {
		$('#signupStatus').html('<h2 class="text-error">Please enter your last name.</h2>');
		window.scrollTo(0,0);
		return false;
	}
	if(email == "") {
		$('#signupStatus').html('<h2 class="text-error">Please enter your email.</h2>');
		window.scrollTo(0,0);
		return false;
	}
	if(password == "" ) {
		$('#signupStatus').html('<h2 class="text-error">Please enter a password.</h2>');
		window.scrollTo(0,0);
		return false;
	}
	if(password != confirmPassword ) {
		$('#signupStatus').html('<h2 class="text-error">Your passwords must match!</h2>');
		window.scrollTo(0,0);
		return false;
	}
	if(cardNumber == '' ) {
		$('#billingStatus').html('<h2 class="text-error">Please enter the card number. </h2>');
		$('html, body').animate({ scrollTop: $('#billingStatus').offset().top }, 'slow');
		return false;
	}
	if(cardNumber.length < 16 ) {
		$('#billingStatus').html('<h2 class="text-error">Please enter the 16 digit card number.</h2>');
		$('html, body').animate({ scrollTop: $('#billingStatus') }, 'slow');
		return false;
	}
	if(nameOnCard == '' ) {
		$('#billingStatus').html('<h2 class="text-error">Please enter the name on the card. </h2>');
		$('html, body').animate({ scrollTop: $('#billingStatus').offset().top }, 'slow');
		return false;
	}
	
	if(cvc == '' ) {
		$('#billingStatus').html('<h2 class="text-error">Please enter the CVC for this card. (3 digit number on the back of the card) </h2>');
		$('html, body').animate({ scrollTop: $('#billingStatus').offset().top }, 'slow');
		return false;
	}
	if(expMonth == '' ) {
		$('#billingStatus').html('<h2 class="text-error">Please enter the expiration month.</h2>');
		$('html, body').animate({ scrollTop: $('#billingStatus').offset().top }, 'slow');
		return false;
	}
	if(expYear == '' ) {
		$('#billingStatus').html('<h2 class="text-error">Please enter the expiration year. </h2>');
		$('html, body').animate({ scrollTop: $('#billingStatus').offset().top }, 'slow');
		return false;
	}
	if(address1 == '' ) {
		$('#billingStatus').html('<h2 class="text-error">Please enter the address the card was registered in.</h2>');
		$('html, body').animate({ scrollTop: $('#billingStatus').offset().top }, 'slow');
		return false;
	}
	if(city == '' ) {
		$('#billingStatus').html('<h2 class="text-error">Please enter the city this card was registered in.</h2>');
		$('html, body').animate({ scrollTop: $('#billingStatus').offset().top }, 'slow');
		return false;
	}
	if(state == '' ) {
		$('#billingStatus').html('<h2 class="text-error">Please enter the state this card was registered in. </h2>');
		$('html, body').animate({ scrollTop: $('#billingStatus').offset().top }, 'slow');
		return false;
	}
	if(zip == '' ) {
		$('#billingStatus').html('<h2 class="text-error">Please enter the zip this card was registered in. </h2>');
		$('html, body').animate({ scrollTop: $('#billingStatus').offset().top }, 'slow');
		return false;
	}
	if (agree == undefined) {
		$('#signupStatus').html('<h2 class="text-error">You must agree to the site terms.</h2>');
		window.scrollTo(0,0);
		return false;
	}
	
	var jsonData = { 
			"tag": tag,
			"fname":fName,
			"lname":lName, 
			"email" : email,
			"password": password,
			"cardName": nameOnCard, 
			"cardNumber": cardNumber, 
			"cvc": cvc, 
			"expMonth": expMonth, 
			"expYear": expYear,
			"address1":address1, 
			"address2":address2, 
			"city": city, 
			"state": state, 
			"zip": zip, 
			"phoneNum": phoneNum,
			"planType": planType, 
			"coupon": couponCode};
	
	$('#processing').show();
	$('#signupForm').hide();
	$.ajax({
            type: 'POST',
			dataType: 'JSON', 
            url: 'system/AJAX/ajaxHandler.php',
            data: { 
				jsonObj: jsonData
			},
            success: function (data) {
			var data = eval(data);
			console.log(data);
			if(data.status == "SUCCESS" ){

				$('#signupForm').html('<div class="well"><h2>Success!</h2><br/> <p>You&#39;re all signed up, check your email for the details!</h2> <br /> <a href="login.php" class="btn btn-success btn-large"> Login </a> </div>');			
				$('#processing').hide();
				$('#signupForm').show();
			}
			else if(data.status == "CARD_ERROR"){
				$('#signupForm').show();
				$('#processing').hide();
				$('#billingStatus').html('<h2 class="text-error">'+data.message+'</h2>');
				$('html, body').animate({ scrollTop: $('#billingStatus').offset().top }, 'slow');
			}
			else if(data.status == "USEREXISTS"){
				$('#signupForm').show();
				$('#processing').hide();
				$('#signupStatus').html('<h2 class="text-error">'+data.message+'</h2>');
				$('html, body').animate({ scrollTop: $('#signupStatus').offset().top }, 'slow');
			}
			else if(data.status == "ERROR") {
				$('#signupStatus').html('<div class="well"><h2 class="text-error">We&#39;re sorry</h2><h2 class="text-error"> An error has occured in signing you up (our bad) please try again later. </h2> </div>');	
				$('#processing').hide();
				$('#signupForm').show();				
				window.scrollTo(0,0);
			}
			
			else {
				$('#signupStatus').html('<div class="well"><h2 class="text-error">We&#39;re sorry</h2><h2 class="text-error"> An error has occured in signing you up (our bad) please try again later. </h2> </div>');	
				$('#processing').hide();
				$('#signupForm').show();
				window.scrollTo(0,0);
			}

            },
            error: function (data) {
				$('#signupStatus').html('<div class="well"><h2 class="text-error">We&#39;re sorry</h2><h2 class="text-error"> An error has occured in signing you up (our bad) please try again later. </h2> </div>');	
			}
        });

});

});