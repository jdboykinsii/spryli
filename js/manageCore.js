$(document).ready(function () {
     $(".doneNewItem").click(function () {
        $("#saveButton").attr("class", "btn disabled");
        $("#saveButton").append('<img src="../img/saving.gif"/>');
        var contentName = $('#newItemName').val();
		if (contentName == ''){
			alert('Please enter a name for this content');
			$('#start').addClass('active');
			$('#content').removeClass('active');
			return false;
		}
        a = $("#editor").html();
        a = {
            tag: "createContentLoggedIn",
			contentName: contentName, 
            contentData: a, 
			type: "text"
        };
        $.ajax({
            type: "POST",
            dataType: "json",
            url: "../system/AJAX/ajaxHandler.php",
            data: {
                jsonObj: a
            },
            success: function (a) {
                "SUCCESS" != eval(a).status && $("#alerts").html("<h3>An error has occured, please try again later.</h3>");
                $("#saveButton").removeClass("disabled");
                $("#saveButton").addClass("btn-success");
                $("#saveButton").html("Save");
				var newItemId = eval(a).contentId;
				var newUrl = eval(a).newUrl;
				$('.items').append('<a href="#" id="'+newItemId+'" class="itemSpecial btn btn-rounded btn-info existingItem">'+contentName+'</a>');
				$('#newItemQRCode').attr('src', 'http://chart.googleapis.com/chart?cht=qr&chs=250x250&chl=http://spryli.com/'+newUrl);
				$('#newItemURL').attr('href', 'http://spryli.com/'+newUrl);
				$('#newItemURL').html('http://spryli.com/'+newUrl);		
				$('#newItemTweet').attr('data-url', 'http://spryli.com/'+newUrl);
				twttr.widgets.load();
				$('.newItemViewLinks').show();
				$('.zeroResult').hide();
				$('.noItems').remove();
            },
            error: function () {
                $("#alerts").html("<h3>An error has occured, please try again later.</h3>");
            }
        })
    });
	
	  $(".doneNewVideoItem").click(function () {
        var contentName = $('#newItemName').val();
		if (contentName == ''){
			alert('Please enter a name for this content');
			$('#start').addClass('active');
			$('#content').removeClass('active');
			return false;
		}
        contentData = $("#newVideoEmbeddCode").val();
        a = {
            tag: "createContentLoggedIn",
			contentName: contentName, 
            contentData: contentData, 
			type: "video"
        };
        $.ajax({
            type: "POST",
            dataType: "json",
            url: "../system/AJAX/ajaxHandler.php",
            data: {
                jsonObj: a
            },
            success: function (a) {
                "SUCCESS" != eval(a).status && $("#alerts").html("<h3>An error has occured, please try again later.</h3>");
                $("#saveButton").removeClass("disabled");
                $("#saveButton").addClass("btn-success btn-rounded pull-right");
                $("#saveButton").html("Save");
				var newItemId = eval(a).contentId;
				var newUrl = eval(a).newUrl;
				$('.items').append('<a href="#" id="'+newItemId+'" class="itemSpecial btn btn-rounded btn-info existingItem">'+contentName+'</a>');
				$('#newItemQRCode').attr('src', 'http://chart.googleapis.com/chart?cht=qr&chs=250x250&chl=http://spryli.com/'+newUrl);
				$('#newItemURL').attr('href', 'http://spryli.com/'+newUrl);
				$('#newItemURL').html('http://spryli.com/'+newUrl);		
				$('#newItemTweet').attr('data-url', 'http://spryli.com/'+newUrl);
				twttr.widgets.load();
				$('.newItemViewLinks').show();
				$('.zeroResult').hide();
				$('.noItems').remove();
            },
            error: function () {
                $("#alerts").html("<h3>An error has occured, please try again later.</h3>");
            }
        })
    });

$("#saveButton").click(function () {
        $("#saveButton").attr("class", "btn disabled");
        $("#saveButton").append('<img src="../img/saving.gif"/>');
        var key = $("#key").val();
		var contentName = $('#newItemName').val();
		if (contentName == ''){
			alert('Please enter a name for this content');
			$('#start').addClass('active');
			$('#content').removeClass('active');
			return false;
		}
        a = $("#editor").html();
        a = {
            tag: "createContentLoggedIn",
			contentName: contentName, 
            contentData: a, 
			type: "text"
        };
        $.ajax({
            type: "POST",
            dataType: "json",
            url: "../system/AJAX/ajaxHandler.php",
            data: {
                jsonObj: a
            },
            success: function (a) {
                "SUCCESS" != eval(a).status && $("#alerts").html("<h3>An error has occured, please try again later.</h3>");
                $("#saveButton").removeClass("disabled");
                $("#saveButton").addClass("btn-success");
                $("#saveButton").html("Save")
				var newItemId = eval(a).contentId;
				var newUrl = eval(a).newUrl;
				$('.items').append('<a href="#" id="'+newItemId+'" class="itemSpecial btn btn-rounded btn-info existingItem">'+contentName+'</a>');
				$('#newItemQRCode').attr('src', 'http://chart.googleapis.com/chart?cht=qr&chs=250x250&chl=http://spryli.com/'+newUrl);
				$('#newItemURL').attr('href', 'http://spryli.com/'+newUrl);
				$('#newItemURL').html('http://spryli.com/'+newUrl);
				$('#newItemTweet').attr('data-url', 'http://spryli.com/'+newUrl);
				twttr.widgets.load();
				$('.newItemViewLinks').show();
				$('.zeroResult').hide();	
				$('.noItems').remove();				
            },
            error: function () {
                $("#alerts").html("<h3>An error has occured, please try again later.</h3>");
            }
        })
    });
	
	
	$("#saveExistingItemButton").click(function () {
        $("#saveExistingItemButton").attr("class", "btn disabled");
        $("#saveExistingItemButton").append('<img src="../img/saving.gif"/>');
        var key = $("#key").val();
		var contentId = $('.item-selected').attr('id');
        a = $(".textContentDiv").html();
        a = {
            tag: "saveContent",
			contentId: contentId, 
            contentData: a, 
			type: "text"
        };
        $.ajax({
            type: "POST",
            dataType: "json",
            url: "../system/AJAX/ajaxHandler.php",
            data: {
                jsonObj: a
            },
            success: function (a) {
                "SUCCESS" != eval(a).status && $("#alerts").html("<h3>An error has occured, please try again later.</h3>");
                $("#saveExistingItemButton").removeClass("disabled");
                $("#saveExistingItemButton").addClass("btn-success");
                $("#saveExistingItemButton").html("Save")
				var newItemId = eval(a).contentId;
				var newUrl = eval(a).newUrl;
				$('.items').append('<a href="#" id="'+newItemId+'" class="itemSpecial btn btn-rounded btn-info existingItem">'+contentName+'</a>');
				$('#newItemQRCode').attr('src', 'http://chart.googleapis.com/chart?cht=qr&chs=250x250&chl=http://spryli.com/'+newUrl);
				$('#newItemURL').attr('href', 'http://spryli.com/'+newUrl);
				$('#newItemURL').html('http://spryli.com/'+newUrl);
				$('#newItemTweet').attr('data-url', 'http://spryli.com/'+newUrl);
				twttr.widgets.load();
				$('.newItemViewLinks').show();
				$('.zeroResult').hide();	
				$('.noItems').remove();				
            },
            error: function () {
                $("#alerts").html("<h3>An error has occured, please try again later.</h3>");
            }
        })
    });
	
	
	$("#saveExistingVideoButton").click(function () {
        $("#saveExistingVideoButton").attr("class", "btn disabled");
        $("#saveExistingVideoButton").append('<img src="../img/saving.gif"/>');
        var key = $("#key").val();
		var contentId = $('.item-selected').attr('id');
        a = $(".videoContentDiv").val();
        a = {
            tag: "saveContent",
			contentId: contentId, 
            contentData: a, 
			type: "video"
        };
        $.ajax({
            type: "POST",
            dataType: "json",
            url: "../system/AJAX/ajaxHandler.php",
            data: {
                jsonObj: a
            },
            success: function (a) {
                "SUCCESS" != eval(a).status && $("#alerts").html("<h3>An error has occured, please try again later.</h3>");
				if(eval(a).status == "SUCCESS"){
					$("#saveExistingVideoButton").removeClass("disabled");
					$("#saveExistingVideoButton").addClass("btn-success");
					$("#saveExistingVideoButton").html("Save");
					$('.videoPreviewDiv').html('<div class="well"> <h1> Your video has been updated!</h1> </div>');
				}				
            },
            error: function () {
                $("#alerts").html("<h3>An error has occured, please try again later.</h3>");
            }
        })
    });
	
	$('.doneFeedback').click(function(){
		var type = "feedback";
		var text = $('#feedbackMessage').val();
		a = {
            tag: "logItem",
			type: type, 
			text: text
        };
		$('#feedbackForm').html('<div style="padding:10px"><img src="../img/ajax-loader.gif" alt="Please Wait"/> <span>Sending Feedback..</span></div>');
        $.ajax({
            type: "POST",
            dataType: "json",
            url: "../system/AJAX/ajaxHandler.php",
            data: {
                jsonObj: a
            },
            success: function (a) {
                "SUCCESS" != eval(a).status && $("#feedbackForm").html("<h3>An error has occured, please try again later.</h3>");
				"SUCCESS" == eval(a).status && $("#feedbackForm").html("<h3>"+eval(a).message+"</h3>");
            },
            error: function () {
                $("#feedbackForm").html("<h3>An error has occured, please try again later.</h3>");
            }
        })
	});
	
	$('.doneSupport').click(function(){
		var type = "support";
		var text = $('#supportMessage').val();
		a = {
            tag: "logItem",
			type: type, 
			text: text
        };
		$('#supportForm').html('<div style="padding:10px"><img src="../img/ajax-loader.gif" alt="Please Wait"/> <span>Sending Support Request..</span></div>');
        $.ajax({
            type: "POST",
            dataType: "json",
            url: "../system/AJAX/ajaxHandler.php",
            data: {
                jsonObj: a
            },
            success: function (a) {
                "SUCCESS" != eval(a).status && $("#supportForm").html("<h3>An error has occured, please try again later.</h3>");
				"SUCCESS" == eval(a).status && $("#supportForm").html("<h3>"+eval(a).message+"</h3>");
            },
            error: function () {
                $("#supportForm").html("<h3>An error has occured, please try again later.</h3>");
            }
        })
	});
	
	$('#frmImgUpload').on('submit', function(e) {
		var contentName = $('#newItemName').val();
		if (contentName == ''){
			alert('Please enter a name for this content');
			$('#start').addClass('active');
			$('#content').removeClass('active');
			return false;
		}
				e.preventDefault();
				//show uploading message
				$("#uploadStatus").html('<div style="padding:10px"><img src="../img/ajax-loader.gif" alt="Please Wait"/> <span>Uploading...</span></div>');
					console.log('submit clicked!');
				$(this).ajaxSubmit({
					target: '#uploadStatus',
					dataType: "json",
					data: {
						contentName: contentName
					},
					success:   function(data) { 
						console.log('data is: ' + data);
						$("#uploadStatus").html('<br /> <span><h2> Done! Click "ViewIt"! </h2></span>');
						var newItemId = eval(data).contentId;
						var newUrl = eval(data).newUrl;
						$('.items').append('<a href="#" id="'+newItemId+'" class="itemSpecial btn btn-rounded btn-info existingItem">'+contentName+'</a>');
						$('#newItemQRCode').attr('src', 'https://chart.googleapis.com/chart?cht=qr&chs=250x250&chl=http://spryli.com/'+newUrl+'');
						$('#newItemURL').attr('href', 'http://spryli.com/'+newUrl);
						$('#newItemURL').html('http://spryli.com/'+newUrl);
						$('#newItemTweet').attr('data-url', 'http://spryli.com/'+newUrl);
						twttr.widgets.load()
						$('.newItemViewLinks').show();
						$('.zeroResult').hide();
						$('.noItems').remove();
						$('#myTab li').removeClass("active");
						$('#myTab li:has(a[href="#view"])').addClass('active');
						$('#content').removeClass('active');
						$('#view').addClass('active');
					}
				});
			});
			
			
			

$(function () {
    var a = $("[title=Font]").siblings(".dropdown-menu");
    $.each("Serif;Sans;Arial;Arial Black;Courier;Courier New;Comic Sans MS;Helvetica;Impact;Lucida Grande;Lucida Sans;Tahoma;Times;Times New Roman;Verdana".split(";"), function (d, b) {
        a.append($('<li><a data-edit="fontName ' + b + '" style="font-family:\'' + b + "'\">" + b + "</a></li>"))
    });
    $("a[title]").tooltip({
        container: "body"
    });
    $(".dropdown-menu input").click(function () {
        return !1
    }).change(function () {
        $(this).parent(".dropdown-menu").siblings(".dropdown-toggle").dropdown("toggle")
    }).keydown("esc",
        function () {
            this.value = "";
            $(this).change()
        });
    $("[data-role=magic-overlay]").each(function () {
        var a = $(this),
            b = $(a.data("target"));
        a.css("opacity", 0).css("position", "absolute").offset(b.offset()).width(b.outerWidth()).height(b.outerHeight())
    });
    if ("onwebkitspeechchange" in document.createElement("input")) {
        var c = $("#editor").offset();
        $("#voiceBtn").css("position", "absolute").offset({
            top: c.top,
            left: c.left + $("#editor").innerWidth() - 35
        })
    } else $("#voiceBtn").hide();
    $("#editor").wysiwyg({
        fileUploadError: function (a,
            b) {
            var c = "";
            "unsupported-file-type" === a ? c = "Unsupported format " + b : console.log("error uploading file", a, b);
            $('<div class="alert"> <button type="button" class="close" data-dismiss="alert">&times;</button><strong>File upload error</strong> ' + c + " </div>").prependTo("#alerts")
        }
    });
    window.prettyPrint && prettyPrint()
});
});