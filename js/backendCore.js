$(document).ready(function () {
    $(".doneNewItem").click(function () {
        $("#saveButton").attr("class", "btn disabled");
        $("#saveButton").append('<img src="img/saving.gif"/>');
        var key = $("#key").val();
        var contentName = $('#contentName').val();
		if (contentName == ''){
			alert('Please enter a name for this content');
			return false;
		}
        a = $("#editor").html();
        a = {
            tag: "createContent",
            contentId: key,
			contentName: contentName, 
            contentData: a
        };
        $.ajax({
            type: "POST",
            dataType: "json",
            url: "system/AJAX/ajaxHandler.php",
            data: {
                jsonObj: a
            },
            success: function (a) {
                "SUCCESS" != eval(a).status && $("#alerts").html("<h3>An error has occured, please try again later.</h3>");
                $("#saveButton").removeClass("disabled");
                $("#saveButton").addClass("btn-success");
                $("#saveButton").html("Save")
            },
            error: function () {
                $("#alerts").html("<h3>An error has occured, please try again later.</h3>")
            }
        })
    })
});
$("#saveButton").click(function () {
        $("#saveButton").attr("class", "btn disabled");
        $("#saveButton").append('<img src="img/saving.gif"/>');
        var key = $("#key").val();
		var contentName = $('#contentName').val();
		if (contentName == ''){
			alert('Please enter a name for this content');
			return false;
		}
        a = $("#editor").html();
        a = {
            tag: "createContent",
            contentId: key,
			contentName: contentName, 
            contentData: a
        };
        $.ajax({
            type: "POST",
            dataType: "json",
            url: "system/AJAX/ajaxHandler.php",
            data: {
                jsonObj: a
            },
            success: function (a) {
                "SUCCESS" != eval(a).status && $("#alerts").html("<h3>An error has occured, please try again later.</h3>");
                $("#saveButton").removeClass("disabled");
                $("#saveButton").addClass("btn-success");
                $("#saveButton").html("Save")
            },
            error: function () {
                $("#alerts").html("<h3>An error has occured, please try again later.</h3>")
            }
        })
    });
	
	$('#frmImgUpload').on('submit', function(e) {
		console.log("submitted!");
				e.preventDefault();
				//show uploading message
				$("#uploadStatus").html('<div style="padding:10px"><img src="img/ajax-loader.gif" alt="Please Wait"/> <span>Uploading...</span></div>');
					console.log('submit clicked!');
				$(this).ajaxSubmit({
					target: '#uploadStatus',
					success:   function(data) { 
						$("#uploadStatus").html('<br /> <span><h2> Done! Click "ViewIt"! </h2></span>');
					}
				});
			});
			
			
			

$(function () {
    var a = $("[title=Font]").siblings(".dropdown-menu");
    $.each("Serif;Sans;Arial;Arial Black;Courier;Courier New;Comic Sans MS;Helvetica;Impact;Lucida Grande;Lucida Sans;Tahoma;Times;Times New Roman;Verdana".split(";"), function (d, b) {
        a.append($('<li><a data-edit="fontName ' + b + '" style="font-family:\'' + b + "'\">" + b + "</a></li>"))
    });
    $("a[title]").tooltip({
        container: "body"
    });
    $(".dropdown-menu input").click(function () {
        return !1
    }).change(function () {
        $(this).parent(".dropdown-menu").siblings(".dropdown-toggle").dropdown("toggle")
    }).keydown("esc",
        function () {
            this.value = "";
            $(this).change()
        });
    $("[data-role=magic-overlay]").each(function () {
        var a = $(this),
            b = $(a.data("target"));
        a.css("opacity", 0).css("position", "absolute").offset(b.offset()).width(b.outerWidth()).height(b.outerHeight())
    });
    if ("onwebkitspeechchange" in document.createElement("input")) {
        var c = $("#editor").offset();
        $("#voiceBtn").css("position", "absolute").offset({
            top: c.top,
            left: c.left + $("#editor").innerWidth() - 35
        })
    } else $("#voiceBtn").hide();
    $("#editor").wysiwyg({
        fileUploadError: function (a,
            b) {
            var c = "";
            "unsupported-file-type" === a ? c = "Unsupported format " + b : console.log("error uploading file", a, b);
            $('<div class="alert"> <button type="button" class="close" data-dismiss="alert">&times;</button><strong>File upload error</strong> ' + c + " </div>").prependTo("#alerts")
        }
    });
    window.prettyPrint && prettyPrint()
});