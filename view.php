<?php 
require('navigation.php');
	require_once 'system/Analytics/Mobile_Detect.php';
	require('system/dbconnect.php');
	require('system/Content/contentClass.php');
	require('system/Analytics/analyticsClass.php');
    $request = substr($_SERVER[REQUEST_URI], 1);
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>spryli &middot; Simple QR Codes</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">

<link rel="icon" type="image/png" href="img/favicon.ico">
<link href='http://fonts.googleapis.com/css?family=Lato:400,700,300' rel='stylesheet' type='text/css'>
<!--[if IE]>
	<link href="http://fonts.googleapis.com/css?family=Lato" rel="stylesheet" type="text/css">
	<link href="http://fonts.googleapis.com/css?family=Lato:400" rel="stylesheet" type="text/css">
	<link href="http://fonts.googleapis.com/css?family=Lato:700" rel="stylesheet" type="text/css">
	<link href="http://fonts.googleapis.com/css?family=Lato:300" rel="stylesheet" type="text/css">
<![endif]-->

<link href="css/bootstrap.css" rel="stylesheet">
<link href="css/font-awesome.min.css" rel="stylesheet">
<link href="css/theme.css" rel="stylesheet">
<link href="css/photoswipe.css" rel="stylesheet">
<!--[if lt IE 9]>
<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<!--[if IE 7]>
<link rel="stylesheet" href="css/font-awesome-ie7.min.css">
<![endif]-->
</head>

<body>
<!--header-->
	<div class="header ">
		<!--logo-->
			<div class="container">
					<div class="logo">
						 <a href="index"><img src="img/spryli.png" alt="" class="animated bounceInDown" /></a>  
					</div>
					<!--menu-->
					<nav id="main_menu">
					<div class="menu_wrap">
						<ul class="nav sf-menu">
							<li class="active"><a href="index">Curious?</a> </li>
								
						</ul>
					</div>
				</nav>
			</div>
		</div>
	<!--//header-->
	<!--page-->

		<!-- /SLIDER -->
		
	<div id="banner">
	<div class="container intro_wrapper">
	<div class="inner_content">
	
	<!--welcome-->
		<div class="welcome_index_special">
		

			
          <?php 
			$detect = new Mobile_Detect;
			$content = new Content();
			$analytics = new Analytics();
			 $result = $content->retrieveContent($content->base64_url_decode($request));
			 if($result['status'] != "OVERLIMIT"){
				$deviceType = ($detect->isMobile() ? ($detect->isTablet() ? 'tablet' : 'phone') : 'computer');
				$analytics->logView($content->base64_url_decode($request), $deviceType);
			 }
			 
			 if($result['type'] == "image"){
				echo '<ul id="Gallery">';
				echo '<li>';
				echo $result['content'];
				echo '</li>';
				echo '</ul>';
			 }
			 
			 else if($result['type'] == "text"){
				echo '<div id="changeFont" class="pull-right">
					<a href="#" class="increaseFont icon-zoom-in" style="font-size: 24px;">Bigger </a>
					<a href="#" class="decreaseFont icon-zoom-out" style="font-size: 24px;">Smaller</a>
					</div>';
				echo $result['content'];
			 }
			 
			 else if($result['type'] == "video"){
				echo '<div class="span8 centerVideo" style="min-height: 480px; height: 480px;">';
				echo $result['content'];
				echo '</div>';
			 }
			 else {
				echo $result['content'];
			
			}

		 
		
 
		  ?>
        
			
		</div>

		</div>
	<!--//welcome-->
		</div>
			</div>
				</div>
				<!--//banner-->
			
	<div class="container wrapper">
	<div class="inner_content">
	<div class="pad45"></div>
	
	<!-- Demo -->
	<!-- END DEMO -->
	<!--info boxes-->
	<!--//info boxes-->
	</div>
		<!--//page-->
		
		<div class="pad25 hidden-desktop"></div>
	</div>
	
	<!-- footer -->
	
	
	<!-- footer 2 -->
	
						
				<!-- up to top -->
				<a href="#"><i class="go-top hidden-phone hidden-tablet  icon-double-angle-up"></i></a>
				<!--//end-->
				
<script src="js/jquery.js"></script>			
<script src="js/bootstrap.min.js"></script>	
<script type="text/javascript" src="js/viewScripts.js"></script>
<script type="text/javascript" src="js/klass.min.js"></script> 

<script>
$(document).ready(function(){
  
  // Reset Font Size
  var originalFontSize = $('.welcome_index_special').css('font-size');
    $(".resetFont").click(function(){
    $('.welcome_index_special').css('font-size', originalFontSize);
  });
  // Increase Font Size
  $(".increaseFont").click(function(){
  console.log("Increasing Font");
    var currentFontSize = $('.welcome_index_special').css('font-size');
    var currentFontSizeNum = parseFloat(currentFontSize, 10);
    var newFontSize = currentFontSizeNum*1.2;
    $('.welcome_index_special').css('font-size', newFontSize);
    return false;
  });
  // Decrease Font Size
  $(".decreaseFont").click(function(){
   console.log("Decreasing Font");
    var currentFontSize = $('.welcome_index_special').css('font-size');
    var currentFontSizeNum = parseFloat(currentFontSize, 10);
    var newFontSize = currentFontSizeNum*0.8;
    $('.welcome_index_special').css('font-size', newFontSize);
    return false;
  });
  var myPhotoSwipe = $("#Gallery a").photoSwipe({ enableMouseWheel: false , enableKeyboard: false }); 
});
</script>
<script type="text/javascript" src="js/code.photoswipe.jquery-3.0.5.min.js"></script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-42810904-1', 'spryli.com');
  ga('send', 'pageview');

</script>
</body>
</html>