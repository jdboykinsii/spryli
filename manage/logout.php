<?php
require("../system/dbconnect.php");
require("../system/Content/contentClass.php");
require("../system/Analytics/analyticsClass.php");
require_once('../system/Sessions/SessionHandler.php');

$session = new SessionHandler();

session_start();
if(!isset($_SESSION['userId'])){
	header("Location: ../login.php");
}
session_destroy();
header("Location: ../login.php");
?>
