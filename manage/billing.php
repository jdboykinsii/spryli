<?php 

require("../system/dbconnect.php");
require("../system/Content/contentClass.php");
require("../system/Analytics/analyticsClass.php");
require_once('../system/Sessions/SessionHandler.php');
require("../system/User/userClass.php");
require("../system/Billing/billingClass.php");
$session = new SessionHandler();
session_start();
$user = new User();
$analytics = new Analytics();
$content = new Content();
$billing = new Billing();
$randomString = $content->random_string();
$userBillingPlan = $billing->getUserPlanId($_SESSION['userId']);
$billingPlanArray = $billing->getPlanDetails($userBillingPlan);
$session = new SessionHandler();
session_start();

if(!isset($_SESSION['userId'])) {
	header('Location: ../login.php');
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>spryli &middot; Simple QR Codes</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" type="image/png" href="img/favicon.ico">

<link href='http://fonts.googleapis.com/css?family=Lato:400,700,300' rel='stylesheet' type='text/css'>
<!--[if IE]>
	<link href="http://fonts.googleapis.com/css?family=Lato" rel="stylesheet" type="text/css">
	<link href="http://fonts.googleapis.com/css?family=Lato:400" rel="stylesheet" type="text/css">
	<link href="http://fonts.googleapis.com/css?family=Lato:700" rel="stylesheet" type="text/css">
	<link href="http://fonts.googleapis.com/css?family=Lato:300" rel="stylesheet" type="text/css">
<![endif]-->

<link href="../css/bootstrap.css" rel="stylesheet">
<link href="../css/font-awesome.min.css" rel="stylesheet">
<link href="../css/theme.css" rel="stylesheet">
<link href="../css/prettyPhoto.css" rel="stylesheet" type="text/css"/>
<link href="../css/zocial.css" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" href="../css/nerveslider.css">
<link href="../css/wysiwyg.css" rel="stylesheet">
<link href="../css/iconStyle.css" rel="stylesheet">
<link href="css/manageStyle.css" rel="stylesheet">
<!--[if lt IE 9]>
<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<!--[if IE 7]>
<link rel="stylesheet" href="../css/font-awesome-ie7.min.css">
<![endif]-->

</head>

<body>
<!--header-->
	<div class="header ">
		<!--logo-->
			<div class="container">
					<div class="logo">
						 <a href="index.php"><img src="../img/spryli.png" alt="" class="animated bounceInDown" /></a>  
					</div>
					<!--menu-->
					<nav id="main_menu">
					<div class="menu_wrap">
						<ul class="nav sf-menu">
							<li><a href="" class="whoami"><?php echo $user->retrieveUserEmail($_SESSION['userId']); ?></a> </li>
							<li class=""><a href="index.php">Dashboard</a> </li>
							<li class="active"><a href="" class="navItem" id="Billing">Billing</a> </li>
							<li> <a href="#support" role="button" data-toggle="modal">Support</a></li>
							<li class="last"><a href="logout">Logout</a></li>				
						</ul>
					</div>
				</nav>
			</div>
		</div>
	<!--//header-->
	<!--page-->

	
			
	<div class="container wrapper">
	<div class="inner_content">
	
	<div class="row">
	
	<span><h1 class="text-center"> Billing </h1></span>
	</div>
	<div class="pad45"></div>
	<!-- main container -->
	<div class="row">
	<div class="span12">
	<div class="span4 sidebar well">
		<h2 class="white"> Your Plan: <?php echo $billingPlanArray['planName'] . " - " . $billingPlanArray['price'] . ' ' . $billingPlanArray['interval']; ?> </h2> 
		<br>
		<span class="viewCount white"><h3> <?php $views = $analytics->getViewCount(); echo $views['views'] . "/" . $views['limit']; ?> Views </h3></span>
		<br> 
		<a href="#feedback" role="button" class="btn btn-success btn-rounded btn-medium" data-toggle="modal">Feedback?</a>
	</div>
	<div class="span6">
		
		
	</div>
		
	</div>
	</div>
	<hr>
	</div>
	<div class="row mainContainer">
	<div class="span12">
	<h2> Cancel Account </h2>
		<button class="btn btn-danger btn-rounded cancelAccount" id="<?php echo $_SESSION['userId'];?>">Cancel Account </button>
	</div>
	</div>
	<!--info boxes-->
	
						
			<!--//info boxes-->
	</div>
		<!--//page-->
		
		<div class="pad25 hidden-desktop"></div>
	</div>
	
	<!-- footer -->
						
<div id="feedback" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="feedback" aria-hidden="true">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×
</button>
    <h3 id="feedkback">How can we make <span>spryli</span> better?</h3>
  </div>
  <div class="modal-body">
  <div id="feedbackForm">
		<p class="form_info">message</p>
		<textarea name="message" id="feedbackMessage" class="span5"></textarea>
	</div>
	</div>
  <div class="modal-footer">
	<button class="btn btn-success btn-rounded btn-large doneFeedback" aria-hidden="true">Submit</button>
    <button class="btn btn-danger btn-rounded btn-large" data-dismiss="modal" aria-hidden="true">
Cancel</button>
   </div>
</div>				

<div id="support" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="support" aria-hidden="true">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×
</button>
    <h3 id="support">How can we help you?</h3>
  </div>
  <div class="modal-body">
  <div id="supportForm">
		<p class="form_info">message</p>
		<textarea name="message" id="supportMessage" class="span5"></textarea>
	</div>
	</div>
  <div class="modal-footer">
	<button class="btn btn-success btn-rounded btn-large doneSupport" aria-hidden="true">Submit</button>
    <button class="btn btn-danger btn-rounded btn-large" data-dismiss="modal" aria-hidden="true">
Cancel</button>
   </div>
</div>						
	
	<!-- footer 2 -->
	<div id="footer2">
		<div class="container">
			<div class="row">
				<div class="span12">
				<div class="copyright">
							spryli
							&copy;
							<script type="text/javascript">
							//<![CDATA[
								var d = new Date()
								document.write(d.getFullYear())
								//]]>
								</script>
							 - All Rights Reserved
						</div>
						</div>
					</div>
				</div>
					</div>
					

				
				
<script src="../js/jquery.js"></script>			
<script src="../js/bootstrap.min.js"></script>	
<script src="../js/jquery.touchSwipe.min.js"></script>
<script src="../js/jquery.mousewheel.min.js"></script>				
<script src="../js/superfish.js"></script>
<script type="text/javascript" src="../js/jquery.prettyPhoto.js"></script>
<script type="text/javascript" src="../js/scripts.js"></script>
<!-- carousel -->
<script type="text/javascript" src="../js/jquery.carouFredSel-6.2.1-packed.js"></script>
<script src="../js/jquery-ui.min.js"></script>
<script type="text/javascript" src="../js/jquery.nerveSlider.min.js"></script>
<script type="text/javascript" src="../js/bootstrap-wysiwyg.js"></script>
<script type="text/javascript" src="https://mindmup.s3.amazonaws.com/lib/jquery.hotkeys.js"></script>
<script type="text/javascript" src="../js/storage.js"></script>
<script type="text/javascript" src="../js/jquery.form.js"></script>
<script type="text/javascript" src="../js/billing.js"></script>
<script type="text/javascript" src="../js/manageCore.js"></script>
</body>
</html>