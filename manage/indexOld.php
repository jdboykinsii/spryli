<?php 

require("../system/dbconnect.php");
require("../system/Content/contentClass.php");
require("../system/Analytics/analyticsClass.php");
$analytics = new Analytics();
$content = new Content();
$randomString = $content->random_string();


?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>spryli &middot; Sharing Made Viral</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" type="image/png" href="img/favicon.ico">

<link href='http://fonts.googleapis.com/css?family=Lato:400,700,300' rel='stylesheet' type='text/css'>
<!--[if IE]>
	<link href="http://fonts.googleapis.com/css?family=Lato" rel="stylesheet" type="text/css">
	<link href="http://fonts.googleapis.com/css?family=Lato:400" rel="stylesheet" type="text/css">
	<link href="http://fonts.googleapis.com/css?family=Lato:700" rel="stylesheet" type="text/css">
	<link href="http://fonts.googleapis.com/css?family=Lato:300" rel="stylesheet" type="text/css">
<![endif]-->

<link href="../css/bootstrap.css" rel="stylesheet">
<link href="../css/font-awesome.min.css" rel="stylesheet">
<link href="../css/theme.css" rel="stylesheet">
<link href="../css/prettyPhoto.css" rel="stylesheet" type="text/css"/>
<link href="../css/zocial.css" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" href="../css/nerveslider.css">
<link href="../css/wysiwyg.css" rel="stylesheet">
<link href="../css/iconStyle.css" rel="stylesheet">

<!--[if lt IE 9]>
<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<!--[if IE 7]>
<link rel="stylesheet" href="../css/font-awesome-ie7.min.css">
<![endif]-->
<style>
.textControlsDiv {
					display: none;
				}
.pictureControlsDiv {
					display: none;
				}				
	.glyph:hover {
		-webkit-transition: all 0.5s ease-out;
		background:#222821;
		color:#fff;
		text-shadow: none;
		-moz-text-shadow: none;
		-webkit-text-shadow: none;
		top:-5px;
		position:relative;
	}
	.whoami{
		margin-left: -150px;
		}
	.mainContainer {
		min-height: 685px;
		}
	.sidebar{
		background: #323A45;
		padding: 20px;
	}
	.itemSpecial {
		margin-top: 10px;
		margin-bottom: 10px;
		font-size: 1.5em;
	}
	.items{
		min-height: 100px;
	}
	
	.countIcon {
		font-size: 64px;
		padding-right: 20px;
		
		}
	.numberCount {
		color: #f89406;
		font-weight: bold;
	}
	
	.analytics {
		background: #FFF;
		border: 2px #323A45 solid;
		
		}
		

</style>
</head>

<body>
<!--header-->
	<div class="header ">
		<!--logo-->
			<div class="container">
					<div class="logo">
						 <a href="index"><img src="../img/spryli.png" alt="" class="animated bounceInDown" /></a>  
					</div>
					<!--menu-->
					<nav id="main_menu">
					<div class="menu_wrap">
						<ul class="nav sf-menu">
							<li><a href="" class="whoami">jdboykinsii@gmail.com</a> </li>
							<li class="active"><a href="">Dashboard</a> </li>
							<li class=""><a href="" class="navItem" id="Billing">Billing</a> </li>
							<li class=""><a href="" class="navItem" id="settings">Settings</a> </li>
							<li class="last"><a href="logout">Logout</a></li>				
						</ul>
					</div>
				</nav>
			</div>
		</div>
	<!--//header-->
	<!--page-->

	
			
	<div class="container wrapper">
	<div class="inner_content">
	
	<div class="row">
	
	<span><h1 class="text-center"> Welcome to the admin panel! </h1></span>
	</div>
	<div class="pad45"></div>
	<!-- main container -->
	<div class="row mainContainer">
	<div class="span2 sidebar rounded">
	<span class="viewCount white"><h2> 300/500 Views </h2></span>
	<a href="#startHere" role="button" class="btn btn-success btn-rounded btn-large" data-toggle="modal" >New &nbsp; <i class="icon-plus"> </i> </a> 
	<hr>
	<br />
	<h3 class="white"> Your Content: </h3>
	<ul class="items">
	<?php $content->getContentByUserId("TESTUSER"); ?>
	</ul>
	<br />
	<hr>
	<button class="btn btn-success btn-rounded btn-medium">How can we improve?</button>
	</div>
	
	<div class="span8 well analytics">
		
            <div class="tabbable tabs-top">
			 <ul id="myTab" class="nav nav-tabs">
              <li class="active"><a href="#time" data-toggle="tab">Views By Time</a></li>
              <li class=""><a href="#device" data-toggle="tab">Views By Device</a></li>
			</ul>
            <div id="myTabContent" class="tab-content">
              <div class="tab-pane active" id="time" style="min-height: 250px;">
                 <div id="checkin_chart" style="width: 700px; height: 220px;">
              <div class="progress progress-striped active">
                <div class="bar" style="width: 70%;"></div>
              </div>
          </div>
              </div>
              <div class="tab-pane fade" id="device">
			  <div class="span2">
			  <h2 class="text-center"> Phone </h2>
					<i class="icon-mobile-phone countIcon" > <span class="numberCount"> 12 </span></i>
					</div>
					<div class="span2">
					<h2 class="text-center"> Tablet </h2>
					<i class="icon-tablet countIcon" > <span class="numberCount">32</span></i>
					</div>
					<div class="span2">
					<h2 class="text-center"> Computer </h2>
					<i class="icon-desktop countIcon"> <span class="numberCount"> 64</span> </i>
					</div>
				</div>
             </div>
          </div>
	</div>
	
	<div class="span8">
	<div class="row demo">
		<div class="span8 well pictureControlsDiv">
		<h1 class="text-center"> What image would you like to share? </h1>
		<ul class="nav nav-tabs" id="tabs">
			<li class="active">
				<a href="#upload" data-toggle="tab">Edit</a>
			</li>
			<li>
				<a href="#view" class="" data-toggle="tab">View It</a>
			</li>
		</ul>
		<div class="tab-content">
			<div class="tab-pane active slide in text-center" id="upload">
			<div id="uploadStatus"> </div>
			<br />
			<form method="post" action="manage/processpictureuploads.php" enctype="multipart/form-data" id="frmImgUpload">
    <input name="ImageFile" class="btn btn-info btn-rounded btn-large" type="file" accept="image/*" multiple="false" />
	<input type="hidden" name="keyId" id="key" value="<?php echo $randomString ?>">
    <br />
	<br />
    <input name="btnSubmit" class="btn btn-success btn-rounded btn-large" type="submit" value="Upload" />
</form>
</div>



<div class="tab-pane slide bg-color-white" id="view">
			
			<div class="span4 offset1">
			<h3> Scan this QR code:</h3>
				<img id="demoQR" src="https://chart.googleapis.com/chart?cht=qr&amp;chs=250x250&amp;chl=http://spryli.com/<?php echo $content->base64_url_encode($randomString) ?>">
			</div>
			<div class="span4 offset1">
			<h3> Or go to this link:</h3>
				<h2><a href="http://spryli.com/<?php echo $content->base64_url_encode($randomString) ?>" target="_blank">spryli.com/<?php echo $content->base64_url_encode($randomString) ?></a></h2>
			</div>
			
			</div>
		</div>
		</div>
        <div class="span8 well textControlsDiv">
		<h1 class="text-center"> What text would you like to share? </h1>
        	     <div class="row-fluid" id="controlsContainer">
        <div class="">
          <ul class="nav nav-tabs" id="tabs">
			<li class="active">
				<a href="#edit" data-toggle="tab">Edit</a>
			</li>
			<li>
				<a href="#qr-code" class="viewIt" data-toggle="tab">View It</a>
			</li>
		</ul>

		<div class="tab-content">
			<div class="tab-pane active slide in" id="edit">
			
	<div id="alerts"></div>
	<div id="editorMain">
	
    <div class="btn-toolbar" data-role="editor-toolbar" data-target="#editor">
      <div class="btn-group">
        <a class="btn dropdown-toggle" data-toggle="dropdown" title="" data-original-title="Font"><i class="icon-font"></i><b class="caret"></b></a>
          <ul class="dropdown-menu">
          <li><a data-edit="fontName Serif" style="font-family:'Serif'">Serif</a></li><li><a data-edit="fontName Sans" style="font-family:'Sans'">Sans</a></li><li><a data-edit="fontName Arial" style="font-family:'Arial'">Arial</a></li><li><a data-edit="fontName Arial Black" style="font-family:'Arial Black'">Arial Black</a></li><li><a data-edit="fontName Courier" style="font-family:'Courier'">Courier</a></li><li><a data-edit="fontName Courier New" style="font-family:'Courier New'">Courier New</a></li><li><a data-edit="fontName Comic Sans MS" style="font-family:'Comic Sans MS'">Comic Sans MS</a></li><li><a data-edit="fontName Helvetica" style="font-family:'Helvetica'">Helvetica</a></li><li><a data-edit="fontName Impact" style="font-family:'Impact'">Impact</a></li><li><a data-edit="fontName Lucida Grande" style="font-family:'Lucida Grande'">Lucida Grande</a></li><li><a data-edit="fontName Lucida Sans" style="font-family:'Lucida Sans'">Lucida Sans</a></li><li><a data-edit="fontName Tahoma" style="font-family:'Tahoma'">Tahoma</a></li><li><a data-edit="fontName Times" style="font-family:'Times'">Times</a></li><li><a data-edit="fontName Times New Roman" style="font-family:'Times New Roman'">Times New Roman</a></li><li><a data-edit="fontName Verdana" style="font-family:'Verdana'">Verdana</a></li></ul>
        </div>
      <div class="btn-group">
        <a class="btn dropdown-toggle" data-toggle="dropdown" title="" data-original-title="Font Size"><i class="icon-text-height"></i>&nbsp;<b class="caret"></b></a>
          <ul class="dropdown-menu">
          <li><a data-edit="fontSize 5"><font size="5">Huge</font></a></li>
          <li><a data-edit="fontSize 3"><font size="3">Normal</font></a></li>
          <li><a data-edit="fontSize 1"><font size="1">Small</font></a></li>
          </ul>
      </div>
      <div class="btn-group">
        <a class="btn" data-edit="bold" title="" data-original-title="Bold (Ctrl/Cmd+B)"><i class="icon-bold"></i></a>
        <a class="btn" data-edit="italic" title="" data-original-title="Italic (Ctrl/Cmd+I)"><i class="icon-italic"></i></a>
        <a class="btn" data-edit="strikethrough" title="" data-original-title="Strikethrough"><i class="icon-strikethrough"></i></a>
        <a class="btn" data-edit="underline" title="" data-original-title="Underline (Ctrl/Cmd+U)"><i class="icon-underline"></i></a>
      </div>
      <div class="btn-group">
        <a class="btn btn-info" data-edit="insertunorderedlist" title="" data-original-title="Bullet list"><i class="icon-list-ul"></i></a>
        <a class="btn" data-edit="insertorderedlist" title="" data-original-title="Number list"><i class="icon-list-ol"></i></a>
        <a class="btn" data-edit="outdent" title="" data-original-title="Reduce indent (Shift+Tab)"><i class="icon-indent-left"></i></a>
        <a class="btn" data-edit="indent" title="" data-original-title="Indent (Tab)"><i class="icon-indent-right"></i></a>
      </div>
      <div class="btn-group">
        <a class="btn btn-info" data-edit="justifyleft" title="" data-original-title="Align Left (Ctrl/Cmd+L)"><i class="icon-align-left"></i></a>
        <a class="btn" data-edit="justifycenter" title="" data-original-title="Center (Ctrl/Cmd+E)"><i class="icon-align-center"></i></a>
        <a class="btn" data-edit="justifyright" title="" data-original-title="Align Right (Ctrl/Cmd+R)"><i class="icon-align-right"></i></a>
        <a class="btn" data-edit="justifyfull" title="" data-original-title="Justify (Ctrl/Cmd+J)"><i class="icon-align-justify"></i></a>
      </div>
      <div class="btn-group">
		  <a class="btn dropdown-toggle" data-toggle="dropdown" title="" data-original-title="Hyperlink"><i class="icon-link"></i></a>
		    <div class="dropdown-menu input-append">
			    <input class="span2" placeholder="URL" type="text" data-edit="createLink">
			    <button class="btn" type="button">Add</button>
        </div>
        <a class="btn" data-edit="unlink" title="" data-original-title="Remove Hyperlink"><i class="icon-cut"></i></a>

      </div>
      
      <div class="btn-group">
        <a class="btn" data-edit="undo" title="" data-original-title="Undo (Ctrl/Cmd+Z)"><i class="icon-undo"></i></a>
        <a class="btn" data-edit="redo" title="" data-original-title="Redo (Ctrl/Cmd+Y)"><i class="icon-repeat"></i></a>
      </div>
	  <input type="hidden" name="keyId" id="key" value="<?php echo $randomString ?>">
	   <button class="btn btn-success" id="saveButton">Save</button>
 
     </div>

    <div id="editor" contenteditable="true">
  <p>What would you like to say?</p>
    </div>
	</div>
			</div>
			<div class="tab-pane slide bg-color-white" id="qr-code">
			
			<div class="span4 offset1">
			<h3> Scan this QR code:</h3>
				<img id="demoQR" src="https://chart.googleapis.com/chart?cht=qr&amp;chs=250x250&amp;chl=http://spryli.com/<?php echo $content->base64_url_encode($randomString) ?>">
			</div>
			<div class="span4 offset1">
			<h3> Or go to this link:</h3>
				<h2><a href="http://spryli.com/<?php echo $content->base64_url_encode($randomString) ?>" target="_blank">spryli.com/<?php echo $content->base64_url_encode($randomString) ?></a></h2>
			</div>
			
			</div>
		</div>

        </div>
        
      </div>
        </div>
      </div>
	<!-- END DEMO -->
	
	</div>
	</div>
	
	<!--info boxes-->
	
						
			<!--//info boxes-->
	</div>
		<!--//page-->
		
		<div class="pad25 hidden-desktop"></div>
	</div>
	
	<!-- footer -->
	
	
	<!-- footer 2 -->
	<div id="footer2">
		<div class="container">
			<div class="row">
				<div class="span12">
				<div class="copyright">
							spryli
							&copy;
							<script type="text/javascript">
							//<![CDATA[
								var d = new Date()
								document.write(d.getFullYear())
								//]]>
								</script>
							 - All Rights Reserved
						</div>
						</div>
					</div>
				</div>
					</div>
					
<div id="startHere" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×
</button>
    <h3 id="myModalLabel">Let's get started!</h3>
  </div>
  <div class="modal-body highlight-blue">
		<h1 class="white text-center">Pick a name: </h1>
		<input class="span5" style="margin-left: auto; margin-right: auto; display:block;" type="text" id="newItemName" value="">
		<h1 class="white text-center"> What would you like to share? </h1>
		<a href="#" class="icon-file-alt demoIcon span1 offset1" style="font-size: 7em; color: #FFF;"></a>
		<a href="#" class="icon-picture demoIcon span1 offset1" style="font-size: 7em; color: #FFF;"></a>
		
		
	</div>
  <div class="modal-footer">
	<button class="btn btn-success btn-rounded btn-large finishNewItem" data-dismiss="modal" aria-hidden="true">Go!</button>
    <button class="btn btn-danger btn-rounded btn-large" data-dismiss="modal" aria-hidden="true">
Cancel</button>
   </div>
</div>						
				
				
<script src="../js/jquery.js"></script>			
<script src="../js/bootstrap.min.js"></script>	
<script src="../js/jquery.touchSwipe.min.js"></script>
<script src="../js/jquery.mousewheel.min.js"></script>				
<script src="../js/superfish.js"></script>
<script type="text/javascript" src="../js/jquery.prettyPhoto.js"></script>
<script type="text/javascript" src="../js/scripts.js"></script>
<!-- carousel -->
<script type="text/javascript" src="../js/jquery.carouFredSel-6.2.1-packed.js"></script>
<script type="text/javascript">

$(document).ready(function() {
	console.log('loaded');
	$('.demoIcon').mouseenter(function(){
		$(this).effect('bounce',"slow", "false");
		});
		
	$('.icon-file-alt').click(function(e){
		
		e.preventDefault();
		
		$('.textControlsDiv').fadeIn('slow');
			$('.pictureControlsDiv').fadeOut('fast');
      
	});
	$('.clickme').click(function(){
		alert('clicked');
	});
	
	$('.icon-picture').click(function(e){
		e.preventDefault();
		
		$('.pictureControlsDiv').fadeIn('slow');
		$('.textControlsDiv').fadeOut('fast');
		
      
	 });
	
	 
	 $(".navItem").click(function(e) {
			e.preventDefault();
			var scrollTo = $(this).attr('id');
			$('html, body').animate({
			 scrollTop: $(scrollTo).offset().top
				}, 500);
		});
		
	 $('.finishNewItem').click(function(){
		var itemName = $('#newItemName').val();
		$('.noItems').remove();
		$('.items').append('<li><a href="#" class="itemSpecial btn btn-rounded btn-info">'+itemName+' </a> </li>');
	 });
	});
	
	</script>
	

<script src="../js/jquery-ui.min.js"></script>
<script type="text/javascript" src="../js/jquery.nerveSlider.min.js"></script>
<script type="text/javascript" src="../js/bootstrap-wysiwyg.js"></script>
<script type="text/javascript" src="../js/manageCore.js"></script>
<script type="text/javascript" src="https://mindmup.s3.amazonaws.com/lib/jquery.hotkeys.js"></script>
<script type="text/javascript" src="../js/storage.js"></script>
<script type="text/javascript" src="../js/jquery.form.js"></script>
<script type="text/javascript" src="https://www.google.com/jsapi"></script>

     <script type="text/javascript">
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var checkin_data = google.visualization.arrayToDataTable([
          ['Check-ins', 'Last 6 hours'],
          ['12 PM',      2],
			
          ['2 PM',      47],
         
          ['4 PM',      17],
          ['5 PM',      37],
          ['8 PM',      7],
		  
        ]);

        var checkin_options = {
          title: 'Views By Time',
		  colors: ['#f89406']
        };


        var chart = new google.visualization.AreaChart(document.getElementById('checkin_chart'));
        chart.draw(checkin_data, checkin_options);




      }
    </script>
</body>
</html>