<?php 

require("../system/dbconnect.php");
require("../system/Content/contentClass.php");
require("../system/Analytics/analyticsClass.php");
require_once('../system/Sessions/SessionHandler.php');
require("../system/User/userClass.php");
require("../system/Billing/billingClass.php");
$user = new User();
$analytics = new Analytics();
$content = new Content();
$billing = new Billing();
$randomString = $content->random_string();
$userBillingPlan = $billing->getUserPlanId($_SESSION['userId']);
$billingPlanArray = $billing->getPlanDetails($userBillingPlan);
$session = new SessionHandler();
session_start();

if(!isset($_SESSION['userId'])) {
	header('Location: ../login.php');
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>spryli &middot; Simple QR Codes</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" type="image/png" href="../img/favicon.ico">

<link href='http://fonts.googleapis.com/css?family=Lato:400,700,300' rel='stylesheet' type='text/css'>
<!--[if IE]>
	<link href="http://fonts.googleapis.com/css?family=Lato" rel="stylesheet" type="text/css">
	<link href="http://fonts.googleapis.com/css?family=Lato:400" rel="stylesheet" type="text/css">
	<link href="http://fonts.googleapis.com/css?family=Lato:700" rel="stylesheet" type="text/css">
	<link href="http://fonts.googleapis.com/css?family=Lato:300" rel="stylesheet" type="text/css">
<![endif]-->

<link href="../css/bootstrap.css" rel="stylesheet">
<link href="../css/font-awesome.min.css" rel="stylesheet">
<link href="../css/theme.css" rel="stylesheet">
<link href="../css/prettyPhoto.css" rel="stylesheet" type="text/css"/>
<link href="../css/zocial.css" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" href="../css/nerveslider.css">
<link href="../css/wysiwyg.css" rel="stylesheet">
<link href="../css/iconStyle.css" rel="stylesheet">
<link href="css/datepicker.css" rel="stylesheet">
<link href="css/manageStyle.css" rel="stylesheet">
<!--[if lt IE 9]>
<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<!--[if IE 7]>
<link rel="stylesheet" href="../css/font-awesome-ie7.min.css">
<![endif]-->

</head>

<body>
<!--header-->
	<div class="header ">
		<!--logo-->
			<div class="container">
					<div class="logo">
						 <a href="index.php"><img src="../img/spryli.png" alt="" class="animated bounceInDown" /></a>  
					</div>
					<!--menu-->
					<nav id="main_menu">
					<div class="menu_wrap">
						<ul class="nav sf-menu">
							<li><a href="" class="whoami"><?php echo $user->retrieveUserEmail($_SESSION['userId']); ?></a> </li>
							<li class="active"><a href="">Dashboard</a> </li>
							<li class=""><a href="billing.php">Billing</a> </li>
							<li> <a href="#support" role="button" data-toggle="modal">Support</a></li>
							<li class="last"><a href="logout.php">Logout</a></li>				
						</ul>
					</div>
				</nav>
			</div>
		</div>
	<!--//header-->
	<!--page-->

	
			
	<div class="container wrapper">
	<div class="inner_content">
	
	<div class="row">
	
	<span><h1 class="text-center"> Welcome to the admin panel! </h1></span>
	</div>
	<div class="pad45"></div>
	<!-- main container -->
	<div class="row mainContainer">
	<div class="span2">
	<div class="sidebar rounded viewCounter">
	<span class="viewCount white"><h3> <?php $views = $analytics->getViewCount(); echo $views['views'] . "/" . $views['limit']; ?> Views </h3></span>
	</div>
	<div class="sidebar rounded">
	<button id="createNewItem" class="btn btn-success btn-rounded btn-large" >New &nbsp; <i class="icon-plus"> </i> </button> 
	<hr>
	<br />
	<h3 class="white"> Your Content: </h3>
	<ul class="items sub15MarginLeft">
	<?php $content->getContentByUserId($_SESSION['userId']); ?>
	</ul>
	<br />
	<hr>
	<a href="#feedback" role="button" class="btn btn-success btn-rounded btn-medium" data-toggle="modal">Feedback?</a>
	</div>
	</div>
	
	<div class="span9 well newItem">
		
            <div class="tabbable tabs-top">
			 <ul id="myTab" class="nav nav-tabs">
			 <li class="active"><a href="#start" data-toggle="tab">Start Here</a></li>
              <li class=""><a href="#content" data-toggle="tab">Content</a></li>
              <li class=""><a href="#view" data-toggle="tab">View</a></li>
			  <li class=""><a href="#print" data-toggle="tab">Print &amp; Deliver</a></li>
			</ul>
            <div id="myTabContent" class="tab-content">
              <div class="tab-pane active" id="start">
                <div class="span4">
				<h1 class="">Pick a name: </h1>
				<input type="text" id="newItemName" value="">
				</div>
				<div class="span4">
		<h1 class="text-center"> What would you like to share? </h1>
		<div class="" style="display:block; margin-left:auto;, margin-right:auto;">
		<a href="#" class="icon-file-alt demoIcon" style="font-size: 4em; color: #2980B9;"></a>
		<a href="#" class="icon-picture demoIcon" style="font-size: 4em; color: #333;"></a>
		<a href="#" class="icon-facetime-video demoIcon" style="font-size: 3em; color: #333;"></a>
		</div>
		</div>
		<a href="#content" data-toggle="tab" class="btn btn-large btn-info pull-right nav-tabs progressTab">Next</a>
              </div>
			  <div class="tab-pane" id="content">
				<!-- Start controls Div -->
				
			   <div class="pictureControlsDiv" >
					<div id="uploadStatus"> </div>
					<br />
					<form method="post" action="uploadPicture.php" enctype="multipart/form-data" id="frmImgUpload">
					<input name="ImageFile" class="btn btn-info btn-rounded btn-large" type="file" accept="image/*" multiple="false" />
					<br />
					<br />
					<input name="btnSubmit" class="btn btn-success btn-rounded btn-large" type="submit" value="Upload" />
					<a href="#start" data-toggle="tab" class="btn btn-large btn-info pull-left progressTab">Back </a>
					</form>
				</div>
				
				<div class="newVideoControlsDiv">
					<div class="row">
						<div class="span5">
							<textarea name="newVideoEmbeddCode" id="newVideoEmbeddCode" class="span5" rows="12"></textarea>
						</div>
						<div class="span4">
							<h2> <a href=""><i class="icon-arrow-left"></i></a>Paste the embedd code here.</h2>
							<br />
							<h3>For YouTube: </h3>
							<p> When viewing the video page, click "Share", and then click "Embed". </p>
							<h3>For Vimeo: </h3>
							<p> When viewing the video, click the "Share" icon near the top right corner of the video. </p>
						</div>
					</div>
				
				<a href="#view" data-toggle="tab" class="btn btn-large btn-success pull-right doneNewVideoItem progressTab">Done</a>
				<a href="#start" data-toggle="tab" class="btn btn-large btn-info pull-left progressTab">Back </a>
				</div>
				
				<div class="textControlsDiv">
			
	<div id="alerts"></div>
	<div id="editorMain">
	
    <div class="btn-toolbar" data-role="editor-toolbar" data-target="#editor">
      <div class="btn-group">
        <a class="btn dropdown-toggle" data-toggle="dropdown" title="" data-original-title="Font"><i class="icon-font"></i><b class="caret"></b></a>
          <ul class="dropdown-menu">
          <li><a data-edit="fontName Serif" style="font-family:'Serif'">Serif</a></li><li><a data-edit="fontName Sans" style="font-family:'Sans'">Sans</a></li><li><a data-edit="fontName Arial" style="font-family:'Arial'">Arial</a></li><li><a data-edit="fontName Arial Black" style="font-family:'Arial Black'">Arial Black</a></li><li><a data-edit="fontName Courier" style="font-family:'Courier'">Courier</a></li><li><a data-edit="fontName Courier New" style="font-family:'Courier New'">Courier New</a></li><li><a data-edit="fontName Comic Sans MS" style="font-family:'Comic Sans MS'">Comic Sans MS</a></li><li><a data-edit="fontName Helvetica" style="font-family:'Helvetica'">Helvetica</a></li><li><a data-edit="fontName Impact" style="font-family:'Impact'">Impact</a></li><li><a data-edit="fontName Lucida Grande" style="font-family:'Lucida Grande'">Lucida Grande</a></li><li><a data-edit="fontName Lucida Sans" style="font-family:'Lucida Sans'">Lucida Sans</a></li><li><a data-edit="fontName Tahoma" style="font-family:'Tahoma'">Tahoma</a></li><li><a data-edit="fontName Times" style="font-family:'Times'">Times</a></li><li><a data-edit="fontName Times New Roman" style="font-family:'Times New Roman'">Times New Roman</a></li><li><a data-edit="fontName Verdana" style="font-family:'Verdana'">Verdana</a></li></ul>
        </div>
      <div class="btn-group">
        <a class="btn dropdown-toggle" data-toggle="dropdown" title="" data-original-title="Font Size"><i class="icon-text-height"></i>&nbsp;<b class="caret"></b></a>
          <ul class="dropdown-menu">
          <li><a data-edit="fontSize 5"><font size="5">Huge</font></a></li>
          <li><a data-edit="fontSize 3"><font size="3">Normal</font></a></li>
          <li><a data-edit="fontSize 1"><font size="1">Small</font></a></li>
          </ul>
      </div>
      <div class="btn-group">
        <a class="btn" data-edit="bold" title="" data-original-title="Bold (Ctrl/Cmd+B)"><i class="icon-bold"></i></a>
        <a class="btn" data-edit="italic" title="" data-original-title="Italic (Ctrl/Cmd+I)"><i class="icon-italic"></i></a>
        <a class="btn" data-edit="strikethrough" title="" data-original-title="Strikethrough"><i class="icon-strikethrough"></i></a>
        <a class="btn" data-edit="underline" title="" data-original-title="Underline (Ctrl/Cmd+U)"><i class="icon-underline"></i></a>
      </div>
      <div class="btn-group">
        <a class="btn btn-info" data-edit="insertunorderedlist" title="" data-original-title="Bullet list"><i class="icon-list-ul"></i></a>
        <a class="btn" data-edit="insertorderedlist" title="" data-original-title="Number list"><i class="icon-list-ol"></i></a>
        <a class="btn" data-edit="outdent" title="" data-original-title="Reduce indent (Shift+Tab)"><i class="icon-indent-left"></i></a>
        <a class="btn" data-edit="indent" title="" data-original-title="Indent (Tab)"><i class="icon-indent-right"></i></a>
      </div>
      <div class="btn-group">
        <a class="btn btn-info" data-edit="justifyleft" title="" data-original-title="Align Left (Ctrl/Cmd+L)"><i class="icon-align-left"></i></a>
        <a class="btn" data-edit="justifycenter" title="" data-original-title="Center (Ctrl/Cmd+E)"><i class="icon-align-center"></i></a>
        <a class="btn" data-edit="justifyright" title="" data-original-title="Align Right (Ctrl/Cmd+R)"><i class="icon-align-right"></i></a>
        <a class="btn" data-edit="justifyfull" title="" data-original-title="Justify (Ctrl/Cmd+J)"><i class="icon-align-justify"></i></a>
      </div>
      <div class="btn-group">
		  <a class="btn dropdown-toggle" data-toggle="dropdown" title="" data-original-title="Hyperlink"><i class="icon-link"></i></a>
		    <div class="dropdown-menu input-append">
			    <input class="span2" placeholder="URL" type="text" data-edit="createLink">
			    <button class="btn" type="button">Add</button>
        </div>
        <a class="btn" data-edit="unlink" title="" data-original-title="Remove Hyperlink"><i class="icon-cut"></i></a>

      </div>
      
      <div class="btn-group">
        <a class="btn" data-edit="undo" title="" data-original-title="Undo (Ctrl/Cmd+Z)"><i class="icon-undo"></i></a>
        <a class="btn" data-edit="redo" title="" data-original-title="Redo (Ctrl/Cmd+Y)"><i class="icon-repeat"></i></a>
      </div>
	   <button class="btn btn-success" id="saveButton">Save</button>
 
     </div>

    <div id="editor" contenteditable="true">
  <p>What would you like to say?</p>
    </div>
	
	</div>
	<br />
				<a href="#start" data-toggle="tab" class="btn btn-large btn-info pull-left progressTab">Back </a>
				<a href="#view" data-toggle="tab" class="btn btn-large btn-success pull-right doneNewItem progressTab">Done</a>
			</div>
				
				
			  <!-- End TextControls Div -->
			  </div>
              <div class="tab-pane fade" id="view">
				<div class="row zeroResult">
					<div class="span8">
					<h2 class="text-center"> You must first save your content to view it. </h2>
					</div>
				</div>
				<div class="row newItemViewLinks">
			<div class="span4 text-center">

			<h3> Scan this QR code:</h3>
				<img id="newItemQRCode" src="https://chart.googleapis.com/chart?cht=qr&amp;chs=250x250&amp;chl=http://spryli.com/">
			<h4> To save: Right click and choose 'Save As' </h4>
			</div>
			<div class="span4 text-center">
			<h3> Or visit this link:</h3>
				<h2><a href="http://spryli.com/" target="_blank" id="newItemURL"></a></h2>
				<a href="https://twitter.com/share" class="twitter-share-button" data-url="http://spryli.com" id="newItemTweet" rel="me" data-size="large" data-related="spryli" data-count="none" data-dnt="true">Tweet</a>

			</div>
			</div>
			<a href="#content" data-toggle="tab" class="btn btn-large btn-info pull-left progressTab">Back </a>
				
          </div>
			<div class="tab-pane" id="print">
                 <h2 class="text-center"> Print &amp; Deliver services are coming soon!</h2>
              </div>
	</div>
	
	</div>
	
	<!--info boxes-->
	
						
			<!--//info boxes-->
	</div>
		<!--//page-->
	
<div class="span9 well existingItems">
		<div class="loading">
			<h2 class="text-center">Working, Please Wait. <img src='../img/ajax-loader.gif' alt="progress bar"/> </h2>
		</div>
            <div class="tabbable tabs-top loadedContent">
			<button class="btn btn-danger btn-rounded deleteItemButton pull-right" id="">Delete </button>
			 <ul id="existingTab" class="nav nav-tabs">
			 <li class="active"><a href="#existingContent" data-toggle="tab">Content</a></li>
              <li class=""><a href="#existingAnalytics" data-toggle="tab">Analytics</a></li>
			  <li class=""><a href="#existingPrint" data-toggle="tab">Print &amp; Deliver</a></li>
			</ul>
            <div id="existingTabContent" class="tab-content">
              <div class="tab-pane active" id="existingContent">
					<div id="alerts"></div>
				<!-- Start controls Div -->
	<div class="pictureControlsDiv">		
				
		<div class="pictureContentDiv">
		</div>	
	</div>
	
	<div class="videoControlsDiv">
		<div class="row">
		<div class="videoPreviewDiv span4" style="min-height: 370px; height: 370px;">
		</div>
		<div class="span4">
			<h2> <a href=""><i class="icon-arrow-down"></i></a>Paste the embedd code here.</h2> 
							
			<textarea class="span5 videoContentDiv" rows="7"></textarea>
			<button class="btn btn-success btn-rounded pull-right" id="saveExistingVideoButton"> Save Changes </button>
			<h3>For YouTube: </h3>
							<p> When viewing the video page, click "Share", and then click "Embed". </p>
							<h3>For Vimeo: </h3>
							<p> When viewing the video, click the "Share" icon near the top right corner of the video. </p>
			
		</div>
		</div>
	</div>
				
			
	
	<div class="textControlsDiv">
	
    <div class="btn-toolbar" data-role="editor-toolbar" data-target="#editorExisting">
      <div class="btn-group">
        <a class="btn dropdown-toggle" data-toggle="dropdown" title="" data-original-title="Font"><i class="icon-font"></i><b class="caret"></b></a>
          <ul class="dropdown-menu">
          <li><a data-edit="fontName Serif" style="font-family:'Serif'">Serif</a></li><li><a data-edit="fontName Sans" style="font-family:'Sans'">Sans</a></li><li><a data-edit="fontName Arial" style="font-family:'Arial'">Arial</a></li><li><a data-edit="fontName Arial Black" style="font-family:'Arial Black'">Arial Black</a></li><li><a data-edit="fontName Courier" style="font-family:'Courier'">Courier</a></li><li><a data-edit="fontName Courier New" style="font-family:'Courier New'">Courier New</a></li><li><a data-edit="fontName Comic Sans MS" style="font-family:'Comic Sans MS'">Comic Sans MS</a></li><li><a data-edit="fontName Helvetica" style="font-family:'Helvetica'">Helvetica</a></li><li><a data-edit="fontName Impact" style="font-family:'Impact'">Impact</a></li><li><a data-edit="fontName Lucida Grande" style="font-family:'Lucida Grande'">Lucida Grande</a></li><li><a data-edit="fontName Lucida Sans" style="font-family:'Lucida Sans'">Lucida Sans</a></li><li><a data-edit="fontName Tahoma" style="font-family:'Tahoma'">Tahoma</a></li><li><a data-edit="fontName Times" style="font-family:'Times'">Times</a></li><li><a data-edit="fontName Times New Roman" style="font-family:'Times New Roman'">Times New Roman</a></li><li><a data-edit="fontName Verdana" style="font-family:'Verdana'">Verdana</a></li></ul>
        </div>
      <div class="btn-group">
        <a class="btn dropdown-toggle" data-toggle="dropdown" title="" data-original-title="Font Size"><i class="icon-text-height"></i>&nbsp;<b class="caret"></b></a>
          <ul class="dropdown-menu">
          <li><a data-edit="fontSize 5"><font size="5">Huge</font></a></li>
          <li><a data-edit="fontSize 3"><font size="3">Normal</font></a></li>
          <li><a data-edit="fontSize 1"><font size="1">Small</font></a></li>
          </ul>
      </div>
      <div class="btn-group">
        <a class="btn" data-edit="bold" title="" data-original-title="Bold (Ctrl/Cmd+B)"><i class="icon-bold"></i></a>
        <a class="btn" data-edit="italic" title="" data-original-title="Italic (Ctrl/Cmd+I)"><i class="icon-italic"></i></a>
        <a class="btn" data-edit="strikethrough" title="" data-original-title="Strikethrough"><i class="icon-strikethrough"></i></a>
        <a class="btn" data-edit="underline" title="" data-original-title="Underline (Ctrl/Cmd+U)"><i class="icon-underline"></i></a>
      </div>
      <div class="btn-group">
        <a class="btn btn-info" data-edit="insertunorderedlist" title="" data-original-title="Bullet list"><i class="icon-list-ul"></i></a>
        <a class="btn" data-edit="insertorderedlist" title="" data-original-title="Number list"><i class="icon-list-ol"></i></a>
        <a class="btn" data-edit="outdent" title="" data-original-title="Reduce indent (Shift+Tab)"><i class="icon-indent-left"></i></a>
        <a class="btn" data-edit="indent" title="" data-original-title="Indent (Tab)"><i class="icon-indent-right"></i></a>
      </div>
      <div class="btn-group">
        <a class="btn btn-info" data-edit="justifyleft" title="" data-original-title="Align Left (Ctrl/Cmd+L)"><i class="icon-align-left"></i></a>
        <a class="btn" data-edit="justifycenter" title="" data-original-title="Center (Ctrl/Cmd+E)"><i class="icon-align-center"></i></a>
        <a class="btn" data-edit="justifyright" title="" data-original-title="Align Right (Ctrl/Cmd+R)"><i class="icon-align-right"></i></a>
        <a class="btn" data-edit="justifyfull" title="" data-original-title="Justify (Ctrl/Cmd+J)"><i class="icon-align-justify"></i></a>
      </div>
      <div class="btn-group">
		  <a class="btn dropdown-toggle" data-toggle="dropdown" title="" data-original-title="Hyperlink"><i class="icon-link"></i></a>
		    <div class="dropdown-menu input-append">
			    <input class="span2" placeholder="URL" type="text" data-edit="createLink">
			    <button class="btn" type="button">Add</button>
        </div>
        <a class="btn" data-edit="unlink" title="" data-original-title="Remove Hyperlink"><i class="icon-cut"></i></a>

      </div>
      
      <div class="btn-group">
        <a class="btn" data-edit="undo" title="" data-original-title="Undo (Ctrl/Cmd+Z)"><i class="icon-undo"></i></a>
        <a class="btn" data-edit="redo" title="" data-original-title="Redo (Ctrl/Cmd+Y)"><i class="icon-repeat"></i></a>
      </div>
	   <button class="btn btn-success" id="saveExistingItemButton">Save</button>
 
     </div>

    <div id="editor" class="textContentDiv editor" contenteditable="true">
    </div>

			
	</div>

	<hr >
		
				
				<br />
	<div class="row">
			 <div class="span4 text-center">
			<h3> Scan this QR code:</h3>
				<img id="existingItemQRCode" src="https://chart.googleapis.com/chart?cht=qr&amp;chs=250x250&amp;chl=http://spryli.com/">
				<h4> To save: Right click and choose 'Save As' </h4>
			</div>
			<div class="span4 text-center">
			<h3> Or visit this link:</h3>
				<h2><a href="http://spryli.com/" target="_blank" id="existingItemURL"></a></h2>
				<a href="https://twitter.com/share" class="twitter-share-button" data-url="http://spryli.com" id="existingItemTweet" rel="me" data-size="large" data-related="spryli" data-count="none" data-dnt="true">Tweet</a>

			</div>
			</div>
			  <!-- End Existing Content Div -->
			  </div>
              <div class="tab-pane fade" id="existingAnalytics">
				<div class="row">
				<div class="span9">
				<div class="pull-right">
					<i class="icon-calendar"> </i><input type="text" value="<?php echo date("m/d/Y");?>" data-date-format="mm/dd/yy" id="dp1" class="span2"> 
					<span> To <i class="icon-calendar"> </i><input type="text" value="<?php echo date("m/d/Y");?>" data-date-format="mm/dd/yy" id="dp2" class="span2"> <button class="btn btn-primary" id="updateAnalytics">Update</button></span>
				</div>
				<br />
				<br />
				<div class="row">
				<div class="span9 analyticsDiv">
				<div id="views_chart" style="width: 800px; height: 150px;">
              <div class="progress progress-striped active">
                <div class="bar" style="width: 70%;"></div>
              </div>
			  </div>
			  	<br />
					<br />
						<br />
					<br />
			  <div class="span3 text-center sub30margin">
			  <h2 class="text-center"> Phone </h2>
					<i class="icon-mobile-phone countIcon" > <span class="phoneCount numberCount"> 0 </span></i>
					</div>
					<div class="span3 text-center sub30margin">
					<h2 class="text-center"> Tablet </h2>
					<i class="icon-tablet countIcon" > <span class="tabletCount numberCount">0</span></i>
					</div>
					<div class="span3 text-center sub30margin">
					<h2 class="text-center"> Computer </h2>
					<i class="icon-desktop countIcon"> <span class="computerCount numberCount"> 0</span> </i>
					</div>
					<br />
					<br />
					</div>
				</div>
				</div>
             </div>
          </div>
			<div class="tab-pane" id="existingPrint">
                 <h2 class="text-center"> Print &amp; Deliver services are coming soon!</h2>
              </div>
	</div>
	
	</div>
	
	<!--info boxes-->
	
						
			<!--//info boxes-->
	</div>
	
	
		<div class="pad25 hidden-desktop"></div>

	
	<!-- footer -->
	
					
<div id="feedback" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="feedback" aria-hidden="true">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×
</button>
    <h3 id="feedkback">How can we make <span>spryli</span> better?</h3>
  </div>
  <div class="modal-body">
  <div id="feedbackForm">
		<p class="form_info">message</p>
		<textarea name="message" id="feedbackMessage" class="span5"></textarea>
	</div>
	</div>
  <div class="modal-footer">
	<button class="btn btn-success btn-rounded btn-large doneFeedback" aria-hidden="true">Submit</button>
    <button class="btn btn-danger btn-rounded btn-large" data-dismiss="modal" aria-hidden="true">
Cancel</button>
   </div>
</div>				

<div id="support" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="support" aria-hidden="true">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×
</button>
    <h3 id="support">How can we help you?</h3>
  </div>
  <div class="modal-body">
  <div id="supportForm">
		<p class="form_info">message</p>
		<textarea name="message" id="supportMessage" class="span5"></textarea>
	</div>
	</div>
  <div class="modal-footer">
	<button class="btn btn-success btn-rounded btn-large doneSupport" aria-hidden="true">Submit</button>
    <button class="btn btn-danger btn-rounded btn-large" data-dismiss="modal" aria-hidden="true">
Cancel</button>
   </div>
</div>						
				
				
<script src="../js/jquery.js"></script>			
<script src="../js/bootstrap.min.js"></script>	
<script src="../js/jquery.touchSwipe.min.js"></script>
<script src="../js/jquery.mousewheel.min.js"></script>				
<script src="../js/superfish.js"></script>
<script type="text/javascript" src="../js/jquery.prettyPhoto.js"></script>
<script type="text/javascript" src="../js/scripts.js"></script>
<!-- carousel -->
<script type="text/javascript" src="../js/jquery.carouFredSel-6.2.1-packed.js"></script>
<script type="text/javascript" src="../js/bootstrap-wysiwyg.js"></script>
<script type="text/javascript">

$(document).ready(function() {
	$('.editor').wysiwyg();
	$('.demoIcon').click(function(){
		$('.demoIcon').css("color", "#333");
		$(this).css("color", "#2980B9");
		});
		
	$('.icon-file-alt').click(function(e){
		
		e.preventDefault();
		
		$('.textControlsDiv').fadeIn('slow');
		$('.pictureControlsDiv').fadeOut('fast');
		$('.newVideoControlsDiv').fadeOut('fast');
      
	});
	
	$('#createNewItem').click(function(){
		$('.existingItem').addClass('btn-info');
		$('.existingItem').removeClass('item-selected');
		$('.newItem').show();
		$('#newItemName').val('');
		$('#myTab li').removeClass("active");
		$('#myTab li:has(a[href="#start"])').addClass('active');
		$('#start').addClass('active');
		$('#content').removeClass('active');
		$('#view').removeClass('active');
		$('#print').removeClass('active');
		$('.textControlsDiv').fadeIn('slow');
		$('.pictureControlsDiv').fadeOut('fast');
		$('.newVideoControlsDiv').fadeOut('fast');
		$("#editor").html('What would you like to say?');
		$('.newItemViewLinks').hide();
		$('.zeroResult').show();
		$('.existingItems').hide();
	});
	
	$('.progressTab').click(function(){
		var show = $(this).attr('href');
		console.log("Show is: " + show);
		$('#myTab li').removeClass("active");
		$('#myTab li:has(a[href="'+show+'"])').addClass('active');
		console.log('#myTab li:has(a[href="'+show+'"])');
	});
	
	$('.items').on("click", '.existingItem', function(){
		$('.newItem').hide();
		
		$('.existingItem').removeClass('item-selected');
		$('.existingItem').addClass('btn-info');
		$('.computerCount').html("0");
		$('.tabletCount').html("0");
		$('.phoneCount').html("0");
		$(this).removeClass('btn-info');
		$(this).addClass('item-selected');
		$('#dp1').datepicker();
		$('#dp2').datepicker();
		$('.existingItems').show();
		var contentId = $(this).attr('id');
		a = {
            tag: "retrieveContentLoggedIn",
			contentId: contentId
        };
		$('.loading').show();
		$('.loadedContent').hide();
		$.ajax({
            type: "POST",
            dataType: "json",
            url: "../system/AJAX/ajaxHandler.php",
            data: {
                jsonObj: a
            },
            success: function (a) {
                "SUCCESS" != eval(a).status && $("#alerts").html("<h3>An error has occured, please try again later.</h3>");
				
               if("text" == eval(a).type){
				var contentData = eval(a).content;
				var existingUrl = eval(a).url;
				var viewData = eval(a).views;
				var chartData = eval(a).chart;
				$('.textContentDiv').html(contentData);
				$('#existingItemQRCode').attr('src', 'http://chart.googleapis.com/chart?cht=qr&chs=250x250&chl=http://spryli.com/'+existingUrl);
				$('#existingItemURL').attr('href', 'http://spryli.com/'+existingUrl);
				$('#existingItemURL').html('http://spryli.com/'+existingUrl);
				$('#existingItemTweet').data('url', existingUrl);
				var tweetURL = $('#existingItemTweet').data('url');
				console.log("Tweet url is: " + tweetURL);
				twttr.widgets.load(document.getElementById('existingItemTweet'));
				// update analytics
				$.each(viewData, function (index) {
					//console.log(data[index].type);
					$('.'+viewData[index].type+'Count').html(viewData[index].count);
				});
				var dataTable = new google.visualization.DataTable();
				dataTable.addColumn('string', 'Date');
				dataTable.addColumn('number', 'Views');
				$.each(chartData, function (index) {
					dataTable.addRow([chartData[index].date, chartData[index].views]);
				});
					
					var chart_options = {
					  title: 'Views By Date', 
					  colors: ['#f89406'],
					  vAxis: {gridlines:{count:5}},
					  hAxis: {minValue: '3'}
					};
					var chart = new google.visualization.ColumnChart(document.getElementById('views_chart'));
					
					chart.draw(dataTable, chart_options);
					console.log("chart redrawn!");
				$('.textControlsDiv').show();
				$('.pictureControlsDiv').hide();
				$('.videoControlsDiv').hide();
				$('.loading').hide();
				$('.loadedContent').show();
				
			   }
			   
			   if("video" == eval(a).type){
				var contentData = eval(a).content;
				var rawContentData = eval(a).rawContent;
				var existingUrl = eval(a).url;
				var viewData = eval(a).views;
				var chartData = eval(a).chart;
				$('.videoContentDiv').html(contentData);
				$('.videoPreviewDiv').html(rawContentData);
				$('#existingItemQRCode').attr('src', 'http://chart.googleapis.com/chart?cht=qr&chs=250x250&chl=http://spryli.com/'+existingUrl);
				$('#existingItemURL').attr('href', 'http://spryli.com/'+existingUrl);
				$('#existingItemURL').html('http://spryli.com/'+existingUrl);
				$('#existingItemTweet').data('url', existingUrl);
				var tweetURL = $('#existingItemTweet').data('url');
				console.log("Tweet url is: " + tweetURL);
				twttr.widgets.load(document.getElementById('existingItemTweet'));
				// update analytics
				$.each(viewData, function (index) {
					//console.log(data[index].type);
					$('.'+viewData[index].type+'Count').html(viewData[index].count);
				});
				var dataTable = new google.visualization.DataTable();
				dataTable.addColumn('string', 'Date');
				dataTable.addColumn('number', 'Views');
				$.each(chartData, function (index) {
					dataTable.addRow([chartData[index].date, chartData[index].views]);
				});
					
					var chart_options = {
					  title: 'Views By Date', 
					  colors: ['#f89406'],
					  vAxis: {gridlines:{count:5}},
					  hAxis: {minValue: '3'}
					};
					var chart = new google.visualization.ColumnChart(document.getElementById('views_chart'));
					
					chart.draw(dataTable, chart_options);
					console.log("chart redrawn!");
				$('.textControlsDiv').hide();
				$('.pictureControlsDiv').hide();
				$('.videoControlsDiv').show();
				$('.loading').hide();
				$('.loadedContent').show();
				
			   }
			   if("image" == eval(a).type){
				var contentData = eval(a).content;
				var existingUrl = eval(a).url;
				var viewData = eval(a).views;
				var chartData = eval(a).chart;
				$('.pictureContentDiv').html(contentData);
				$('#existingItemQRCode').attr('src', 'http://chart.googleapis.com/chart?cht=qr&chs=250x250&chl=http://spryli.com/'+existingUrl);
				$('#existingItemURL').attr('href', 'http://spryli.com/'+existingUrl);
				$('#existingItemURL').html('http://spryli.com/'+existingUrl);
				$('#existingItemTweet').data('url', 'http://spryli.com/'+existingUrl);
				var tweetURL = $('#existingItemTweet').data('url');
				console.log("Tweet url is: " + tweetURL);
				twttr.widgets.load(document.getElementById('existingItemTweet'));
				// Update analytics
				$.each(viewData, function (index) {
					//console.log(data[index].type);
					$('.'+viewData[index].type+'Count').html(viewData[index].count);
				});
				var dataTable = new google.visualization.DataTable();
				dataTable.addColumn('string', 'Date');
				dataTable.addColumn('number', 'Views');
				$.each(chartData, function (index) {
					dataTable.addRow([chartData[index].date, chartData[index].views]);
				});
				var chart_options = {
					 title: 'Views By Date', 
					 colors: ['#f89406'], 
					 vAxis: {gridlines:{count:5}},
					 hAxis: {minValue: '3'}
				};
				var chart = new google.visualization.ColumnChart(document.getElementById('views_chart'));
				chart.draw(dataTable, chart_options);
				console.log("chart redrawn!");
				$('.textControlsDiv').hide();
				$('.videoControlsDiv').hide();
				$('.pictureControlsDiv').show();
				$('.loading').hide();
				$('.loadedContent').show();
			   }
			   
            },
            error: function () {
                $("#alerts").html("<h3>An error has occured, please try again later.</h3>");
            }
        })
	});
	
	$('.icon-picture').click(function(e){
		e.preventDefault();
		
		$('.pictureControlsDiv').fadeIn('slow');
		$('.textControlsDiv').fadeOut('fast');
		$('.newVideoControlsDiv').fadeOut('fast');
      
	 });
	 
	 $('.icon-facetime-video').click(function(e){
		e.preventDefault();
		$('.newVideoControlsDiv').fadeIn('slow');
		$('.pictureControlsDiv').fadeOut('fast');
		$('.textControlsDiv').fadeOut('fast');
		
      
	 });
	
	 
	 $(".navItem").click(function(e) {
			e.preventDefault();
			var scrollTo = $(this).attr('id');
			$('html, body').animate({
			 scrollTop: $(scrollTo).offset().top
				}, 500);
		});
		
	 $('.finishNewItem').click(function(){
		var itemName = $('#newItemName').val();
		$('.noItems').remove();
		$('.items').append('<li><a href="#" class="itemSpecial btn btn-rounded btn-info">'+itemName+' </a> </li>');
	 });
	 
	 $('#dp1').datepicker();
	 $('#dp2').datepicker();
	 $('#updateAnalytics').click(function(){
		$('.computerCount').html("0");
		$('.tabletCount').html("0");
		$('.phoneCount').html("0");
		range = $('#dp1').val() + "|" + $('#dp2').val(); 
		itemId = $('.item-selected').attr('id');
		a = {
            tag: "updateAnalytics",
			range: range, 
			itemId: itemId
        };
		$('.loading').show();
		$('.loadedContent').hide();
		$.ajax({
            type: "POST",
            dataType: "json",
            url: "../system/AJAX/ajaxHandler.php",
            data: {
                jsonObj: a
            },
            success: function (a) {
                "SUCCESS" != eval(a).status && $("#alerts").html("<h3>An error has occured, please try again later.</h3>");
			    "ZERO_RESULT" == eval(a).status 
				$('.computerCount').html("0");
				$('.tabletCount').html("0");
				$('.phoneCount').html("0");
				"SUCCESS" == eval(a).status 
				$('.loading').hide();
				$('.loadedContent').show();
				var viewData = eval(a).views;
				var chartData = eval(a).chart;
				$.each(viewData, function (index) {
					//console.log(data[index].type);
					$('.'+viewData[index].type+'Count').html(viewData[index].count);
				});
				var dataTable = new google.visualization.DataTable();
				dataTable.addColumn('string', 'Date');
				dataTable.addColumn('number', 'Views');
				$.each(chartData, function (index) {
					dataTable.addRow([chartData[index].date, chartData[index].views]);
				});
				var chart_options = {
					 title: 'Views By Date', 
					 colors: ['#f89406'], 
					 vAxis: {gridlines:{count:5}},
					 hAxis: {minValue: '3'}
				};
				var chart = new google.visualization.ColumnChart(document.getElementById('views_chart'));
				chart.draw(dataTable, chart_options);
            },
            error: function () {
                $("#alerts").html("<h3>An error has occured, please try again later.</h3>");
				$('.loading').hide();
				$('.loadedContent').show();
            }
        })
		
	 });
	  
	});
	
	$('.deleteItemButton').click(function(){
		var contentId = $('.item-selected').attr('id');								  	
		var tag = "deleteItem";

		var jsonData = { 
			"tag" : tag, 
			"contentId": contentId};
		if (confirm('Are you sure you want to delete this QR code?')) { 
			$.ajax({
				type: "POST",
				dataType: 'JSON',
				url: '../system/AJAX/ajaxHandler.php',
				data: {
					jsonObj: jsonData
				},
				success: function (data) {
					var data = eval(data);
					if (data.status == "SUCCESS") {
						location.reload();
					}
					
					else {
						alert(data.message);		
					}
				},
				error: function (data) {
					alert("We're sorry. An error has occured in deleting this QR code. Our technicians have been notified.");	
				}
			});
		}
	});
	
	</script>
	

<script src="../js/jquery-ui.min.js"></script>
<script type="text/javascript" src="../js/jquery.nerveSlider.min.js"></script>
<script type="text/javascript" src="../js/manageCore.js"></script>
<script type="text/javascript" src="../js/jquery.hotkeys.js"></script>
<script type="text/javascript" src="../js/storage.js"></script>
<script type="text/javascript" src="../js/jquery.form.js"></script>
     <script type="text/javascript" src="https://www.google.com/jsapi"></script>
     <script type="text/javascript">
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var chartData = google.visualization.arrayToDataTable([
          ['Date', 'Views'],
          ['<?php echo date("m-d");?>',  0],
        ]);

        var chart_options = {
          title: 'Views By Date',
		  colors: ['#f89406'],
		  vAxis: {gridlines:{count:5}},
		  hAxis: {minValue: '3'}		  
        };

        var chart = new google.visualization.ColumnChart(document.getElementById('views_chart'));
        chart.draw(chartData, chart_options);




      }
    </script>
<script type="text/javascript" src="js/bootstrap-datepicker.js"></script>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-42810904-1', 'spryli.com');
  ga('send', 'pageview');

</script>

</body>
</html>