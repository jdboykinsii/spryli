<?php
ini_set('display_errors', 1);
error_reporting(E_ALL);

		require_once '../system/amazon-SDK/sdk.class.php';
		require('../system/dbconnect.php');
		require('../system/Content/contentClass.php');
		require_once('../system/Sessions/SessionHandler.php');
		require('../system/Billing/billingClass.php');
		$session = new SessionHandler();
		session_start();
		$s3 = new AmazonS3();
		$bucket = 'spryli';
		$content = new Content();
		if(isset($_FILES['ImageFile'])) {
		$fileName = md5($_FILES['ImageFile']['name'] . time(). uniqid());
		$contentName = $_POST['contentName'];
		//$s3->putObject($s3->inputFile($fileName, false), $bucket, $fileName, S3::ACL_PUBLIC_READ);
		$response = $s3->create_object($bucket, $fileName, array (
															'fileUpload' => $_FILES['ImageFile']['tmp_name'],
															'storage' => $s3::STORAGE_STANDARD,
															'acl' => $s3::ACL_PUBLIC
															));
		$imageURL= $s3->get_object_url($bucket, $fileName);
		echo json_encode($content->insertPicture($imageURL, $contentName));
		
	}
	
?>