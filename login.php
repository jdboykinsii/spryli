<?php 
require('navigation.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>spryli &middot; Sharing Made Viral</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">

<link rel="icon" type="image/png" href="img/favicon.ico">
<link href='http://fonts.googleapis.com/css?family=Lato:400,700,300' rel='stylesheet' type='text/css'>
<!--[if IE]>
	<link href="http://fonts.googleapis.com/css?family=Lato" rel="stylesheet" type="text/css">
	<link href="http://fonts.googleapis.com/css?family=Lato:400" rel="stylesheet" type="text/css">
	<link href="http://fonts.googleapis.com/css?family=Lato:700" rel="stylesheet" type="text/css">
	<link href="http://fonts.googleapis.com/css?family=Lato:300" rel="stylesheet" type="text/css">
<![endif]-->

<link href="css/bootstrap.css" rel="stylesheet">
<link href="css/font-awesome.min.css" rel="stylesheet">
<link href="css/theme.css" rel="stylesheet">
<link href="css/prettyPhoto.css" rel="stylesheet" type="text/css"/>
<link href="css/zocial.css" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" href="css/nerveslider.css">
<link href="css/wysiwyg.css" rel="stylesheet">
<!--[if lt IE 9]>
<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<!--[if IE 7]>
<link rel="stylesheet" href="css/font-awesome-ie7.min.css">
<![endif]-->
</head>

<body>
<!--header-->
	<div class="header ">
		<!--logo-->
			<div class="container">
					<div class="logo">
						 <a href="index"><img src="img/spryli.png" alt="" class="animated bounceInDown" /></a>  
					</div>
					<!--menu-->
					<nav id="main_menu">
					<div class="menu_wrap">
						<ul class="nav sf-menu">
							<li class=""><a href="index">Home</a> </li>
							<li class=""><a href="index" class="navItem" id=".demo">Demo</a> </li>
							<li class=""><a href="index" class="navItem" id=".pricing">Pricing</a> </li>
							<li class="last active"><a href="login">Login</a></li>
						</ul>
					</div>
				</nav>
			</div>
		</div>
	<!--//header-->
	<!--page-->

		<!-- /SLIDER -->
		
	<div id="banner">
	<div class="container intro_wrapper">
	<div class="inner_content">
	
	<!--welcome-->
		<div class="welcome_index">
		<h2> Log In </h2>
		<div id="form_status">
		
		</div>
		<form>
			<p class="form_info">Email<span class="required" >*</span></p>
			<input class="span5" type="text" name="Email" id="loginEmail"value="">
			<p class="form_info">Password <span class="required">*</span></p>
			<input class="span5" type="password" name="password" id="loginPassword"  value="">
			
			<div class="clear"></div>
			<a href="#" class="btn  btn-success btn-large btn-form marg-right5 btn-rounded" id="loginBtn">Login</a>
			<a href="index" class="btn  btn-info  btn-large btn-form btn-rounded">Sign-Up</a>
			<div class="clear"></div>
		</form>
			
		</div>

		</div>
	<!--//welcome-->
		</div>
			</div>
				</div>
				<!--//banner-->
			
	<div class="container wrapper">
	<div class="inner_content">
	<div class="pad45"></div>
	
	<!-- Demo -->
	<!-- END DEMO -->
	<!--info boxes-->
	<!--//info boxes-->
	</div>
		<!--//page-->
		
		<div class="pad25 hidden-desktop"></div>
	</div>
	
	<!-- footer -->
	
	
	<!-- footer 2 -->
	
						
				<!-- up to top -->
				<a href="#"><i class="go-top hidden-phone hidden-tablet  icon-double-angle-up"></i></a>
				<!--//end-->
				
<script src="js/jquery.js"></script>			
<script src="js/bootstrap.min.js"></script>	
<script type="text/javascript" src="js/login.js"></script>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-42810904-1', 'spryli.com');
  ga('send', 'pageview');

</script>
</body>
</html>