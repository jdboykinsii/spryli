<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>spryli &middot; Simple QR Codes</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">

<link rel="icon" type="image/png" href="img/favicon.ico">
<link href='http://fonts.googleapis.com/css?family=Lato:400,700,300' rel='stylesheet' type='text/css'>
<!--[if IE]>
	<link href="http://fonts.googleapis.com/css?family=Lato" rel="stylesheet" type="text/css">
	<link href="http://fonts.googleapis.com/css?family=Lato:400" rel="stylesheet" type="text/css">
	<link href="http://fonts.googleapis.com/css?family=Lato:700" rel="stylesheet" type="text/css">
	<link href="http://fonts.googleapis.com/css?family=Lato:300" rel="stylesheet" type="text/css">
<![endif]-->

<link href="css/bootstrap.css" rel="stylesheet">
<link href="css/font-awesome.min.css" rel="stylesheet">
<link href="css/theme.css" rel="stylesheet">
<link href="css/photoswipe.css" rel="stylesheet">
<!--[if lt IE 9]>
<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<!--[if IE 7]>
<link rel="stylesheet" href="css/font-awesome-ie7.min.css">
<![endif]-->
</head>

<body>
<!--header-->
	<div class="header ">
		<!--logo-->
			<div class="container">
					<div class="logo">
						 <a href="index"><img src="img/spryli.png" alt="" class="animated bounceInDown" /></a>  
					</div>
					<!--menu-->
					<nav id="main_menu">
					<div class="menu_wrap">
						<ul class="nav sf-menu">
						<li class=""><a href="index">Home</a> </li>
							<li class=""><a href="" class="navItem" id=".demo">Demo</a> </li>
							<li class=""><a href="" class="navItem" id=".pricing">Pricing</a> </li>
							<li class="active"><a href="contact.php" class="navItem" >Contact</a> </li>
							<li class="last"><a href="login">Login</a></li>
								
						</ul>
					</div>
				</nav>
			</div>
		</div>
	<!--//header-->
	<!--page-->

		<!-- /SLIDER -->
		
	<div id="banner">
	<div class="container intro_wrapper">
	<div class="inner_content">
	
	
		</div>
			</div>
				</div>
				<!--//banner-->
			
	<div class="container wrapper">
	<div class="inner_content">
	<div class="pad45"></div>
	<div class="row">
					<div class="span4">
					<h3>Questions? Comments?</h3>
						<p> Feel free to contact us! </p>
					
				
				<h5>
					E: <a href="mailto:jay@spryli.com">jay@spryli.com</a><br>
				</h5>
			</div>
		
		<div class="span5 ">
		<div class="contact_form">  
		<div id="note"></div>
		<div id="fields">
		<form id="ajax-contact-form">
			<p class="form_info">name <span class="required">*</span></p>
			<input class="span5" type="text" name="name" value="">
			<p class="form_info">email <span class="required">*</span></p>
			<input class="span5" type="text" name="email" value="">
			<p class="form_info">subject</p>
			<input class="span5" type="text" name="subject" value=""><br>
			<p class="form_info">message</p>
			<textarea name="message" id="message" class="span5"></textarea>
			<div class="clear"></div>
			
			<input type="submit" class="btn  btn-primary btn-form marg-right5" value="submit">
			<input type="reset" class="btn  btn-primary btn-form" value="reset">
			<div class="clear"></div>
		</form>
	</div>
</div>                   
	</div>                	
		</div>
	<!-- END DEMO -->
	<!--info boxes-->
	<!--//info boxes-->
	</div>
		<!--//page-->
		
		<div class="pad25 hidden-desktop"></div>
	</div>
	
	<!-- footer -->
	<div id="footer2">
		<div class="container">
			<div class="row">
				<div class="span12">
				<a href="terms.php">Site Terms &amp; Conditions, Privacy Policy</a>
				<div class="copyright">
							spryli
							&copy;
							<script type="text/javascript">
							//<![CDATA[
								var d = new Date()
								document.write(d.getFullYear())
								//]]>
								</script>
							 - All Rights Reserved
						</div>
						</div>
					</div>
				</div>
					</div>
					
	
	<!-- footer 2 -->
	
						
				<!-- up to top -->
				<a href="#"><i class="go-top hidden-phone hidden-tablet  icon-double-angle-up"></i></a>
				<!--//end-->
				
<script src="js/jquery.js"></script>			
<script src="js/bootstrap.min.js"></script>	
<script type="text/javascript" src="js/viewScripts.js"></script>
<script type="text/javascript" src="js/klass.min.js"></script> 

<script>

		$(document).ready(function(){	
			$("#ajax-contact-form").submit(function() {
				var str = $(this).serialize();		
				$.ajax({
					type: "POST",
					url: "contact_form/contact_process.php",
					data: str,
					success: function(msg) {
						// Message Sent - Show the 'Thank You' message and hide the form
						if(msg == 'OK') {
							result = '<div class="notification_ok">Your message has been sent. Thank you!</div>';
							$("#fields").hide();
						} else {
							result = msg;
						}
						$('#note').html(result);
					}
				});
				return false;
			});															
		});
	//]]>		
</script>
 
 

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-42810904-1', 'spryli.com');
  ga('send', 'pageview');

</script>
</body>
</html>