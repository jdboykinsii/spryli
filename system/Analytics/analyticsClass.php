<?php 
class Analytics{
	public function __construct() {
		$services_json = json_decode(getenv("VCAP_SERVICES"),true);
		$mysql_config = $services_json["mysql-5.1"][0]["credentials"];
		$this->DBusername = $mysql_config["username"];
		$this->DBpassword = $mysql_config["password"];
		$this->DBhost = $mysql_config["hostname"];	
		$this->DBport = $mysql_config["port"];	
		$this->DBName = $mysql_config["name"];

		$con =  mysql_connect($this->DBhost, $this->DBusername, $this->DBpassword);
			if (!$con) 
			{
				//die('Could not connect on construction: ' . mysql_error());
				return false;
			}
	}
	
	public function getFirstSurveyResponses($question1, $question2, $question3, $paid, $pricePoint, $userEmail){
		$cleanQuestion1 = mysql_real_escape_string($question1);
		$cleanQuestion2 = mysql_real_escape_string($question2);
		$cleanQuestion3 = mysql_real_escape_string($question3);
		$cleanPaid = mysql_real_escape_string($paid);
		$cleanPricePoint = mysql_real_escape_string($pricePoint);
		$cleanUserEmail = mysql_real_escape_string($userEmail);
		$query = "INSERT INTO $this->DBName.bootstrapSurvey VALUES('', '$cleanQuestion1', '$cleanQuestion2', '$cleanQuestion3', '$cleanPaid', '$cleanPricePoint', '$cleanUserEmail', NOW() )";
		//echo $query;
		mysql_query($query);
		
	}

	public function sendThankYouEmail($userEmail, $shortUrl){

		$message = '<html><body style="background:url(http://spryli.com/img/email/bkgrnd.jpg); min-height: 400px;
  font-family: Helvetica Neue, Helvetica, Arial, sans-serif;">
<table style="
  min-height: 20px;
  margin-top: 50px;
  padding: 19px;
  margin-bottom: 20px;
  background-color: #f5f5f5;
  border: 1px solid #eee;
  border: 1px solid rgba(0, 0, 0, 0.05);
  -webkit-border-radius: 4px;
  -moz-border-radius: 4px;
  border-radius: 4px;
  -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.05);
  -moz-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.05);
  box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.05);margin-top: 50px;
	display: block;
	margin-right: auto;
	margin-left: auto;
	width: 600px;">
<tr> 
<td> </td>
</tr>
<tr> ';
$message .=' <td style="background-color: #49afcd;
  background-image: -moz-linear-gradient(top, #4096ee, #7abcff);
  background-image: -ms-linear-gradient(top, #4096ee, #7abcff);
  background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#4096ee), to(#7abcff));
  background-image: -webkit-linear-gradient(top, #4096ee, #7abcff);
  background-image: -o-linear-gradient(top, #4096ee, #7abcff);
  background-image: linear-gradient(top, #4096ee, #7abcff);
  background-repeat: repeat-x;
  border-color: #7abcff #7abcff #1f6377;
  border-color: rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.25);
  *background-color: #7abcff;
  -webkit-border-radius: 4px;
  -moz-border-radius: 4px;
  border-radius: 4px;
  color: #fff;
  filter: progid:DXImageTransform.Microsoft.gradient(enabled = false)"> <h1 align="center"> Thanks for signing up! </h1></td>
</tr>
<tr> 
<td>  <p label="Description">Thank you for signing up!</p>
<p>This email is to confirm that an account for Spryli.com was created using this email address!</p>
<p> If you have any questions or comments, feel free to contact me personally at <a href="mailto:jay@spryli.com">jay@spryli.com</a></p>
                                    </td>
</tr>

</table>

</body>
</html>
';
$subject = "Thank you for your input!";
$headers = "From: admin@pangoapp.com \r\n";
$headers .= "Reply-To: admin@pangoapp.com \r\n";
$headers .= "MIME-Version: 1.0\r\n";
$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
mail($userEmail, $subject, $message, $headers);
	}
	
	public function logView($contentId, $deviceType){
		$cleanContentId = mysql_real_escape_string($contentId);
		$cleanDeviceType = mysql_real_escape_string($deviceType);
		$query = "INSERT INTO $this->DBName.views VALUES('', '$cleanContentId', '$cleanDeviceType', NOW() )";
		//echo $query;
		mysql_query($query);
	}
	
	public function retrieveViewsByDevice($range){
		
		
		if (is_numeric($range)) {
		
			$currentDate = date('Y-m-d H:i:s');
			$subtractDate = strtotime ( "-".$range." hours" , strtotime ( $currentDate ) ) ;
			$pastDate = date('Y-m-d H:i:s', $subtractDate);
			$query = "SELECT DATE_FORMAT(dateCreated, '%l%p') as TIME, COUNT(*) FROM db138376_moblir_PROD.check_ins WHERE dateCreated between '$pastDate' AND now() AND businessId ='TESTBUSINESS' GROUP BY HOUR(dateCreated) ";
			//echo $query;
			$result = mysql_query($query) or die(mysql_error());
			$titleArray = array("TIME", "COUNT");
			$rowArray = array($titleArray);
			if(mysql_num_rows($result) == 0 ) {
				$itemArray = array("status" => "ZERO_RESULT");
			}
			 
			else {
				while($row = mysql_fetch_array($result)){
					//$itemRow = array( "c" => array("v" => $row['TIME']), array("v" => $row['COUNT(*)']));
					$itemRow = array($row['TIME'], $row['COUNT(*)']);
					array_push($rowArray, $itemRow);
				}
			} 
			//return array("cols" => $colArray, "rows" => $rowArray);
			return json_encode($rowArray);
		}
		
		
		
		else if ($range == 'Today') {
			$currentDate = date('Y-m-d H:i:s');

			$query = "SELECT DATE_FORMAT(dateCreated, '%l%p') as TIME, COUNT(*) FROM db138376_moblir_PROD.check_ins WHERE DATE(dateCreated) = now() AND businessId ='TESTBUSINESS' GROUP BY HOUR(dateCreated) ";
			//echo $query;
			$result = mysql_query($query) or die(mysql_error());
			$itemArray = array();
			if(mysql_num_rows($result) == 0 ) {
				$itemArray = array("status" => "ZERO_RESULT");
			}
			 
			else {
				while($row = mysql_fetch_array($result)){
					$itemRow = array( "Time" => $row['TIME'], "Count" => $row['COUNT(*)']);
					array_push($itemArray, $itemRow);
				}
			} 
			return $itemArray;
		}
		
		else {
			return "Invalid Range: " . $range;
		}
	}

	public function updateAnalytics($range, $itemId){
		$rangeArray = explode("|", $range);
		$value1 = date("Y-m-d", strtotime($rangeArray[0]));
		$value2 = date("Y-m-d", strtotime($rangeArray[1]));
		$itemId = mysql_real_escape_string($itemId);
		$query = "SELECT COUNT(*), deviceType FROM $this->DBName.views WHERE contentId = '$itemId' AND DATE(dateViewed) between '$value1' AND '$value2' GROUP BY deviceType";
		$query2 = "SELECT COUNT(*), DATE_FORMAT(dateViewed, '%c-%e') as DATE FROM $this->DBName.views WHERE contentId = '$itemId' AND DATE(dateViewed) between '$value1' AND '$value2' GROUP BY DATE";
		
		$result = mysql_query($query) or die(mysql_error());
		$result2 = mysql_query($query2) or die(mysql_error());
			
			if(mysql_num_rows($result) == 0 ) {
				$resultArray = array("status" => "ZERO_RESULT", "range" => $range, "views" => array("computer" => '0', "tablet"=> '0', "phone" => '0'), "chart" => array( "date" => date("m-d"), "views" => intval(0)));
				return $resultArray;
			}
			 
			else {
				$viewsArray = array();
				$chartArray = array();
				while($row = mysql_fetch_array($result)){
					$itemRow = array("type" => $row['deviceType'], "count" => $row['COUNT(*)']);
					array_push($viewsArray, $itemRow);
				}
				
				while($row = mysql_fetch_array($result2)){
					$itemRow = array("date" => $row['DATE'], "views" => intval($row['COUNT(*)']));
					array_push($chartArray, $itemRow);
				}
				
				$resultArray = array("status" => "SUCCESS", "views" => $viewsArray, "chart" => $chartArray, "range" => $range);
				//$resultArray = array("status" => "SUCCESS", "views" =>$viewsArray, "range" => $range);
				return $resultArray;
			} 
			
		
	}
	
	public function getViewCount(){
		$selectedUser = $_SESSION['userId'];
		$currentMonth = date("n");
		$query = "SELECT ( SELECT COUNT(primaryKey) FROM $this->DBName.views WHERE contentId IN (SELECT contentId FROM $this->DBName.users_content WHERE userId = '$selectedUser') AND MONTH(dateViewed) = '$currentMonth') as views, planViewLimit FROM $this->DBName.billing_plans  WHERE planId = (SELECT planId FROM $this->DBName.users_billing_plans WHERE userId = '$selectedUser')"; 
		$result = mysql_query($query);
		if(mysql_num_rows($result) == 0){
			$returnArray = array("views" => "0", "limit"=> $this->formatNumberString($result['planViewLimit']), "limiStringt" => $result['planViewLimit']);
		}
		else {
			$result = mysql_fetch_assoc($result);
			$returnArray = array("views" => $result['views'], "limit" => $this->formatNumberString($result['planViewLimit']), "limitString" => $result['planViewLimit'] );
		}
		return $returnArray;
		
	}
	
	public function checkViews($suppliedContentId) {
		$cleanContentId = mysql_real_escape_string($suppliedContentId);
		$currentMonth = date("n");
		$query = "SELECT (SELECT COUNT(primaryKey) FROM $this->DBName.views WHERE contentId = '$cleanContentId' MONTH(dateViewed) = '$currentMonth') as views, planViewLimit FROM $this->DBName.billing_plans  WHERE planId = (SELECT planId FROM $this->DBName.users_billing_plans WHERE userId = '$selectedUser')"; 
		$result = mysql_query($query);
		if(mysql_num_rows($result) == 0){
			$returnArray = array("views" => "0", "limit"=> $this->formatNumberString($result['planViewLimit']), "limiStringt" => $result['planViewLimit']);
		}
		else {
			$result = mysql_fetch_assoc($result);
			$returnArray = array("views" => $result['views'], "limit" => $this->formatNumberString($result['planViewLimit']), "limitString" => $result['planViewLimit'] );
		}
		return $returnArray;
	}
	
		public function checkDEMOViews($suppliedContentId) {
		$cleanContentId = mysql_real_escape_string($suppliedContentId);
		$currentMonth = date("n");
		$query = "SELECT (SELECT COUNT(primaryKey) FROM $this->DBName.views WHERE contentId = '$cleanContentId' MONTH(dateViewed) = '$currentMonth') as views, planViewLimit FROM $this->DBName.billing_plans  WHERE planId = (SELECT planId FROM $this->DBName.users_billing_plans WHERE userId = '$selectedUser')"; 
		$result = mysql_query($query);
		if(mysql_num_rows($result) == 0){
			$returnArray = array("views" => "0", "limit"=> $this->formatNumberString($result['planViewLimit']), "limiStringt" => $result['planViewLimit']);
		}
		else {
			$result = mysql_fetch_assoc($result);
			$returnArray = array("views" => $result['views'], "limit" => $this->formatNumberString($result['planViewLimit']), "limitString" => $result['planViewLimit'] );
		}
		return $returnArray;
	}
	
	function formatNumberString($numberString) {
		if(strlen($numberString) >= 4){
			// add comma after 3rd string from end of string.
			$numberString = strrev($numberString);
			$numberString = substr($numberString, 0, 3) . ",". substr($numberString, 3);
			$numberString = strrev($numberString);
			return $numberString;
		}
		else {
			return $numberString;
		}
	}
	// Depreciated. 
	function checkForZeroViewsByDevice($viewsArray) {
		$scanThisArray = $viewsArray["type"];
		$typesArray = array("computer", "tablet", "phone");
		$count = count($viewsArray);
		if($count == 3){
			return $viewsArray;
		}
		else{
			$missingItem = array_diff($typesArray, $scanThisArray);
			foreach($missingItem as $item){
				$append = array("type" => $item, "count" => "0");
				array_push($viewsArray, $append);
			}
			return $viewsArray;
		}
	}
	
	function base64_url_encode($input)
	{
    	//return strtr(base64_encode($input), '+/=', '-_,');
		return base_convert(intval($input), 10, 32);
	}
 
	function base64_url_decode($input)
	{
    	return base_convert($input, 32, 10);
	}
}
?>