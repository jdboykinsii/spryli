<?php
// Standard user function class. Recycle as needed. 
class User extends DBClass {
	public $BOOLTRUE = '1';
	public $BOOLFALSE = '0';
	
	public function __construct() {
	$services_json = json_decode(getenv("VCAP_SERVICES"),true);
		$mysql_config = $services_json["mysql-5.1"][0]["credentials"];
		$this->DBusername = $mysql_config["username"];
		$this->DBpassword = $mysql_config["password"];
		$this->DBhost = $mysql_config["hostname"];	
		$this->DBport = $mysql_config["port"];	
		$this->DBName = $mysql_config["name"];

		$con =  mysql_connect($this->DBhost, $this->DBusername, $this->DBpassword);
		mysql_set_charset("utf8");
			if (!$con) 
			{
				//die('Could not connect on construction: ' . mysql_error());
				return false;
			}
	}
	
	public function checkEmailAvailable($suppliedEmail){ 
		$cleanEmail = mysql_real_escape_string($suppliedEmail);
		$query = "SELECT userEmail FROM $this->DBName.users WHERE userEmail = '$cleanEmail'";
		$result = mysql_query($query) or die(mysql_error());
		if(mysql_num_rows($result) == 0 ){
			//echo "No record found";
			return true;
		}
		else {
			//echo "Record Found";
			return false;
		}
		
	}
	
	public function checkSignupEmailAvailable($suppliedEmail){ 
		$cleanEmail = mysql_real_escape_string($suppliedEmail);
		//echo $cleanEmail;
		$query = "SELECT userEmail FROM $this->DBName.users WHERE userEmail = '$cleanEmail'";
		$result = mysql_query($query) or die(mysql_error());
		if(mysql_num_rows($result) == 0 ){
			$result = array("status" => "PASS");
			return $result;
		}
		else {
			$result = array("status" => "TAKEN");
			return $result;
		}
		
	}
	
	public function isEmailRegistered($suppliedEmail){ 
		$cleanEmail = mysql_real_escape_string($suppliedEmail);
		$query = "SELECT userEmail FROM $this->DBName.users WHERE userEmail = '$cleanEmail'";
		$result = mysql_query($query) or die(mysql_error());
		if(mysql_num_rows($result) == 0 ){
			//echo "No record found";
			return false;
		}
		else {
			//echo "Record Found";
			return true;
		}
		
	}
	
	public function createNewUser($suppliedUserFName, $suppliedUserLName, $suppliedUserEmail, $suppliedUserPWString, $address1, $address2, $city, $state, $zip, $cardName, $cardNumber, $expMonth, $expYear, $cvc, $planType, $suppliedUserPhoneNum, $coupon){
		// Parent process. 
		$this->currentUserFName = mysql_real_escape_string($suppliedUserFName);
		$this->currentUserLName = mysql_real_escape_string($suppliedUserLName);
		$this->currentUserEmail = strtolower(mysql_real_escape_string($suppliedUserEmail));
		$this->currentUserHashedPW = hash('sha1', mysql_real_escape_string($suppliedUserPWString));
		$this->currentUserAddress1 = mysql_real_escape_string($address1);
		$this->currentUserAddress2 = mysql_real_escape_string($address2);
		$this->currentUserCity = mysql_real_escape_string($city);
		$this->currentUserState = mysql_real_escape_string($state);
		$this->currentUserZip = mysql_real_escape_string($zip);
		$this->currentUserCardName = mysql_real_escape_string($cardName);
		$this->currentUserCardNumber = mysql_real_escape_string($cardNumber);
		$this->currentUserCardExpMonth = mysql_real_escape_string($expMonth);
		$this->currentUserCardExpYear = mysql_real_escape_string($expYear);
		$this->currentUserCardCVC = mysql_real_escape_string($cvc);
		$this->currentUserPlanType = mysql_real_escape_string($planType);
		$this->currentUserPhoneNum = mysql_real_escape_string($suppliedUserPhoneNum);
		if($this->currentUserPhoneNum = ""){
			$this->currentUserPhoneNum = "00000000";
		}
		if($this->checkEmailAvailable($this->currentUserEmail)){
			$billing = new Billing();
			if($planType == "ppu"){
				$chargeResult = $billing->chargeCustomer($this->currentUserEmail, $this->currentUserPlanType, $this->currentUserCardName, $this->currentUserCardNumber, $this->currentUserCardCVC, $this->currentUserCardExpMonth, $this->currentUserCardExpYear, $this->currentUserAddress1, $this->currentUserAddress2, $this->currentUserCity,  $this->currentUserZip, $this->currentUserState, $coupon);
			
			}
			else {
				$chargeResult = $billing->subscribeCustomer($this->currentUserEmail, $this->currentUserPlanType, $this->currentUserCardName, $this->currentUserCardNumber, $this->currentUserCardCVC, $this->currentUserCardExpMonth, $this->currentUserCardExpYear, $this->currentUserAddress1, $this->currentUserAddress2, $this->currentUserCity,  $this->currentUserZip, $this->currentUserState, $coupon);
			}
			if($chargeResult['status'] == 'SUCCESS'){
				$this->createNewUserId();
				$this->createUniqueSalt();
				$this->createNewPasswordHash();
				//$security = new Security();
				//$token = $security->generateNewToken($this->currentUserId);
				if($this->storePart1Data() && $this->storePart2Data() && $this->storePart3Data($chargeResult['stripeCustId']) ){
					$resultArray = array("status"=> "SUCCESS");
					$mailObject = new Mail();
					$mailObject->sendWelcomeEmail($this->currentUserEmail);
					return $resultArray;
				}
				else {
				$resultArray = array("status"=> "ERROR");
					return $resultArray;
				}
			}
			else {
				$resultArray = $chargeResult;
				return $resultArray;
			}
		}
		
		else {
			$resultArray = array("status"=> "USEREXISTS", "message" => 'Sorry, this email is taken.');
			return $resultArray;
		}
		
		
	}
	
	private function createNewUserId(){
		$this->currentUserId = md5(uniqid());
	}
	
	private function createUniqueSalt(){
		$this->currentUserSalt = hash('sha1', $this->currentUserId . $this->currentUserEmail . time() );
	}
	
	private function createNewPasswordHash(){
		$this->currentUserNewPasswordHash = hash('sha256', $this->currentUserSalt + $this->currentUserHashedPW);
	}
	
	private function storePart1Data(){
		// Stores 
		$query = "INSERT INTO $this->DBName.users VALUES ('', '$this->currentUserId', '$this->currentUserEmail', '$this->currentUserSalt', '$this->currentUserNewPasswordHash', now(), now(), '1')";
		if(mysql_query($query) or die(mysql_error())){
			return true;
		}
		else {
			return false;
		}
	}
	
	private function storePart2Data(){
		// Stores extra data about the user
		$query = "INSERT INTO $this->DBName.usersDetail VALUES ('', '$this->currentUserId', '$this->currentUserFName', '$this->currentUserLName', '$this->currentUserAddress1', '$this->currentUserAddress2', '$this->currentUserCity', '$this->currentUserState', '$this->currentUserZip',  '$this->currentUserPhoneNum')";
		if(mysql_query($query) or die(mysql_error())){
			return true;
		}
		else {
			return false;
		}
	}
	
	private function storePart3Data($stripeId){
		$query = "INSERT INTO $this->DBName.users_billing_plans VALUES ('', '$this->currentUserId', '$this->currentUserPlanType', '$stripeId', '$this->BOOLTRUE')";
			if(mysql_query($query) or die(mysql_error())){
				return true;
			}
			else {
				return false;
			}
	}
	
	private function updatePasswordHash($suppliedUserId){
		$query = "UPDATE $this->DBName.users SET userSalt = '$this->currentUserSalt', userPassword = '$this->currentUserNewPasswordHash' WHERE userId = '$suppliedUserId' ";
		if(mysql_query($query) or die(mysql_error())){
			return true;
		}
		else {
			return false;
		}
	}
	
	public function verifyUserPassword($suppliedEmail, $suppliedPasswordHash){
		$cleanEmail = mysql_real_escape_string($suppliedEmail);
		$cleanHash = hash('sha1', mysql_real_escape_string($suppliedPasswordHash));
		$query = "SELECT userId, userSalt, userPassword FROM $this->DBName.users WHERE userEmail = '$cleanEmail' AND isActive = '1'";
		//echo $query;
		$result = mysql_query($query) or die(mysql_error());
		if(mysql_num_rows($result) == 0 ){
			$resultArray = array("status"=> "FALSE");
			return $resultArray;

		}
		else {
			while($row = mysql_fetch_array($result)){
				$valueToVerifyAgainst =  $row['userPassword'];
				$this->currentUserId = $row['userId'];
				//$security = new Security();
				//$token = $security->generateNewToken($this->currentUserId);
				$valueToVerfiy = hash('sha256', $row['userSalt'] + $cleanHash);
				if($valueToVerfiy == $valueToVerifyAgainst){
					//echo "MATCHES";
					//echo "<br /> " . $valueToVerfiy . " = " . $valueToVerifyAgainst;
					$query = "UPDATE $this->DBName.users SET lastActive = NOW() WHERE userEmail = '$cleanEmail'";
					mysql_query($query);
					$sessionId = session_id();
					$_SESSION['userId'] = $row['userId'];
					$resultArray = array("status"=> "TRUE");
					return $resultArray;
				}
				else {
					//echo "NOT MATCH";
					//echo "<br /> " . $valueToVerfiy . " NOT EQUAL " . $valueToVerifyAgainst;
					$resultArray = array("status"=> "FALSE");
					return $resultArray;
				}

			}
		}
		
	}
	
	public function verifyUserPasswordInternal($suppliedUserId, $suppliedPasswordHash){
		$cleanUserId = mysql_real_escape_string($suppliedUserId);
		$cleanHash = mysql_real_escape_string($suppliedPasswordHash);
		$query = "SELECT userId, userSalt, userPassword FROM $this->DBName.users WHERE userId = '$cleanUserId' AND isActive = '1'";
		$result = mysql_query($query) or die(mysql_error());
		if(mysql_num_rows($result) == 0 ){
			//echo "No record found";
			return false;
		}
		else {
			while($row = mysql_fetch_array($result)){
				$valueToVerifyAgainst =  $row['userPassword'];
				$this->currentUserId = $row['userId'];
				$security = new Security();
				$valueToVerfiy = hash('sha256', $row['userSalt'] + $cleanHash);
				if($valueToVerfiy == $valueToVerifyAgainst){
					return true;
				}
				else {
					
					return false;
				}

			}
		}
		
	}
	
	public function createForgotPasswordToken($suppliedEmail){
		$query = "SELECT userEmail, tokenValue FROM $this->DBName.resetPassword WHERE userEmail = '$suppliedEmail' ";
		$result = mysql_query($query) or die(mysql_error());
		if(mysql_num_rows($result) > 0){
			while($row = mysql_fetch_array($result)){
				$this->forgotPasswordToken = $row['tokenValue'];
			}
		}
		else {
			$tokenSalt = "r3sAFrefresw3TRumyA@fr!";
			$currentDateTime = date("Y-m-d h:i:s");
			$this->forgotPasswordToken = md5($tokenSalt . $currentDateTime);
			$sql = "INSERT INTO $this->DBName.resetPassword(tokenId, tokenValue, userEmail, creationDate) VALUES ('','$this->forgotPasswordToken', '$suppliedEmail', NOW())";
			mysql_query($sql) or die(mysql_error());
		}
	}
	
	public function forgotUserPassword($suppliedEmail){
		$cleanEmail = strtolower(mysql_real_escape_string($suppliedEmail));
		if($this->isEmailRegistered($cleanEmail)){
			$this->createForgotPasswordToken($cleanEmail);
			$this->sendForgotPasswordEmail($cleanEmail, $this->forgotPasswordToken);
			return "SUCCESS";
		}
		else {
			return "DOESNOTEXIST";
			
		} 
	}
	
	public function clearForgotPasswordToken($suppliedEmail){
		$cleanEmail = mysql_real_escape_string($suppliedEmail);
		$query = "DELETE FROM $this->DBName.resetPassword WHERE userEmail = '$cleanEmail'";
		if(mysql_query($query) or die(mysql_error())){
			return "true";
		}
		else {
			return "false";
		}
	}
	
	public function deleteUserAccount($suppliedUserId){
		$cleanUserId = mysql_real_escape_string($suppliedUserId);
		$deleteString = "DELETED|-|". date("Y-m-d h:i:s");
		$query = "UPDATE $this->DBName.users SET isActive = '0', userEmail = concat(userEmail,'$deleteString') WHERE userId = '$cleanUserId'";
		if(mysql_query($query) or die(mysql_error())) 
		{
			return "TRUE";
		}
		else{ 
			return "FALSE";
		}
	}
	
	public function changeUserPassword($suppliedUserId, $oldPasswordHash, $newPasswordHash){
		$cleanSuppliedUserId = mysql_real_escape_string($suppliedUserId);
		$cleanOldPasswordHash = mysql_real_escape_string($oldPasswordHash);
		$cleanNewPasswordHash = mysql_real_escape_string($newPasswordHash);
		$this->currentUserId = $cleanSuppliedUserId;
		if($this->verifyUserPasswordInternal($cleanSuppliedUserId, $cleanOldPasswordHash)){
			$this->createUniqueSalt();
			$this->currentUserHashedPW = $cleanNewPasswordHash;
			$this->createNewPasswordHash();
			$security = new Security();
			$token = $security->generateNewToken($this->currentUserId);
			if($this->updatePasswordHash($this->currentUserId)){
			
				$resultArray = array("status"=> "TRUE", "token" => $token );
				return $resultArray;
			}
			else {
				$resultArray = array("status"=> "ERROR", "token" => "" );
				return $resultArray;
			}
		}
		else {
			$resultArray = array("status"=> "FALSE");
			return $resultArray;
		}
	}
	
	public function resetUserPassword($token, $newPassword){
		// From those two, we need to change the password. 
		$cleanToken = mysql_real_escape_string($token);
		$cleanNewPassword = mysql_real_escape_string($newPassword);
		$query = "SELECT userEmail, tokenValue FROM $this->DBName.resetPassword WHERE tokenValue = '$cleanToken'";
		$result = mysql_query($query) or die(mysql_error());
		if(mysql_num_rows($result) == 0 ){
			//echo "No record found";
			return "error";
		}
		else {
			while($row = mysql_fetch_array($result)){
				return $this-> updateUserPassword($row['userEmail'], $cleanNewPassword);
			}
		}
	}
	
	public function retrieveUserEmail($suppliedId){
		$cleanUserId = mysql_real_escape_string($suppliedId);
		$query = "SELECT userEmail FROM $this->DBName.users WHERE userId = '$cleanUserId'";
		$result = mysql_query($query) or die(mysql_error());
		if(mysql_num_rows($result) == 0 ){
			//echo "No record found";
			return false;
		}
		else {
				$row = mysql_fetch_array($result);
				return $row['userEmail'];
			}
		
	}
	
	public function updateUserPassword($userEmail, $newPassword){
		$this->currentUserId = $this->retrieveUserId($userEmail);
		$this->currentUserEmail = $userEmail;
		$this->createUniqueSalt();
		$this->currentUserHashedPW = hash('sha1', $newPassword);
		$this->createNewPasswordHash();
		if($this->updatePasswordHash($this->currentUserId)){
			$this->clearForgotPasswordToken($userEmail);
			return "true";
		}
		else {
			return "error";
		}
	}
	
	public function cancelAccount($userId){
		$cleanUserId = mysql_real_escape_string($userId);
		if($cleanUserId == $_SESSION['userId']){
			$query1 = "SELECT stripeId FROM $this->DBName.users_billing_plans WHERE userId = '$cleanUserId'";
			$result = mysql_query($query1) or die(mysql_error()); 
			$row = mysql_fetch_array($result);
			$billing = new Billing();
			$billing->cancelBilling($row['stripeId']);
			$query2 = "UPDATE $this->DBName.users SET isActive = '$this->BOOLFALSE' WHERE userId = '$cleanUserId'";
			mysql_query($query2) or die(mysql_error()); 
			$query3 = "UPDATE $this->DBName.users_billing_plans SET active = '$this->BOOLFALSE' WHERE userId = '$cleanUserId'";
			mysql_query($query3) or die(mysql_error());
			$resultArray = array("status" => "SUCCESS", "message" => 'Your account has succesfully been closed.');
			session_destroy();
			return $resultArray;
		}
		else {
			$resultArray = array("status" => "ERROR", "message" => 'An error has occured in closing your account. Our support team has been notified.');
			return $resultArray;
		}
	}
	
	public function logItem($type, $text){
		$cleanType = mysql_real_escape_string($type);
		$cleanText = mysql_real_escape_string($text);
		$userId = $_SESSION['userId'];
		$userEmail = $this->retrieveUserEmail($_SESSION['userId']);
		if($cleanType === 'feedback'){
			$query = "INSERT INTO $this->DBName.feedbackRequests VALUES ('', '$userId', '$userEmail', '$cleanText', NOW(), '$this->BOOLFALSE', '$this->BOOLFALSE')";
			if(mysql_query($query) or die(mysql_error)){
				$resultArray = array('status' => 'SUCCESS', 'message' => 'Thanks for your feedback!');
			}
			else {
				$resultArray = array('status' => 'ERROR', 'message' => "We're sorry, an error has occurred and our technicians have been notified.");
			}
			return $resultArray;
		}
		else if($cleanType === 'support'){
			$query = "INSERT INTO $this->DBName.supportRequests VALUES ('', '$userId', '$userEmail', '$cleanText', NOW(), '$this->BOOLFALSE', '$this->BOOLFALSE')";
			if(mysql_query($query) or die(mysql_error)){
				$resultArray = array('status' => 'SUCCESS', 'message' => 'Thank you, one of our technicians should contact you soon.');
			}
			else {
				$resultArray = array('status' => 'ERROR', 'message' => "We're sorry, an error has occurred and our technicians have been notified.");
			}
			return $resultArray;
		}
		else {
			$resultArray = array('status' => 'ERROR', 'message' => 'Invalid parameter');
			return $resultArray;
		}
	}
	
	public function logUserFeedback(){
	
	}
	
	public function logSupportRequest(){
	
	}

	public function __destruct() {
	}
}

?>