<?php
class AJAXClass extends DBClass {
	private $request = "";
	private $requestResponse = "";
	
	public function __construct(){
		$session = new SessionHandler();
		session_start();
		session_regenerate_id(true); 
	}
	
	public function setRequest($ajaxRequest) {
		//$this->request = json_decode($ajaxRequest, true);
		$this->request = $ajaxRequest;
	
	}
	
	public function handleRequest() {
		$tag = $this->request{"tag"};
		switch ($tag)
		{
			case "saveContent":
			$content = new content;
			$this->requestResponse = $content->saveContent($this->request{"contentId"}, $this->request{"contentData"}, $this->request{"type"});
			break;
			
			/*
			case "retrieveContent":
			$content = new content;
			$this->requestResponse = $content->retrieveContent($this->request{"pageId"}, $this->request{"userId"});
			break;
			*/
			
			case "retrieveContent":
			$content = new content;
			$this->requestResponse = $content->retrieveContent($this->request{"contentId"});
			break;
			
			case "retrieveContentLoggedIn":
			$content = new content;
			$this->requestResponse = $content->retrieveContentLoggedIn($this->request{"contentId"});
			break;
			
			case "createNewPage":
			$content = new content;
			$this->requestResponse = $content->createNewPage($this->request{"pangoId"}, $this->request{"pageName"}, $this->request{"userId"});
			break;
			
			case "deletePage": 
			$content = new content;
			$this->requestResponse = $content->deletePage($this->request{"pageId"}, $this->request{"userId"});
			break;
			
			case "createNewPango":
			$content = new content;
			$this->requestResponse = $content->createNewPango($this->request{"userId"}, $this->request{"pangoName"});
			break;
			
			case "loadPages":
			$content = new content;
			$this->requestResponse = $content->listPagesAjax($this->request{"pangoId"});
			break;

			case "createContent":
			$content = new content;
			$this->requestResponse = $content->createContent($this->request{"contentId"}, $this->request{"contentName"}, $this->request{"contentData"}, $this->request{"type"});
			break;
			
			case "createContentLoggedIn":
			$content = new content;
			$this->requestResponse = $content->createContentLoggedIn($this->request{"contentName"}, $this->request{"contentData"}, $this->request{"type"});
			break;
			
			case "checkEmail":
			$user = new User();
			$this->requestResponse = $user->checkSignupEmailAvailable($this->request{"email"});
			break;
			
			case "createNewAccount":
			$user = new User();
			$this->requestResponse = $user->createNewUser($this->request{"fname"}, $this->request{"lname"}, $this->request{"email"}, $this->request{"password"}, $this->request{'address1'}, $this->request{'address2'}, $this->request{'city'}, $this->request{"state"}, $this->request{'zip'}, $this->request{'cardName'}, $this->request{'cardNumber'}, $this->request{'expMonth'}, $this->request{'expYear'}, $this->request{'cvc'}, $this->request{'planType'}, $this->request{'phoneNum'}, $this->request{'coupon'});
			break;
			
			case "loginUser":
			$user = new User();
			$this->requestResponse = $user->verifyUserPassword($this->request{"loginEmail"}, $this->request{"loginPassword"});
			break;
			
			case "updateAnalytics":
			$analytics = new Analytics();
			$this->requestResponse = $analytics->updateAnalytics($this->request{"range"}, $this->request{"itemId"});
			break;
			
			case "cancelAccount":
			$cancelAccount = new User();
			$this->requestResponse = $cancelAccount->cancelAccount($this->request{"userId"});
			break;
			
			case "logItem":
			$logItem = new User();
			$this->requestResponse = $logItem->logItem($this->request{"type"}, $this->request{"text"});
			break;
			
			case "deleteItem":
			$deleteItem = new Content();
			$this->requestResponse = $deleteItem->deleteItem($this->request{"contentId"});
			break;
		}
	}	

	public function getRequest() {
		echo json_encode($this->requestResponse);
	}
 

}


?>