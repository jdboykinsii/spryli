<?php
require('../dbconnect.php');
require('AJAXClass.php');
require('../Content/contentClass.php');
require('../User/userClass.php');
require('../Analytics/analyticsClass.php');
require('../Billing/billingClass.php');
require_once('../Sessions/SessionHandler.php');
require('../mail/mailClass.php');

$ajaxHandler = new AJAXClass();
$recievedAjaxRequest = $_POST['jsonObj'];
$ajaxHandler->setRequest($recievedAjaxRequest);
$ajaxHandler->handleRequest();
echo $ajaxHandler->getRequest();
?>