<?php

/**
* A PHP session handler to keep session data within a MySQL database
*
* @author 	Manuel Reinhard <manu@sprain.ch>
* @link		https://github.com/sprain/PHP-MySQL-Session-Handler
*/

class SessionHandler{

	public function __construct() {
		$this->setDbDetails();
		$this->setDbTable('session_handler_table');
		session_set_save_handler(array($this, 'open'),
                         array($this, 'close'),
                         array($this, 'read'),
                         array($this, 'write'),
                         array($this, 'destroy'),
                         array($this, 'gc'));
	}
    /**
     * a database MySQLi connection resource
     * @var resource
     */
    protected $dbConnection;
    
    
    /**
     * the name of the DB table which handles the sessions
     * @var string
     */
    protected $dbTable;
	


	/**
	 * Set db data if no connection is being injected
	 * @param 	string	$dbHost	
	 * @param	string	$dbUser
	 * @param	string	$dbPassword
	 * @param	string	$dbDatabase
	 */	
	 
	 
	public function setDbDetails(){
		$services_json = json_decode(getenv("VCAP_SERVICES"),true);
		$mysql_config = $services_json["mysql-5.1"][0]["credentials"];
		$this->DBusername = $mysql_config["username"];
		$this->DBpassword = $mysql_config["password"];
		$this->DBhost = $mysql_config["hostname"];	
		$this->DBport = $mysql_config["port"];	
		$this->DBName = $mysql_config["name"];
	
		$con =  mysql_connect($this->DBhost, $this->DBusername, $this->DBpassword);
		mysql_set_charset("utf8");
			if (!$con) 
			{
				die('Could not connect on construction: ' . mysql_error());
				return false;
			}
		
		//create db connection
		$this->dbConnection = new mysqli($this->DBhost, $this->DBusername, $this->DBpassword, $this->DBName);
		
		//check connection
		if (mysqli_connect_error()) {
		    throw new Exception('Connect Error (' . mysqli_connect_errno() . ') ' . mysqli_connect_error());
		}//if
			
	}
	
	
	
	/**
	 * Inject DB connection from outside
	 * @param 	object	$dbConnection	expects MySQLi object
	 */
	public function setDbConnection($dbConnection){
	
		$this->dbConnection = $dbConnection;
		
	}
	
	
	/**
	 * Inject DB connection from outside
	 * @param 	object	$dbConnection	expects MySQLi object
	 */
	public function setDbTable($dbTable){
	
		$this->dbTable = $dbTable;
		
	}
	

    /**
     * Open the session
     * @return bool
     */
    public function open() {
		if($this->checkSessionExists()){
		
		}
		else {
			//delete old session handlers
			$limit = time() - (3600 * 24);
			$sql = sprintf("DELETE FROM %s WHERE timestamp < %s", $this->dbTable, $limit);
			return $this->dbConnection->query($sql);
		}

    }
	
	public function checkSessionExists() {
		 $sql = sprintf("SELECT id FROM %s WHERE id = '%s'", $this->dbTable, $this->dbConnection->escape_string(session_Id()));
		 $result = $this->dbConnection->query($sql);
		 if ($result->num_rows && $result->num_rows > 0) {
                return true;
            } else {
                return false;
            }
	}

    /**
     * Close the session
     * @return bool
     */
    public function close() {

        return $this->dbConnection->close();

    }

    /**
     * Read the session
     * @param int session id
     * @return string string of the sessoin
     */
    public function read($id) {

        $sql = sprintf("SELECT data FROM %s WHERE id = '%s'", $this->dbTable, $this->dbConnection->escape_string($id));
        if ($result = $this->dbConnection->query($sql)) {
            if ($result->num_rows && $result->num_rows > 0) {
                $record = $result->fetch_assoc();
                return $record['data'];
            } else {
                return false;
            }
        } else {
            return false;
        }
        return true;
        
    }
    

    /**
     * Write the session
     * @param int session id
     * @param string data of the session
     */
    public function write($id, $data) {
		
        $sql = sprintf("REPLACE INTO %s VALUES('%s', '%s', '%s')",
        			   $this->dbTable, 
                       $this->dbConnection->escape_string($id),
                       $this->dbConnection->escape_string($data),
                       time());
        return $this->dbConnection->query($sql);

    }

    /**
     * Destoroy the session
     * @param int session id
     * @return bool
     */
    public function destroy($id) {

        $sql = sprintf("DELETE FROM %s WHERE `id` = '%s'", $this->dbTable, $this->dbConnection->escape_string($id));
        return $this->dbConnection->query($sql);

	}
	
	

    /**
     * Garbage Collector
     * @param int life time (sec.)
     * @return bool
     * @see session.gc_divisor      100
     * @see session.gc_maxlifetime 1440
     * @see session.gc_probability    1
     * @usage execution rate 1/100
     *        (session.gc_probability/session.gc_divisor)
     */
    public function gc($max) {

        $sql = sprintf("DELETE FROM %s WHERE `timestamp` < '%s'", $this->dbTable, time() - intval($max));
        return $this->dbConnection->query($sql);

    }

}//class
