<?php

class Content extends DBClass {
	public $currentUserId = "null";
	public $currentUserFName = "null";
	public $currentUserLName = "null";
	public $currentUserEmail = "null";
	public $BOOLTRUE = '1';
	public $BOOLFALSE = '0';
	public function __construct() {
	$services_json = json_decode(getenv("VCAP_SERVICES"),true);
		$mysql_config = $services_json["mysql-5.1"][0]["credentials"];
		$this->DBusername = $mysql_config["username"];
		$this->DBpassword = $mysql_config["password"];
		$this->DBhost = $mysql_config["hostname"];	
		$this->DBport = $mysql_config["port"];	
		$this->DBName = $mysql_config["name"];
	
		$con =  mysql_connect($this->DBhost, $this->DBusername, $this->DBpassword);
		mysql_set_charset("utf8");
			if (!$con) 
			{
				//die('Could not connect on construction: ' . mysql_error());
				return false;
			}
	}
	
	public function saveContent($contentId, $contentData, $type){
		//echo $contentId." contentData is: " . $contentData;
		$cleanContentId = mysql_real_escape_string($contentId);
		if($type === 'video'){
			$contentHTMLData = mysql_real_escape_string($contentData);
		}
		else {
			$contentHTMLData = mysql_real_escape_string($contentData);
		}
		//$query = "UPDATE $this->DBName.content SET content = '$cleanContentData',  lastModified = NOW() WHERE contentId ='$cleanContentId'";
		//echo $query;	
		$query = sprintf("UPDATE $this->DBName.content SET content = '%s',  lastModified = NOW() WHERE contentId ='%s'",
            $contentHTMLData,
            mysql_real_escape_string($contentId));
		$query2 = "UPDATE $this->DBName.users_content SET lastModified = NOW() WHERE contentId ='$cleanContentId'";
		$result = mysql_query($query) or die(mysql_error());
		if(mysql_affected_rows() == 0) {
			$resultArray = array("status" => "FAIL");
		}
		else {
			if($type == 'video'){
				$returnValue = $contentData;
			}
			else {
				$returnValue = mysql_real_escape_string($contentHTMLData);
			}
			$resultArray = array("status" => "SUCCESS", "contentId" => $cleanContentId, "updatedContent" => $returnValue );
		}
		return $resultArray;
	}

	public function deleteItem($contentId){
		$userId = $_SESSION['userId'];
		$cleanContentId = mysql_real_escape_string($contentId);
		$query = "UPDATE $this->DBName.users_content SET lastModified = NOW(), active = '$this->BOOLFALSE' WHERE contentId ='$cleanContentId' AND userId = '$userId'";
		if(mysql_query($query)) {
			$resultArray = array("status" => "SUCCESS");
		}
		else {
			$resultArray = array("status" => "ERROR");
			}
		return $resultArray;
	}
	
	public function createContent($contentId, $contentName, $contentData, $type){
		
		//echo $contentId." contentData is: " . $contentData;
		$cleanContentId = mysql_real_escape_string($contentId);
		$cleanContentName = mysql_real_escape_string($contentName);
		if($type == "video"){
			$cleanContentData = html_entity_decode($contentData);
		} 
		else {
			$cleanContentData = mysql_real_escape_string($contentData);
		}
		
		$cleanType = mysql_real_escape_string($type);
		$query = "INSERT INTO $this->DBName.content VALUES ('', '$cleanContentId', '$cleanContentData', NOW(), NOW(), '$cleanType', 'free')";
		//echo $query;	
		$result = mysql_query($query) or die(mysql_error());
		if(mysql_affected_rows() == 0) {
			$resultArray = array("status" => "FAIL");
		}
		else {
			$resultArray = array("status" => "SUCCESS");
		}
		return $resultArray;
	}
	
	public function deleteContent($contentId){
		$cleanContentId = mysql_real_escape_string($contentId);
		$query = "UPDATE $this->DBName.users_content SET lastModified = NOW(), active ='$this->BOOLFALSE' WHERE contentId ='$cleanContentId'";
		mysql_query($query);
	}
	
	public function createContentLoggedIn($contentName, $contentData, $contentType){
		$cleanContentId = $this->rollTheDice();
		$suppliedUserId = $_SESSION['userId'];
		$cleanContentType = mysql_real_escape_string($contentType);
		//echo $contentId." contentData is: " . $contentData;
		$billing = new Billing();
		$cleanContentName = mysql_real_escape_string($contentName);
		if($type === "video"){
			$cleanContentData = htmlentities($contentData, ENT_QUOTES);
		} 
		else {
			$cleanContentData = mysql_real_escape_string($contentData);
		}
		$planId = $billing->getUserPlanId($suppliedUserId);
		$query = "INSERT INTO $this->DBName.content VALUES ('', '$cleanContentId', '$cleanContentData', NOW(), NOW(), '$cleanContentType', '$planId')";
		//echo $query;	
		$query2 = "INSERT INTO $this->DBName.users_content VALUES ('', '$suppliedUserId', '$cleanContentId', '$cleanContentName', NOW(), NOW(), '$this->BOOLTRUE')";
		$result2 = mysql_query($query2) or die(mysql_error());
		$result = mysql_query($query) or die(mysql_error());
		if(mysql_affected_rows() == 0) {
			$resultArray = array("status" => "FAIL");
		}
		else {
			$resultArray = array("status" => "SUCCESS", "contentId" => $cleanContentId, "newUrl" => $this->base64_url_encode($cleanContentId));
		}
		return $resultArray;
	}
	
	public function retrieveContentByPage($pageId, $userId){
		
		$cleanPageId = mysql_real_escape_string($pageId);
		$query = "SELECT content.contentId, content.content FROM $this->DBName.content, $this->DBName.pages_content WHERE pages_content.contentId = content.contentId AND pages_content.pageId = '$cleanPageId'";
		//echo $query;				
		$result = mysql_query($query) or die(mysql_error());
		if(mysql_num_rows($result) == 0 ) {
			$resultArray = array("status" => "ERROR", "content" => "<h3> Sorry, that content doesn't exist</h3>" );
		}
		else {
			while($row = mysql_fetch_array($result)) {
				$resultArray = array("status" => "SUCCESS", "content" => html_entity_decode($row['content']), "contentId" => $row['contentId']);
			}
		}
		return $resultArray;
	}
	
	public function retrieveContent($contentId){
		$cleanContentId = mysql_real_escape_string($contentId);
		//$query = "SELECT content, type FROM $this->DBName.content WHERE contentId ='$cleanContentId'";
		/*
		$query = "SELECT contentId, content, 
						(SELECT COUNT(primaryKey) FROM $this->DBName.views WHERE contentId = '$cleanContentId') as contentViews, 
						billing_plans.planViewLimit as viewLimit, 
						type
						FROM $this->DBName.content, $this->DBName.billing_plans
						WHERE $this->DBName.content.planId = $this->DBName.billing_plans.planId
						AND contentId = '$cleanContentId'";
		*/				
		$query = "SELECT contentId, content, type, planId, 
					(SELECT COUNT(primaryKey) FROM $this->DBName.views WHERE MONTH(dateViewed) = MONTH(now())  
						AND contentId IN 
							( SELECT contentId FROM $this->DBName.users_content WHERE userId =  
								(SELECT userId FROM $this->DBName.users_content WHERE contentId = '$cleanContentId')
								)
							) as totalViews, 
					(SELECT planViewLimit FROM $this->DBName.billing_plans WHERE content.planId = billing_plans.planId) as planLimitViews 
				 FROM $this->DBName.content WHERE contentId = '$cleanContentId'";
		
		//echo $query;				
		$result = mysql_query($query) or die(mysql_error());
		if(mysql_num_rows($result) == 0 ) {
			$resultArray = array("status" => "ERROR", "content" => '<h1 class="text-center"> Sorry, that content doesn&#39;t exist.</h1>' );
		}
		else {
			while($row = mysql_fetch_array($result)) {
				if($row['totalViews'] >= $row['planLimitViews']){
					$resultArray = array("status" => "OVERLIMIT", "content" => '<h1 class="text-center"> Sorry, this content has reached its view limit.</h1>' );
				}
				else if($row['type'] == 'image'){
					$resultArray = array("status" => "SUCCESS", "type" => "image", "content" => '<a href="'.$row['content'].'"><img src="'.$row['content'].'" alt="Picture"/></a>', "url" => $this->base64_url_encode($cleanContentId) );
				}
				else if($row['type'] == 'video'){
					$resultArray = array("status" => "SUCCESS", "type" => "video", "content" => html_entity_decode($row['content']), "rawContent" => $row['content'], "url" => $this->base64_url_encode($cleanContentId) );
				}
				else {
					$resultArray = array("status" => "SUCCESS", "type" => "text", "content" => html_entity_decode($row['content']), "url" => $this->base64_url_encode($cleanContentId));
				}
			}
		}
		return $resultArray;
	}
	
	public function retrieveContentLoggedIn($contentId){
		$cleanContentId = mysql_real_escape_string($contentId);
		//$query = "SELECT content, type FROM $this->DBName.content WHERE contentId ='$cleanContentId'";
		$query = "SELECT contentId, content, 
						COUNT(contentId) as contentViews, 
						billing_plans.planViewLimit as viewLimit, 
						type
						FROM $this->DBName.content, $this->DBName.billing_plans
						WHERE $this->DBName.content.planId = $this->DBName.billing_plans.planId
						AND contentId = '$cleanContentId'";
		//echo $query;				
		$result = mysql_query($query) or die(mysql_error());
		if(mysql_num_rows($result) == 0 ) {
			$resultArray = array("status" => "ERROR", "content" => '<h1 class="text-center"> Sorry, that content doesn&#39;t exist.</h1>' );
		}
		else {
			while($row = mysql_fetch_array($result)) {
				$analytics = new Analytics();
				$range = date("Y-m-d") . "|" . date("Y-m-d");
				$viewsArray = $analytics->updateAnalytics($range, $cleanContentId);
				if($row['type'] == 'image'){
					$resultArray = array("status" => "SUCCESS", "type" => "image", "content" => '<a href="'.$row['content'].'"><img src="'.$row['content'].'" alt="Picture"/></a>', "url" => $this->base64_url_encode($cleanContentId) , "views" => $viewsArray['views'], "chart" => $viewsArray['chart'] );
				}
				else if($row['type'] == 'video'){
					$resultArray = array("status" => "SUCCESS", "type" => "video", "content" => html_entity_decode($row['content']), "rawContent" => $row['content'],  "url" => $this->base64_url_encode($cleanContentId), "views" => $viewsArray['views'], "chart" => $viewsArray['chart']);	
				}
				else {
					$resultArray = array("status" => "SUCCESS", "type" => "text", "content" => html_entity_decode($row['content']), "url" => $this->base64_url_encode($cleanContentId), "views" => $viewsArray['views'], "chart" => $viewsArray['chart']);
				}
			}
		}
		return $resultArray;
	}
		
	public function createNewPage($pangoId, $pageName, $userId){
		$cleanPangoId = mysql_real_escape_string($pangoId);
		$cleanPageName = mysql_real_escape_string($pageName);
		$cleanUserId = mysql_real_escape_string($userId);
		/*
			VALIDATE REQUEST
		*/
		$newPageId = uniqid("page");
		$newContentId = uniqid("content");
		if( $this->insertToPagesContent($newPageId, $newContentId) && $this->insertToPages($newPageId, $cleanPageName) && $this->insertToPangoPages($cleanPangoId, $newPageId) && $this->insertToContent($newContentId) ){
		$returnArray = array("status" => "SUCCESS", "pageId" => $newPageId, "contentId" => $newContentId);
		return $returnArray;
		}
		else {
			$returnArray = array("status" => "ERROR");
		}
	}
	
	public function deletePage($pageId, $userId){
		$cleanPageId = mysql_real_escape_string($pageId);
		$cleanUserId = mysql_real_escape_string($userId);
		/*
			VALIDATE REQUEST
		*/
		$query = "UPDATE $this->DBName.pages_content SET active = '$this->BOOLFALSE' WHERE pageId = '$cleanPageId'";
		$query2 = "UPDATE $this->DBName.pages SET active = '$this->BOOLFALSE' WHERE pageId = '$cleanPageId'";
		$query3 = "UPDATE $this->DBName.pango_pages SET active = '$this->BOOLFALSE' WHERE pagesId = '$cleanPageId'";
		//echo $query3;
		mysql_query($query3) or die(mysql_error());
		if(mysql_query($query3)){
			$resultArray = array("status" => "SUCCESS");
		}
		else {
			$resultArray = array("status" => "FAIL");
		}
		return $resultArray;
	}

	public function insertToPango($suppliedPangoId, $suppliedPangoName) {
	//echo $suppliedPangoId;
		$query = "INSERT INTO $this->DBName.pango VALUES('', '$suppliedPangoId', '$suppliedPangoName', NOW(), NOW())";
		//echo $query;
		if(mysql_query($query) or die(mysql_error())){
			return true;
		}
		else {
			return false;
		}
	}
	
	public function insertToUsersPango($suppliedUserId, $suppliedPangoId){
		$query = "INSERT INTO $this->DBName.users_pango VALUES ('','$suppliedUserId','$suppliedPangoId','$this->BOOLTRUE')";
		if(mysql_query($query) or die(mysql_error())){
				return true;
			}
			else {
				return false;
			}		
	}
	
	public function insertToPagesContent($suppliedPageId, $suppliedContentId) {
		$query1 = "INSERT INTO $this->DBName.pages_content (`primaryKey`, `pageId`, `contentId`, `active`) VALUES ('','$suppliedPageId','$suppliedContentId', '$this->BOOLTRUE')";
		if(mysql_query($query1) or die(mysql_error())){
			return true;
		}
		else {
			return false;
		}
	}
	
	public function insertToPages($suppliedPageId, $suppliedPageName) {
		$query2 = "INSERT INTO $this->DBName.pages (`primaryKey`, `pageId`, `pageName`, `dateCreated`, `lastModified` , `active`) VALUES ('','$suppliedPageId','$suppliedPageName', NOW(), NOW(), '$this->BOOLTRUE')";
		if(mysql_query($query2) or die(mysql_error())){
			return true;
		}
		else {
			return false;
		}
	}
	
	public function insertToPangoPages($suppliedPangoId, $suppliedPageId) {
		$query3 = "INSERT INTO $this->DBName.pango_pages (`primaryKey`, `pangoId`, `pagesId`, `active`) VALUES ('','$suppliedPangoId','$suppliedPageId', '$this->BOOLTRUE')";
		if(mysql_query($query3) or die(mysql_error())){
			return true;
		}
		else {
			return false;
		}		
	}
	
	public function createNewPango($suppliedUserId, $newPangoName){
		$cleanUserId = mysql_real_escape_string($suppliedUserId);
		$cleanPangoName = mysql_real_escape_string($newPangoName);
		$newPangoId = uniqid("p");
		if($this->insertToPango($newPangoId, $cleanPangoName) && $this->insertToUsersPango($suppliedUserId, $newPangoId)){
			$resultArray = array("status" => "SUCCESS", "pangoId" => $newPangoId);
		}
		else {
			$resultArray = array("status" => "FAIL");
		}
		return $resultArray;
	}
	
	public function insertToContent($suppliedContentId) {
		$query4 = "INSERT INTO $this->DBName.content (`primaryKey`, `contentId`, `content`, `dateCreated`, `lastModified`) VALUES ('' ,'$suppliedContentId' ,'',now(), now())";
		if(mysql_query($query4) or die(mysql_error())){
			return true;
		}
		else {
			return false;
		}	
	}
	
	public function updatePageContent($contentId, $userId){
		
	}
	
	public function insertDEMOPicture($contentId, $contentData){
		//$cleanContentId = $this->rollTheDice();
		$cleanContentId = mysql_real_escape_string($contentId);
		$cleanContentData = mysql_real_escape_string($contentData);
		$query = "INSERT INTO $this->DBName.content VALUES ('', '$cleanContentId', '$cleanContentData', NOW(), NOW(), 'image', 'free') ";
		//$query2 = "INSERT INTO $this->DBName.users_content VALUES ('', '$suppliedUserId', '$cleanContentId', '$cleanContentName', NOW(), NOW())";
		//echo $query;				
		$result = mysql_query($query) or die(mysql_error());
		//$result2 = mysql_query($query2) or die(mysql_error());
		if(mysql_affected_rows() == 0) {
			$resultArray = array("status" => "FAIL");
		}
		else {
			$resultArray = array("status" => "SUCCESS");
		}
		return $resultArray;
	}
	
	public function insertPicture($contentData, $contentName){
		$cleanContentId = $this->rollTheDice();
		$cleanContentName = mysql_real_escape_string($contentName);
		$cleanContentData = mysql_real_escape_string($contentData);
		$suppliedUserId = $_SESSION['userId'];
		$billing = new Billing();
		$planId = $billing->getUserPlanId($suppliedUserId);
		$query = "INSERT INTO $this->DBName.content VALUES ('', '$cleanContentId', '$cleanContentData', NOW(), NOW(), 'image', '$planId') ";
		$query2 = "INSERT INTO $this->DBName.users_content VALUES ('', '$suppliedUserId', '$cleanContentId', '$cleanContentName', NOW(), NOW(), '$this->BOOLTRUE')";
		//echo $query;				
		$result = mysql_query($query) or die(mysql_error());
		$result2 = mysql_query($query2) or die(mysql_error());
		if(mysql_affected_rows() == 0) {
			$resultArray = array("status" => "FAIL");
		}
		else {
			$resultArray = array("status" => "SUCCESS", "contentId" => strval($cleanContentId), "newUrl" => $this->base64_url_encode($cleanContentId));
		}
		return $resultArray;
	}
	
	public function listPangos($userId) {
		$query = "SELECT DISTINCT t1.pangoId as pangoId, t1.pangoName as pangoName FROM $this->DBName.pango JOIN ( $this->DBName.pango as t1, $this->DBName.users_pango as t2 )ON (t1.pangoId = t2.pangoId) WHERE t2.userId = '$userId' AND active = '$this->BOOLTRUE' ORDER BY t1.lastModified ASC";
		//echo $query;
		$result = mysql_query($query) or die(mysql_error());
		
		if(mysql_num_rows($result) == 0){
			echo '';
		}
		else {
			while($row = mysql_fetch_array($result)){
				
			echo '<li role="presentation"><a role="menuitem" tabindex="-1" href="#" class="selectPango" id="'.$row['pangoId'].'">'.$row['pangoName'].'</a></li>';
			}
		}
	}
	
	public function listPages($pangoId){
		$query = "SELECT pages.pageId as pageId, pages.pageName as pageName FROM  $this->DBName.pages,  $this->DBName.pango_pages WHERE  pango_pages.pagesId = pages.pageId AND pango_pages.pangoId = '$pangoId' AND pango_pages.active = '$this->BOOLTRUE' ORDER BY dateCreated ASC";
		//echo $query;
		$result = mysql_query($query) or die(mysql_error());
		
		if(mysql_num_rows($result) == 0){
			echo '';
		}
		else {
			while($row = mysql_fetch_array($result)){
				
			echo '<li> <a href="#" id="'.$row['pageId'].'" class="selectPage">'.$row['pageName'].'</a></li>';
			}
		}
	}
	
	public function listPagesAjax($pangoId){
		$cleanPangoId = mysql_real_escape_string($pangoId);
		$query = "SELECT DISTINCT t2.pageName, t2.pageId, t3.contentId FROM $this->DBName.pango_pages as t1, $this->DBName.pages as t2, $this->DBName.pages_content as t3 WHERE t1.pangoId = '$cleanPangoId' AND t1.pagesId = t2.pageId AND t2.pageId = t3.pageId ORDER BY t2.dateCreated";
		$result = mysql_query($query) or die(mysql_error());
		if(mysql_num_rows($result) == 0){
			$resultArray = array("status" => "ZERO_RESULT");
		}
		else {
			$pagesArray = array();
			while($row = mysql_fetch_array($result)){
				$itemArray = array("pageName" => $row['pageName'], "pageId" => $row['pageId']);
				array_push($pagesArray, $itemArray);
				//echo '<li> <a href="#" id="'.$row['pageId'].'" class="selectPage">'.$row['pageName'].'</a></li>';
				
			}
			$resultArray = array("status" => "SUCCESS", "pagesData" => $pagesArray);
		}
		return $resultArray;
	}

	public function getContentByUserId($userId){
		$cleanUserId = mysql_real_escape_string($userId);
		$query = "SELECT contentId, contentName FROM $this->DBName.users_content WHERE userId = '$cleanUserId' AND active = '$this->BOOLTRUE' ORDER BY lastModified ASC ";
		$result = mysql_query($query) or die(mysql_error());
		if(mysql_num_rows($result) == 0) {
			echo '<li class="noItems itemSpecial btn btn-rounded"> 0 QR Codes/Short URLs</li>'; 
		}
		else {
			while($row=mysql_fetch_assoc($result)){
			echo '<li><a href="#" id="'.$row['contentId'].'" class="itemSpecial btn btn-rounded btn-info existingItem">'.$row['contentName'].'</a> </li>';
			
			}
		}
	}

	function base64_url_encode($input)
	{
    	//return strtr(base64_encode($input), '+/=', '-_,');
		return base_convert(intval($input), 10, 32);
	}
 
	function base64_url_decode($input)
	{
    	return base_convert($input, 32, 10);
	}

    function random_string ($length = 16)
    {
        //$string = sha1(microtime(true).mt_rand(10000,90000));
        $string = rand(999, 9999999999);
        //for ($n = 0; $n < count($length); $n++) $string .= chr(rand(32, 126));
		return $string;

    }
	
	function rollTheDice(){
		 //$string = sha1(microtime(true).mt_rand(10000,90000));
        $string = rand(999, 9999999999);
        //for ($n = 0; $n < count($length); $n++) $string .= chr(rand(32, 126));
		
		if($this->stringCheck($string)){
			return $string;
		}
		else {
			$this->rollTheDice();
		}
	}

    function stringCheck($stringToCheck){
		$query = "SELECT contentId FROM $this->DBName.content WHERE contentId = '$stringToCheck'";
		$result = mysql_query($query) or die(mysql_error());
		if(mysql_num_rows($result) == 0){
			return true;
		}
		else {
			return false;
			}
	}
	
	public function __destruct() {
	}
}

?>