<?php

class Security {
	public $skey = 'e1d}5f|H*:2*52$34Fw"2.W'; // you can change it
	public $salt = "X)D2[[!>Et&w+ @Q21m-e>g";
	public $newSecurityToken = "null";
    public  function safe_b64encode($string) {
        $data = base64_encode($string);
        $data = str_replace(array('+','/','='),array('-','_',''),$data);
        return $data;
    }

    public function safe_b64decode($string) {
        $data = str_replace(array('-','_'),array('+','/'),$string);
        $mod4 = strlen($data) % 4;
        if ($mod4) {
            $data .= substr('====', $mod4);
        }
        return base64_decode($data);
    }
	
	public function createToken($userID) {
	// Token = (userId + Salt + hash_unique). 
	// Hash = (userId + date)
	// output an auth_hash, and auth_token
	
	}

    public  function encode($value){ 
        if(!$value){return false;}
        $text = $value;
        $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);
        $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
        $crypttext = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $this->skey,$text, MCRYPT_MODE_ECB, $iv);
        return trim($this->safe_b64encode($crypttext)); 
    }

    public function decode($value){
        if(!$value){return false;}
        $crypttext = $this->safe_b64decode($value); 
        $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);
        $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
        $decrypttext = mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $this->skey, $crypttext, MCRYPT_MODE_ECB, $iv);
        return trim($decrypttext);
    }
	
	public function validateToken($suppliedId, $suppliedToken){
		$suppliedTokenPlainText = $this->decode($suppliedToken);
		$tokenPieces = explode("|", $suppliedTokenPlainText);
		$pos = strpos($tokenPieces[0], $suppliedId);

		//echo "Full plaintext = ". $suppliedTokenPlainText ."<br />";
		if ($pos === false) {
			//echo "The string '$suppliedId' was not found in the string '$tokenPieces[0]'";
			return false;
		} 
		else {
			//echo "The string '$suppliedId' was found in the string '$tokenPieces[0]'";
			//echo " and exists at position $pos";
			return true;
		}
	}
	
	public function generateNewToken($suppliedUserId){
		$expireDate = date('Y-m-d H:i:s T', strtotime('+3 days'));
		$str = $suppliedUserId. "|" . $expireDate;
		$this->newSecurityToken = $this->encode($str);
		return $this->newSecurityToken;
	}
}

?>