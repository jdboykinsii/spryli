<?php 
require('config.php');
class Billing {
	public $BOOLTRUE = '1';
	public $BOOLFALSE = '0';
	public function __construct() {
		$services_json = json_decode(getenv("VCAP_SERVICES"),true);
		$mysql_config = $services_json["mysql-5.1"][0]["credentials"];
		$this->DBusername = $mysql_config["username"];
		$this->DBpassword = $mysql_config["password"];
		$this->DBhost = $mysql_config["hostname"];	
		$this->DBport = $mysql_config["port"];	
		$this->DBName = $mysql_config["name"];

		$con =  mysql_connect($this->DBhost, $this->DBusername, $this->DBpassword);
			if (!$con) 
			{
				//die('Could not connect on construction: ' . mysql_error());
				return false;
			}
	}
	
	public function getPlanDetails($planId){
		$cleanPlanId = mysql_real_escape_string($planId);
		$query = "SELECT * FROM $this->DBName.billing_plans WHERE planId = '$cleanPlanId'";
		$result = mysql_query($query) or die(mysql_error());
		if(mysql_num_rows($result) == 0) {
			$resultArray=array('status' => 'ERROR', 'message' => "Invalid Plan");
		}
		else {
			while($row=mysql_fetch_assoc($result)){
			
				$resultArray=array('status' => 'SUCCESS', "planName" => ucfirst($row['planName']), "price" => $row['planPrice'], "priceString" =>str_replace(array("$", "."), '', $row['planPrice']), "limit" => $this->formatNumberString($row['planViewLimit']), "limitString" => $row['planViewLimit'], 'interval' => $row['interval']);
			
			}
			return $resultArray;
		}
	}
	
	public function getUserPlanId($suppliedUserId){
		$cleanUserId = mysql_real_escape_string($suppliedUserId);
		$query = "SELECT planId FROM $this->DBName.users_billing_plans WHERE userId = '$cleanUserId'";
		$result = mysql_query($query);
		if(mysql_num_rows($result) == 0) {
			
			return "ERROR";
		}
		else {
			$row = mysql_fetch_assoc($result);
			return $row['planId'];
		}
	}
	
	public function listPlans(){
	$query = "SELECT * FROM $this->DBName.billing_plans WHERE planActive = '$this->BOOLTRUE' and planId <> 'free' and planId <> 'test'";
		$result = mysql_query($query);
		if(mysql_num_rows($result) == 0) {
			$resultArray=array('status' => 'ERROR', 'message' => "Invalid Plan");
			return $resultArray;
		}
		else {
			while($row=mysql_fetch_assoc($result)){
			if($row['interval'] == "ppu"){
				$color = "green";
				$colorDash = "-green";
				$usage = "for " .$row['planViewLimit'] . " views";
				$period = "Views";
			}
			else {
				$color = "";
				$colorDash = "";
				$usage = "per month";
				$period = "Views/Month";
			}
			if($row['planName'] === "plus") {$popular = " tile-hot ";} else {$popular = "";}
			echo '<div class="span3 pricing-table tile '.$popular.$row['interval'].'">
					<ul>
						<li class="pricing-header-row-1'.$colorDash.'">
						<h2 class="white"><span>'.ucfirst($row['planName']).'</span></h2>
						
						</li>
						<li class="pricing-header-row-2">
						<h3 class="price '.$color.'">'.substr($row['planPrice'], 0, 3).'</h3>
							<small> '.$usage.'</small>
						</li>
						<li class="pricing-content-row-odd">
							<strong> UNLIMITED </strong>QR codes / Short URLs
						</li>
						<li class="pricing-content-row-even">
							<strong> '.$this->formatNumberString($row['planViewLimit']).' </strong> '.$period.'
						</li>
						<li class="pricing-content-row-odd">
							<span> Analytics Included</span>
						</li>
						<li class="pricing-content-row-even">
							<span> Print &amp; Deliver (Coming Soon) </span>
						</li>
						<li class="pricing-content-row-odd">
							<span> No Ads </span>
						</li>
						<li class="pricing-footer'.$colorDash.'">
						<a class="btn btn-small btn-primary btn-rounded but-price" href="signup.php?planId='.$row['planId'].'">
						 <h5><span>Sign up!</span></h5></a>
						</li>
					</ul>
				</div>';
				
				
			}
		}
	
	
	}

	public function listPlansBillingPage($suppliedPlanId){
	$query = "SELECT * FROM $this->DBName.billing_plans WHERE planActive = '$this->BOOLTRUE' and planId <> 'free' and planId <> 'test' and planId <> '$suppliedPlanId' and planId <> 'ppu'";
		$result = mysql_query($query);
		if(mysql_num_rows($result) == 0) {
			$resultArray=array('status' => 'ERROR', 'message' => "Invalid Plan");
			return $resultArray;
		}
		else {
			while($row=mysql_fetch_assoc($result)){
			if($row['interval'] == "ppu"){
				$color = "green";
				$colorDash = "-green";
				$usage = "for " .$row['planViewLimit'] . " views";
				$period = "Views";
			}
			else {
				$color = "";
				$colorDash = "";
				$usage = "per month";
				$period = "Views/Month";
			}
			if($row['planName'] === "plus") {$popular = " tile-hot ";} else {$popular = "";}
			echo '<div class="span3 pricing-table tile '.$popular.$row['interval'].'">
					<ul>
						<li class="pricing-header-row-1'.$colorDash.'">
						<h2 class="white"><span>'.ucfirst($row['planName']).'</span></h2>
						
						</li>
						<li class="pricing-header-row-2">
						<h3 class="price '.$color.'">'.substr($row['planPrice'], 0, 3).'</h3>
							<small> '.$usage.'</small>
						</li>
						<li class="pricing-content-row-odd">
							<strong> UNLIMITED </strong>QR codes / Short URLs
						</li>
						<li class="pricing-content-row-even">
							<strong> '.$this->formatNumberString($row['planViewLimit']).' </strong> '.$period.'
						</li>
						<li class="pricing-content-row-odd">
							<span> Analytics Included</span>
						</li>
						<li class="pricing-content-row-even">
							<span> Print &amp; Deliver (Coming Soon) </span>
						</li>
						<li class="pricing-content-row-odd">
							<span> No Ads </span>
						</li>
						<li class="pricing-footer'.$colorDash.'">
						<a class="btn btn-small btn-primary btn-rounded but-price" href="signup.php?planId='.$row['planId'].'">
						 <h5><span>Change Plan</span></h5></a>
						</li>
					</ul>
				</div>';
				
				
			}
		}
	
	
	}

	public function insertCustomerID($customerUsername, $email, $planID, $customerID){
		// Sanitize
		$cleanFinalUsername = mysql_real_escape_string($customerUsername);
		$cleanFinalEmail = mysql_real_escape_string($email);

		$sql="INSERT INTO $this->DBName.billing (id, username, email, planID, customerStripeID, serviceActive) VALUES ('', '$cleanFinalUsername','$cleanFinalEmail', '$planID', '$customerID', 'ACTIVE')";
		
		// Insert Query
		if (!mysql_query($sql,$con))
		{
			die('<h2>Error: ' . mysql_error(). ' </h2>');
		}
	}
	
	public function chargeCustomer($suppliedUserId, $suppliedPlanId, $cardholder_name, $card, $card_cvc, $exp_month, $exp_year, $address_line1, $address_line2, $address_zip, $address_state) {
			// First, we get the userId passed to us. We'll use it to tag what customer we just billed. 
			$user = new User();
			$userEmail = $user->retrieveUserEmail($suppliedUserId);
			// We'll also have the plan ID passed to us. With that, we'll get the price we bill for. Be sure to poass it the string!
			$planArray = $this->getPlanDetails($suppliedPlanId);
			$amount = $planArray['priceString'];
			
			
			// Create card Array
			$cardDetails = array("name" => $cardholder_name,
						'number' => $card,
						'cvc' => $card_cvc,
						"exp_month" => $exp_month,
						"exp_year" => $exp_year,
						"email" => $userEmail, 
						"address_line1" => $address_line1,
						"address_line2" => $address_line2,
						"address_zip" => $address_zip,
						"address_state" => $address_state,
						"address_country" => 'USA');
						

		try {
		$chargeRequest = Stripe_Charge::create(array( 
		"amount" => $amount, 
		"currency" => "usd", 
		"card" => $cardDetails,
		 "description" => 'One time charge for ' . $planArray['planName'] . ' to user: ' . $userEmail)
		 );
			 //return status array. 
			 $statusArray = array("status" => 'SUCCESS', "stripeCustId" => $chargeRequest->id);
			 return $statusArray;
		}
		catch (Exception $e) {
			$statusArray = array("status" => 'FAIL', "message" => $e->getMessage());
			return $statusArray;
		  }		
					
	}
	
	public function subscribeCustomer($suppliedEmail, $suppliedPlanId, $cardholder_name, $card, $card_cvc, $exp_month, $exp_year, $address_line1, $address_line2, $address_city, $address_zip, $address_state, $coupon) {
			// First, we get the userId passed to us. We'll use it to tag what customer we just billed. 
			$userEmail = $suppliedEmail;
			// We'll also have the plan ID passed to us. With that, we'll get the price we bill for. Be sure to poass it the string!
			$planArray = $this->getPlanDetails($suppliedPlanId);
			$amount = $planArray['priceString'];
			
			// Create card Array
			$cardDetails = array("name" => $cardholder_name,
						'number' => $card,
						'cvc' => $card_cvc,
						"exp_month" => $exp_month,
						"exp_year" => $exp_year,
						"email" => $userEmail, 
						"address_line1" => $address_line1,
						"address_line2" => $address_line2,
						"address_zip" => $address_zip,
						"address_state" => $address_state,
						"address_country" => 'USA');
		try {
		$chargeRequest = Stripe_Customer::create(array(
        "card" => $cardDetails,
		"plan" => strtolower($planArray['planName']),
		"email" => $email,
		"coupon" => $coupon,
		"description" => $userEmail .' subscribed to spryli ' . strtolower($planArray['planName']) . ' plan.'));
			 //return status array. 
			 $statusArray = array("status" => 'SUCCESS', "stripeCustId" => $chargeRequest->id);
			 return $statusArray;
		}
		catch (Exception $e) {
			$statusArray = array("status" => 'CARD_ERROR', "message" => $e->getMessage());
			return $statusArray;
		  }		
					
	}
	
	public function validateSubscription(){
		$userId = $_SESSION['userId'];
		$query = "SELECT stripeId FROM $this->DBName.users_billing_plans WHERE userId = '$userId'";
		$result = mysql_query($query) or die(mysql_error());
		if(mysql_num_rows($result) == 0){
			$returnArray = array("status" => "ERROR");
			return $returnArray;
		}
		else {
			$row = mysql_fetch_assoc($result);
			$customerStatus = Stripe_Customer::retrieve($row['stripeId']);
			$returnArray = array("status" => "SUCESS", "subscriptionActive" => $customerStatus['delinquent']);
			return $returnArray;
		}
	}
	
	public function cancelBilling($suppliedStripeId){
		$cu = Stripe_Customer::retrieve($suppliedStripeId);
		$result = $cu->cancelSubscription();
		return $result;
	}
	
	public function billUserForViews($userId){
	
	}
	
	public function cancelSubscription($userId){
	
	}
	
	public function verifyStatus($userId){
	
	}

	function formatNumberString($numberString) {
		if(strlen($numberString) >= 4){
			// add comma after 3rd string from end of string.
			$numberString = strrev($numberString);
			$numberString = substr($numberString, 0, 3) . ",". substr($numberString, 3);
			$numberString = strrev($numberString);
			return $numberString;
		}
		else {
			return $numberString;
		}
	}

	function base64_url_encode($input)
	{
    	//return strtr(base64_encode($input), '+/=', '-_,');
		return base_convert(intval($input), 10, 32);
	}
 
	function base64_url_decode($input)
	{
    	return base_convert($input, 32, 10);
	}
}
?>