<?php

class Mail {
	public function sendWelcomeEmail($email){
		$message = '<html><body style="background:url(http://spryli.com/img/email/bkgrnd.jpg); min-height: 400px;
					  font-family: Helvetica Neue, Helvetica, Arial, sans-serif;">
					<table style="
					  min-height: 20px;
					  margin-top: 50px;
					  padding: 19px;
					  margin-bottom: 20px;
					  background-color: #f5f5f5;
					  border: 1px solid #eee;
					  border: 1px solid rgba(0, 0, 0, 0.05);
					  -webkit-border-radius: 4px;
					  -moz-border-radius: 4px;
					  border-radius: 4px;
					  -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.05);
					  -moz-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.05);
					  box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.05);margin-top: 50px;
						display: block;
						margin-right: auto;
						margin-left: auto;
						width: 600px;">
					<tr> 
					<td> </td>
					</tr>
					<tr> ';
		$message .=' <td style="background-color: #323A45;
					  background-repeat: repeat-x;
					  border-color: #7abcff #7abcff #1f6377;
					  border-color: rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.25);
					  *background-color: #7abcff;
					  -webkit-border-radius: 4px;
					  -moz-border-radius: 4px;
					  border-radius: 4px;
					  color: #fff;
					  filter: progid:DXImageTransform.Microsoft.gradient(enabled = false)"><img align="center" src="http://spryli.com/img/spryli.png"><br /> <h1 align="center"> Thanks for signing up! </h1></td>
					</tr>
					<tr> 
					<td>  <p label="Description">Thank you for signing up!</p>
					<p>This email is to confirm that an account for Spryli.com was created using this email address!</p>
					<p> If you have any questions or comments, feel free to contact me personally at <a href="mailto:jay@spryli.com">jay@spryli.com</a></p>
														</td>
					</tr>

					</table>

					</body>
					</html>
';
		if($this->sendElasticEmail($email, "Spryli Account Creation", $message, $message, "jay@spryli.com", "Spryli"))
		{
			return true;
		}
		else 
		{
			return false;
		}
	}
	
	public function sendContactEmail($email, $message){
	
		if($this->sendElasticEmail('jay@spryli.com', "Spryli Contact Request", $message, $message, $email, "Spryli-Bot!"))
		{
			return true;
		}
		else 
		{
			return false;
		}
	}
	
	public function sendElasticEmail($to, $subject, $body_text, $body_html, $from, $fromName)
	{
		$res = "";

		$data = "username=".urlencode("jay@spryli.com");
		$data .= "&api_key=".urlencode("f92cd59a-c83a-4b77-8eab-2aa2f515ccda");
		$data .= "&from=".urlencode($from);
		$data .= "&from_name=".urlencode($fromName);
		$data .= "&to=".urlencode($to);
		$data .= "&subject=".urlencode($subject);
		if($body_html)
		  $data .= "&body_html=".urlencode($body_html);
		if($body_text)
		  $data .= "&body_text=".urlencode($body_text);

		$header = "POST /mailer/send HTTP/1.0\r\n";
		$header .= "Content-Type: application/x-www-form-urlencoded\r\n";
		$header .= "Content-Length: " . strlen($data) . "\r\n\r\n";
		$fp = fsockopen('ssl://api.elasticemail.com', 443, $errno, $errstr, 30);

		if(!$fp)
		  return "ERROR. Could not open connection";
		else {
		  fputs ($fp, $header.$data);
		  while (!feof($fp)) {
			$res .= fread ($fp, 1024);
		  }
		  fclose($fp);
		}
		return $res;                  
	}


}


?>
