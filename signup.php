<?php 
require('navigation.php');
require("system/dbconnect.php");
require("system/Billing/billingClass.php");
$billing = new Billing();

$planInfo = $billing->getPlanDetails($_GET['planId']);
if($planInfo['status'] === 'ERROR'){
	header('Location index.php');
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>spryli &middot; Sharing Made Viral</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">

<link rel="icon" type="image/png" href="img/favicon.ico">
<link href='http://fonts.googleapis.com/css?family=Lato:400,700,300' rel='stylesheet' type='text/css'>
<!--[if IE]>
	<link href="http://fonts.googleapis.com/css?family=Lato" rel="stylesheet" type="text/css">
	<link href="http://fonts.googleapis.com/css?family=Lato:400" rel="stylesheet" type="text/css">
	<link href="http://fonts.googleapis.com/css?family=Lato:700" rel="stylesheet" type="text/css">
	<link href="http://fonts.googleapis.com/css?family=Lato:300" rel="stylesheet" type="text/css">
<![endif]-->

<link href="css/bootstrap.css" rel="stylesheet">
<link href="css/font-awesome.min.css" rel="stylesheet">
<link href="css/theme.css" rel="stylesheet">
<link href="css/prettyPhoto.css" rel="stylesheet" type="text/css"/>
<link href="css/zocial.css" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" href="css/nerveslider.css">
<link href="css/wysiwyg.css" rel="stylesheet">
<!--[if lt IE 9]>
<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<!--[if IE 7]>
<link rel="stylesheet" href="css/font-awesome-ie7.min.css">
<![endif]-->
<style>
	#processing {
					display: none;
					}
</style>
</head>

<body>
<!--header-->
	<div class="header ">
		<!--logo-->
			<div class="container">
					<div class="logo">
						 <a href="index"><img src="img/spryli.png" alt="" class="animated bounceInDown" /></a>  
					</div>
					<!--menu-->
					<nav id="main_menu">
					<div class="menu_wrap">
						<ul class="nav sf-menu">
							<li class="active"><a href="index">Home</a> </li>
							<li class=""><a href="index" class="navItem" id=".demo">Demo</a> </li>
							<li class=""><a href="index" class="navItem" id=".pricing">Pricing</a> </li>
							<li class="last"><a href="login">Login</a></li>
						</ul>
					</div>
				</nav>
			</div>
		</div>
	<!--//header-->
	<!--page-->

		<!-- /SLIDER -->
		
	<div id="banner">
	<div class="container intro_wrapper">
	<div class="inner_content">
	
		</div>
			</div>
				</div>
				<!--//banner-->
			
	<div class="container wrapper">
	<div class="inner_content">
	<div class="pad45"></div>
	
	<!-- Demo -->
	<div class="row">
	<div class="span6">
	<div class="span4 pricing-table tile <?php if($planInfo['planName'] === "Plus") { echo " tile-hot ";} $planInfo['interval'];?>">
					<ul>
						<li class="pricing-header-row-1<?php if($planInfo['planName'] === "Pay Per Use") {echo '-green';} ?>">
						<h2 class="white"><span><?php echo $planInfo['planName'] ?></span></h2>
						</li>
						<li class="pricing-header-row-2">
						<h3 class="price <?php if($planInfo['planName'] === "Pay Per Use") {echo 'green';} ?>"><?php echo $planInfo['price']; ?></h3>
							<small> <?php echo $planInfo['interval']; ?></small>
						</li>
						<li class="pricing-content-row-odd">
							<strong> UNLIMITED </strong>QR codes / Short URLs
						</li>
						<li class="pricing-content-row-even">
							<strong> <?php echo $planInfo['limit'] ?> </strong> <?php if($planInfo['planName'] === "Pay Per Use") {echo 'Views';} else {echo 'Views / Month';}?>
						</li>
						<li class="pricing-content-row-even">
							<span> Analytics Included</span>
						</li>
						<li class="pricing-content-row-odd">
							<span> Print &amp; Deliver (Coming Soon)</span>
						</li>
						<li class="pricing-content-row-even">
							<span> No Ads </span>
						</li>
						<li class="pricing-footer<?php if($planInfo['planName'] === "Pay Per Use") {echo '-green';} ?>">
						<a class="btn btn-small btn-primary btn-rounded but-price" href="#">
						 <h5><span>SELECTED PLAN</span></h5></a>
						</li>
					</ul>
				</div>
	<div class="span5">
	<h1 class="text-center"> FAQ (Frequently Asked Questions) </h1>
	<span><h2> What is Print &amp; Deliver? </h2></span>
	<p> Print &amp; deliver is a service offered by Spryli which allows you to order printed versions of your QR codes created through Spryli. </p>
	<span><h2> What is your cancellation policy? </h2></span>
	<p> You may cancel at any time, via logging in, clicking on "Billing" and selecting "Close account". </p>
	<span><h2> Do my views expire? </h2></span>
	<p> For monthly plans, all views reset at the end of the month. For our pay-per-use plan views do not expire. </p>
	
	</div>
	</div>
	<div class="span5">
	<div id="processing">
	<h1 class="text-center"> Processing.. <img src="img/ajax-loader.gif"/> </h1>
	</div>
	<form id="signupForm">
			<h2> Contact Info </h2>
			<div id="signupStatus">
			</div>
			<input type="hidden" id="planType" value="<?php echo $_GET['planId'] ?>">
			<p class="form_info">First Name <span class="required">*</span></p>
			<input class="span5" type="text" id="fName" value="">
			<p class="form_info">Last Name <span class="required">*</span></p>
			<input class="span5" type="text" id="lName" value="">
			<p class="form_info">Email <span class="required">*</span></p>
			<input class="span5" type="text" id="email" value="">
			<p class="form_info">Password</p>
			<input class="span5" type="password" id="password" value=""><br>
			<p class="form_info">Confirm Password <span class="required">*</span></p>
			<input class="span5" type="password" id="passwordConfirm" value="">
			<hr>
			<h2> Billing Info </h2>
			<div id="billingStatus">
			</div>
			<p class="form_info">Card Number<span class="required">*</span></p>
			<input class="span5" type="text" id="cardNumber" value="">
			<p class="form_info">Name On Card<span class="required">*</span></p>
			<input class="span5" type="text" id="nameOnCard" value="">
			<p class="form_info">CVC (Code on back of card) <span class="required">*</span></p>
			<input class="span2 " type="text" id="cvc" value="">
			<p class="form_info">Expiration Month<span class="required">*</span></p>
			<input class="span2" type="text" length="2" id="expMonth" value="">
			<p class="form_info">Expiration Year<span class="required">*</span></p>
			<input class="span2" type="text" length="2" id="expYear" value="">
			<p class="form_info">Address Line 1 <span class="required">*</span></p>
			<input class="span5" type="text" id="address1" value="">
			<p class="form_info">Address Line 2 <span class="required">*</span></p>
			<input class="span5" type="text" id="address2" value="">
			<p class="form_info">City<span class="required">*</span></p>
			<input class="span5" type="text" id="city" value="">
			<p class="form_info">State <span class="required">*</span></p>
			<select class="span2" type="text" id="state" value="">
                <option value="Select One">Select One</option>
                <option value="AK">Alaska</option>
                <option value="AL">Alabama</option>
                <option value="AR">Arkansas</option>
                <option value="AZ">Arizona</option>
                <option value="CA">California</option>
                <option value="CO">Colorado</option>
                <option value="CT">Connecticut</option>
                <option value="DC">District of Columbia</option>
                <option value="DE">Delaware</option>
                <option value="FL">Florida</option>
                <option value="GA">Georgia</option>
                <option value="HI">Hawaii</option>
                <option value="IA">Iowa</option>
                <option value="ID">Idaho</option>
                <option value="IL">Illinois</option>
                <option value="IN">Indiana</option>
                <option value="KS">Kansas</option>
                <option value="KY">Kentucky</option>
                <option value="LA">Louisiana</option>
                <option value="MA">Massachusetts</option>
                <option value="MD">Maryland</option>
                <option value="ME">Maine</option>
                <option value="MI">Michigan</option>
                <option value="MN">Minnesota</option>
                <option value="MO">Missouri</option>
                <option value="MS">Mississippi</option>
                <option value="MT">Montana</option>
                <option value="NC">North Carolina</option>
                <option value="ND">North Dakota</option>
                <option value="NE">Nebraska</option>
                <option value="NH">New Hampshire</option>
                <option value="NJ">New Jersey</option>
                <option value="NM">New Mexico</option>
                <option value="NV">Nevada</option>
                <option value="NY">New York</option>
                <option value="OH">Ohio</option>
                <option value="OK">Oklahoma</option>
                <option value="OR">Oregon</option>
                <option value="PA">Pennsylvania</option>
                <option value="PR">Puerto Rico</option>
                <option value="RI">Rhode Island</option>
                <option value="SC">South Carolina</option>
                <option value="SD">South Dakota</option>
                <option value="TN">Tennessee</option>
                <option value="TX">Texas</option>
                <option value="UT">Utah</option>
                <option value="VA">Virginia</option>
                <option value="VT">Vermont</option>
                <option value="WA">Washington</option>
                <option value="WI">Wisconsin</option>
                <option value="WV">West Virginia</option>
                <option value="WY">Wyoming</option>
              </select>
            
			</select>
			<p class="form_info">Zip <span class="required">*</span></p>
			<input class="span2 " type="text" id="zip" value="">
			<p class="form_info">Phone Number</p>
			<input class="span5" type="text" id="phone" value="">
			<br />
			<p class="form_info"> <input type="checkbox" value=""> I agree to the <a href="terms.php" target="_blank">Site Terms &amp; Conditions</a> </p>
			
			<br />
			<hr>
			<p class="form_info">Coupon Code </p>
			<input class="span2 " type="text" id="coupon" value="">
			<br />
			<input type="button" class="btn btn-success btn-form marg-right5 btn-large btn-rounded pull-right" id="signupButton" value="Sign up!">
			<div class="clear"></div>
		</form>	
	</div>
	</div>
	<!-- END DEMO -->

	</div>
		<!--//page-->
		
		<div class="pad25 hidden-desktop"></div>
	</div>
	
	<!-- footer -->
	
	
	<!-- footer 2 -->
	
						
				<!-- up to top -->
				<a href="#"><i class="go-top hidden-phone hidden-tablet  icon-double-angle-up"></i></a>
				<!--//end-->
				
<script src="js/jquery.js"></script>			
<script type="text/javascript" src="js/signup.js"></script>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-42810904-1', 'spryli.com');
  ga('send', 'pageview');

</script>
</body>
</html>